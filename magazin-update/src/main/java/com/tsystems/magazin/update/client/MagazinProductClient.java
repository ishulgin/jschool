package com.tsystems.magazin.update.client;

import com.tsystems.magazin.domain.dto.ProductDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("magazin-product")
public interface MagazinProductClient {
    @GetMapping("/stats/top")
    List<ProductDTO> getMostPopularProducts(@RequestParam("n") int n);

    @GetMapping("/stats/sales")
    List<ProductDTO> getBestSalesOffers(@RequestParam("n") int n);
}
