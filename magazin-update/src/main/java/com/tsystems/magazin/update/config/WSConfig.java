package com.tsystems.magazin.update.config;

import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.json.JSONConfiguration;

import java.util.HashMap;
import java.util.Map;

public class WSConfig extends PackagesResourceConfig {
    public WSConfig() {
        super(properties());
    }

    private static Map<String, Object> properties() {
        Map<String, Object> result = new HashMap<>();
        result.put(PackagesResourceConfig.PROPERTY_PACKAGES, "com.sun.jersey;com.tsystems.magazin.update.controller.ws");
        result.put("com.sun.jersey.config.feature.FilterForwardOn404", "true");
        result.put(JSONConfiguration.FEATURE_POJO_MAPPING, "true");

        return result;
    }
}
