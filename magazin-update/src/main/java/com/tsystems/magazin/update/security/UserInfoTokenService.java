package com.tsystems.magazin.update.security;

import lombok.Setter;
import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.FixedAuthoritiesExtractor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.BaseOAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

import java.util.*;

public class UserInfoTokenService implements ResourceServerTokenServices {
    private static final String[] PRINCIPAL_KEYS =
            new String[]{"user", "username", "userid", "user_id", "login", "id", "name"};

    private final String userInfoEndpointUrl;

    private final String clientId;

    @Setter
    private OAuth2RestOperations restTemplate;

    @Setter
    private String tokenType = DefaultOAuth2AccessToken.BEARER_TYPE;

    @Setter
    private AuthoritiesExtractor authoritiesExtractor = new FixedAuthoritiesExtractor();

    public UserInfoTokenService(String userInfoEndpointUrl, String clientId) {
        this.userInfoEndpointUrl = userInfoEndpointUrl;
        this.clientId = clientId;
    }

    @Override
    public OAuth2Authentication loadAuthentication(String accessToken) {
        Map<String, Object> map = getMap(this.userInfoEndpointUrl, accessToken);

        if (map.containsKey("error")) {
            throw new InvalidTokenException(accessToken);
        }

        return extractAuthentication(map);
    }

    private OAuth2Authentication extractAuthentication(Map<String, Object> map) {
        Object principal = getPrincipal(map);

        OAuth2Request request = getRequest(map);

        List<GrantedAuthority> authorities = authoritiesExtractor.extractAuthorities(map);

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(principal, "N/A", authorities);
        token.setDetails(map);

        return new OAuth2Authentication(request, token);
    }

    private Object getPrincipal(Map<String, Object> map) {
        for (String key : PRINCIPAL_KEYS) {
            if (map.containsKey(key)) {
                return map.get(key);
            }
        }
        return "unknown";
    }

    @SuppressWarnings({"unchecked"})
    private OAuth2Request getRequest(Map<String, Object> map) {
        Map<String, Object> request = (Map<String, Object>) map.get("oauth2Request");

        String requestClientId = (String) request.get("clientId");

        Set<String> scope = request.containsKey("scope") ?
                new HashSet<>((Collection<String>) request.get("scope")) :
                Collections.emptySet();

        Set<GrantedAuthority> authorities = request.containsKey("authorities") ?
                new HashSet<>((Collection<GrantedAuthority>) request.get("authorities")) :
                Collections.emptySet();

        return new OAuth2Request(null, requestClientId, authorities, true, scope,
                null, null, null, null);
    }

    @Override
    public OAuth2AccessToken readAccessToken(String accessToken) {
        throw new UnsupportedOperationException("Not supported: read access token");
    }

    @SuppressWarnings({"unchecked"})
    private Map<String, Object> getMap(String path, String accessToken) {
        try {
            if (restTemplate == null) {
                BaseOAuth2ProtectedResourceDetails resource = new BaseOAuth2ProtectedResourceDetails();
                resource.setClientId(this.clientId);

                restTemplate = new OAuth2RestTemplate(resource);
            }

            OAuth2AccessToken existingToken = restTemplate.getOAuth2ClientContext().getAccessToken();

            if (existingToken == null || !accessToken.equals(existingToken.getValue())) {
                DefaultOAuth2AccessToken token = new DefaultOAuth2AccessToken(accessToken);
                token.setTokenType(tokenType);

                restTemplate.getOAuth2ClientContext().setAccessToken(token);
            }

            return restTemplate.getForEntity(path, Map.class).getBody();
        } catch (Exception e) {
            return Collections.singletonMap("error", "Could not fetch user details");
        }
    }
}
