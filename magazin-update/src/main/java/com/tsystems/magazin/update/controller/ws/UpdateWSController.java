package com.tsystems.magazin.update.controller.ws;

import com.tsystems.magazin.update.service.UpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/products")
@Produces(MediaType.APPLICATION_JSON)
@Component
public class UpdateWSController {
    private UpdateService updateService;

    @Autowired
    public void setUpdateService(UpdateService updateService) {
        this.updateService = updateService;
    }

    @GET
    @Path("/top/{max}")
    public Response getTopProducts(@PathParam("max") int max) {
        return Response.ok(updateService.getCachedTopProducts(max)).build();
    }

    @GET
    @Path("/sales/{max}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSalesProducts(@PathParam("max") int max) {
        return Response.ok(updateService.getCachedSalesProducts(max)).build();
    }
}
