package com.tsystems.magazin.update.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
//TODO config -> properties
public class MQConfig {
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("admin");
        connectionFactory.setPassword("admin");

        return connectionFactory;
    }

    @Bean
    public TopicExchange topic() {
        return new TopicExchange("magazin-exchange");
    }

    @Bean
    public Queue topQueue() {
        return new Queue("top-queue");
    }

    @Bean
    public Queue salesQueue() {
        return new Queue("sales-queue");
    }

    @Bean
    public Queue generalQueue() {
        return new Queue("general-queue");
    }

    @Bean
    public Binding bindingTop(TopicExchange topic, Queue topQueue) {
        return BindingBuilder.bind(topQueue)
                .to(topic)
                .with("top.*");
    }

    @Bean
    public Binding bindingSales(TopicExchange topic, Queue salesQueue) {
        return BindingBuilder.bind(salesQueue)
                .to(topic)
                .with("sale.*");
    }

    @Bean
    public Binding bindingGeneral(TopicExchange topic, Queue generalQueue) {
        return BindingBuilder.bind(generalQueue)
                .to(topic)
                .with("general.*");
    }
}
