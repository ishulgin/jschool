package com.tsystems.magazin.update;

import com.sun.jersey.spi.spring.container.servlet.SpringServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import javax.servlet.ServletRegistration;
import java.util.HashMap;
import java.util.Map;

@EnableAsync
@EnableDiscoveryClient
@EnableResourceServer
@EnableOAuth2Client
@EnableFeignClients
@EnableGlobalMethodSecurity(prePostEnabled = true)
@SpringBootApplication
public class MagazinUpdateApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(MagazinUpdateApplication.class, args);
    }

    @Bean
    public ServletContextInitializer servletInitializer() {
        return servletContext -> {
            final ServletRegistration.Dynamic appServlet = servletContext.addServlet("jersey-servlet", new SpringServlet());
            Map<String, String> filterParameters = new HashMap<>();

            filterParameters.put("javax.ws.rs.Application", "com.tsystems.magazin.update.config.WSConfig");
            appServlet.setInitParameters(filterParameters);
            appServlet.setLoadOnStartup(2);
            appServlet.addMapping("/updates/*");
        };
    }
}
