package com.tsystems.magazin.update.controller;

import com.tsystems.magazin.update.service.UpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UpdatesController {
    private UpdateService updateService;

    @Autowired
    public void setUpdateService(UpdateService updateService) {
        this.updateService = updateService;
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/products/top")
    public ResponseEntity initTopUpdate() {
        updateService.initTopProductsUpdate();

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/products/sales")
    public ResponseEntity initSalesUpdate() {
        updateService.initSalesProductsUpdate();

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/products/general")
    public ResponseEntity initGeneralUpdate() {
        updateService.initGeneralUpdate();

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
