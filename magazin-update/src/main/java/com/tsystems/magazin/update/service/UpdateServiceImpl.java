package com.tsystems.magazin.update.service;

import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.update.client.MagazinProductClient;
import com.tsystems.magazin.update.event.UpdateEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UpdateServiceImpl implements UpdateService {
    private static final int CACHE_SIZE = 10;

    private List<ProductDTO> topProducts;

    private List<ProductDTO> salesProducts;

    private MagazinProductClient magazinProductClient;

    private ApplicationEventPublisher applicationEventPublisher;

    private static List<ProductDTO> cut(List<ProductDTO> list, int n) {
        return list
                .stream()
                .limit(n)
                .collect(Collectors.toList());
    }

    @Autowired
    public void setMagazinProductClient(MagazinProductClient magazinProductClient) {
        this.magazinProductClient = magazinProductClient;
    }

    @Autowired
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public void initTopProductsUpdate() {
        if (updateTopProductsList()) {
            applicationEventPublisher.publishEvent(UpdateEvent.TOP);
        }
    }

    private boolean updateTopProductsList() {
        List<ProductDTO> products = magazinProductClient.getMostPopularProducts(CACHE_SIZE);

        if (topProducts != null && topProducts.equals(products)) {
            return false;
        }

        topProducts = products;

        log.info("Top was updated.");

        return true;
    }

    @Override
    public void initSalesProductsUpdate() {
        if (updateSalesProductsList()) {
            applicationEventPublisher.publishEvent(UpdateEvent.SALES);
        }
    }

    private boolean updateSalesProductsList() {
        List<ProductDTO> products = magazinProductClient.getBestSalesOffers(CACHE_SIZE);

        if (salesProducts != null && salesProducts.equals(products)) {
            return false;
        }

        salesProducts = products;

        log.info("Best sales offers were updated.");

        return true;
    }

    @Override
    public void initGeneralUpdate() {
        boolean isSalesProductsUpdated = updateSalesProductsList();
        boolean isTopProductsUpdated = updateTopProductsList();

        if (isSalesProductsUpdated && isTopProductsUpdated) {
            applicationEventPublisher.publishEvent(UpdateEvent.GENERAL);
        } else if (isSalesProductsUpdated) {
            applicationEventPublisher.publishEvent(UpdateEvent.SALES);
        } else if (isTopProductsUpdated) {
            applicationEventPublisher.publishEvent(UpdateEvent.TOP);
        }
    }

    @NotNull
    @Override
    public List<ProductDTO> getCachedTopProducts(int n) {
        if (topProducts == null) {
            log.info("Fetching top.");

            topProducts = magazinProductClient.getMostPopularProducts(CACHE_SIZE);
        }

        return n > CACHE_SIZE ?
                magazinProductClient.getMostPopularProducts(n) :
                cut(topProducts, n);
    }

    @NotNull
    @Override
    public List<ProductDTO> getCachedSalesProducts(int n) {
        if (salesProducts == null) {
            log.info("Fetching best sales offers.");

            salesProducts = magazinProductClient.getBestSalesOffers(CACHE_SIZE);
        }

        return n > CACHE_SIZE ?
                magazinProductClient.getBestSalesOffers(n) :
                cut(salesProducts, n);
    }
}
