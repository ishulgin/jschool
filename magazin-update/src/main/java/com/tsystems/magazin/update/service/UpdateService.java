package com.tsystems.magazin.update.service;

import com.tsystems.magazin.domain.dto.ProductDTO;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface UpdateService {
    void initTopProductsUpdate();

    void initSalesProductsUpdate();

    void initGeneralUpdate();

    @NotNull List<ProductDTO> getCachedTopProducts(int n);

    @NotNull List<ProductDTO> getCachedSalesProducts(int n);
}
