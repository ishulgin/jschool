package com.tsystems.magazin.update.component;

import com.tsystems.magazin.update.event.UpdateEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class UpdateListener {
    private RabbitTemplate template;

    @Autowired
    public void setTemplate(RabbitTemplate template) {
        this.template = template;
    }

    @Async
    @EventListener
    public void onUpdate(UpdateEvent updateEvent) {
        switch (updateEvent) {
            case TOP:
                forceTopProductsUpdate();
                break;
            case SALES:
                forceSalesProductsUpdate();
                break;
            case GENERAL:
                forceGeneralUpdate();
                break;
        }
    }

    private void forceTopProductsUpdate() {
        log.info("Triggering top update.");

        template.convertAndSend("magazin-exchange", "top.basic", "");
    }

    private void forceSalesProductsUpdate() {
        log.info("Triggering best sales offers update.");

        template.convertAndSend("magazin-exchange", "sales.basic", "");
    }

    private void forceGeneralUpdate() {
        log.info("Triggering general update.");

        template.convertAndSend("magazin-exchange", "general.basic", "");
    }
}
