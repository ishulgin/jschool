package com.tsystems.magazin.update.event;

public enum UpdateEvent {
    TOP,
    SALES,
    GENERAL
}
