package com.tsystems.magazin.auth.controller;

import com.tsystems.magazin.auth.service.AuthService;
import com.tsystems.magazin.domain.dto.UserDTO;
import com.tsystems.magazin.domain.dto.request.PasswordRequestDTO;
import com.tsystems.magazin.domain.dto.request.UserRequestDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
public class AuthController {
    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    private AuthService authService;

    @Autowired
    public void setAuthService(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping("/current")
    public Principal getPrincipal(Authentication authentication, Principal principal) {
        logger.debug("{}: Getting principal.", principal.getName() != null ? principal.getName() : "-");

        return principal;
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/users")
    public ResponseEntity<Integer> createUser(@RequestBody @Valid UserRequestDTO userRequestDTO) {
        try {
            return ResponseEntity.ok(authService.createUser(userRequestDTO));
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/users/{username}")
    public ResponseEntity<UserDTO> getUserDetails(@PathVariable("username") @NotNull String username) {
        return ResponseEntity.ok(authService.getUserDetails(username));
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/users/{id}/password")
    public ResponseEntity changePassword(@PathVariable("id") int userId, @RequestBody @Valid PasswordRequestDTO passwordRequestDTO) {
        try {
            authService.changePassword(userId, passwordRequestDTO);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
