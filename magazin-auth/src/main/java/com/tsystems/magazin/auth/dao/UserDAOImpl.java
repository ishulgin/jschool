package com.tsystems.magazin.auth.dao;

import com.tsystems.magazin.auth.model.Role;
import com.tsystems.magazin.auth.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
public class UserDAOImpl implements UserDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User findUserById(int id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public User findByUsername(String username) {
        try {
            return (User) entityManager.createQuery("select u from User u where u.username=:username")
                    .setParameter("username", username)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public boolean existsByUsername(String username) {
        Query query = entityManager.createQuery("select count(u) from User u where u.username=:username")
                .setParameter("username", username);

        return (long) query.getSingleResult() != 0;
    }

    @Override
    public void saveUser(User user) {
        entityManager.persist(user);
    }

    @Override
    public void updateUser(User user) {
        entityManager.merge(user);
    }

    @Override
    public Role getDefaultRole() {
        return (Role) entityManager.createQuery("select r from Role r where r.role='user'")
                .getSingleResult();
    }
}
