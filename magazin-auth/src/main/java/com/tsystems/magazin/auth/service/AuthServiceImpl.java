package com.tsystems.magazin.auth.service;

import com.tsystems.magazin.auth.dao.UserDAO;
import com.tsystems.magazin.auth.model.User;
import com.tsystems.magazin.auth.util.ModelMapperWrapper;
import com.tsystems.magazin.domain.dto.UserDTO;
import com.tsystems.magazin.domain.dto.request.PasswordRequestDTO;
import com.tsystems.magazin.domain.dto.request.UserRequestDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Collections;

@Service
@Slf4j
public class AuthServiceImpl implements AuthService {
    private PasswordEncoder encoder;

    @Autowired
    public void setEncoder(PasswordEncoder encoder) {
        this.encoder = encoder;
    }

    private UserDAO userDAO;

    private ModelMapperWrapper modelMapperWrapper;

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Autowired
    public void setModelMapperWrapper(ModelMapperWrapper modelMapperWrapper) {
        this.modelMapperWrapper = modelMapperWrapper;
    }

    @Override
    @Transactional
    public int createUser(@NotNull UserRequestDTO userRequestDTO) {
        if (userDAO.existsByUsername(userRequestDTO.getUsername())) {
            log.debug("{}: User already exists.", userRequestDTO.getUsername());
        }

        User user = modelMapperWrapper.mapUserRequestDTO(userRequestDTO);
        user.setPassword(encoder.encode(userRequestDTO.getPassword()));
        user.setRoles(Collections.singleton(userDAO.getDefaultRole()));

        userDAO.saveUser(user);

        log.info("{}: Registered.", user.getUsername());

        return user.getId();
    }

    @Override
    @Transactional
    public UserDTO getUserDetails(@NotNull String username) {
        User user = userDAO.findByUsername(username);

        if (user == null) {
            log.debug("{}: Not found.", username);

            return null;
        }

        return modelMapperWrapper.mapUser(user);
    }

    @Override
    @Transactional
    public void changePassword(int userId, @NotNull PasswordRequestDTO passwordRequestDTO) {
        User user = userDAO.findUserById(userId);

        if (user == null) {
            log.debug("User {}: Not found.", userId);

            throw new IllegalArgumentException();
        }

        if (!encoder.matches(passwordRequestDTO.getOldPassword(), user.getPassword())) {
            log.debug("User {}: Passwords don't match.", userId);

            throw new IllegalArgumentException();
        }

        log.info("User {}: Password was changed.", user.getId());

        user.setPassword(encoder.encode(passwordRequestDTO.getPassword()));

        userDAO.updateUser(user);
    }
}
