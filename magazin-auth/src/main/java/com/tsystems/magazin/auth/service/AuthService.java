package com.tsystems.magazin.auth.service;

import com.tsystems.magazin.domain.dto.UserDTO;
import com.tsystems.magazin.domain.dto.request.PasswordRequestDTO;
import com.tsystems.magazin.domain.dto.request.UserRequestDTO;

import javax.validation.constraints.NotNull;

public interface AuthService {
    int createUser(@NotNull UserRequestDTO userRequestDTO);

    UserDTO getUserDetails(@NotNull String username);

    void changePassword(int userId, @NotNull PasswordRequestDTO passwordRequestDTO);
}
