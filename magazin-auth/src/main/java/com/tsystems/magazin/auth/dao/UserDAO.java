package com.tsystems.magazin.auth.dao;

import com.tsystems.magazin.auth.model.Role;
import com.tsystems.magazin.auth.model.User;

public interface UserDAO {
    User findUserById(int id);

    User findByUsername(String username);

    boolean existsByUsername(String username);

    void saveUser(User user);

    void updateUser(User user);

    Role getDefaultRole();
}
