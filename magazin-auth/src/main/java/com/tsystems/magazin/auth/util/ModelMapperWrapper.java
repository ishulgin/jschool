package com.tsystems.magazin.auth.util;

import com.tsystems.magazin.auth.model.Role;
import com.tsystems.magazin.auth.model.User;
import com.tsystems.magazin.domain.dto.UserDTO;
import com.tsystems.magazin.domain.dto.request.UserRequestDTO;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ModelMapperWrapper {
    private ModelMapper modelMapper = new ModelMapper();

    public ModelMapperWrapper() {
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.LOOSE);

        modelMapper.getConfiguration().setAmbiguityIgnored(true);

        modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(mapping -> mapping.skip(UserDTO::setRoles));
    }

    public User mapUserRequestDTO(UserRequestDTO userRequestDTO) {
        return modelMapper.map(userRequestDTO, User.class);
    }

    public UserDTO mapUser(User user) {
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        userDTO.setRoles(user.getRoles()
                .stream()
                .map(Role::getRole)
                .collect(Collectors.toList()));
        return userDTO;
    }
}
