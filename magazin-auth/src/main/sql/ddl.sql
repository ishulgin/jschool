CREATE TABLE Role (
  id   INT AUTO_INCREMENT,
  role VARCHAR(32) NOT NULL UNIQUE,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;

INSERT INTO Role
VALUES (1, 'user'),
       (2, 'admin'),
       (3, 'manager');

CREATE TABLE User (
  id       INT                  AUTO_INCREMENT,
  username VARCHAR(64) NOT NULL UNIQUE,
  password VARCHAR(60) NOT NULL,
  enabled  BOOLEAN     NOT NULL DEFAULT true,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;

CREATE INDEX userUsernameIndex
  ON User (username) USING HASH;

CREATE TABLE UserRole (
  roleId INT,
  userId INT,
  PRIMARY KEY (roleId, userId),
  FOREIGN KEY (roleId) REFERENCES Role (id),
  FOREIGN KEY (userId) REFERENCES User (id)
)
  ENGINE = InnoDB;