package com.tsystems.magazin.gateway.controller.rest;

import com.tsystems.magazin.domain.dto.OrderItemDTO;
import com.tsystems.magazin.domain.model.OrderStatus;
import com.tsystems.magazin.domain.model.PaymentStatus;
import com.tsystems.magazin.gateway.client.MagazinOrderClient;
import com.tsystems.magazin.gateway.component.SessionCart;
import com.tsystems.magazin.gateway.security.model.CustomUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderRestController {
    private SessionCart sessionCart;

    private MagazinOrderClient magazinOrderClient;

    @Autowired
    public void setSessionCart(SessionCart sessionCart) {
        this.sessionCart = sessionCart;
    }

    @Autowired
    public void setMagazinOrderClient(MagazinOrderClient magazinOrderClient) {
        this.magazinOrderClient = magazinOrderClient;
    }

    @DeleteMapping(value = "/cart")
    @ResponseBody
    public ResponseEntity removeCartItem(@RequestBody int productId, @AuthenticationPrincipal CustomUser user) {
        if (user == null) {
            sessionCart.getProducts().remove(productId);

            return new ResponseEntity<>(HttpStatus.OK);
        }

        magazinOrderClient.removeCartItem(user.getId(), productId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/orders/{id}")
    public ResponseEntity<List<OrderItemDTO>> getOrderItems(@PathVariable int id,
                                                            @AuthenticationPrincipal CustomUser user) {
        try {
            return ResponseEntity.ok(magazinOrderClient.getOrderItems(id, user.getId()));
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAuthority('manager')")
    @GetMapping("/admin/orders/{id}")
    public ResponseEntity<List<OrderItemDTO>> getOrderItems(@PathVariable int id) {
        return ResponseEntity.ok(magazinOrderClient.getOrderItems(id));
    }

    @PreAuthorize("hasAuthority('manager')")
    @PostMapping("/orders/{id}/paymentStatus")
    public ResponseEntity updatePaymentStatus(@PathVariable int id, @RequestBody PaymentStatus paymentStatus) {
        try {
            magazinOrderClient.updatePaymentStatus(id, paymentStatus);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAuthority('manager')")
    @PostMapping("/orders/{id}/orderStatus")
    public ResponseEntity updateOrderStatus(@PathVariable int id, @RequestBody OrderStatus orderStatus) {
        try {
            magazinOrderClient.updateOrderStatus(id, orderStatus);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
