package com.tsystems.magazin.gateway.controller;

import com.tsystems.magazin.domain.dto.AccountDTO;
import com.tsystems.magazin.domain.dto.request.AccountDetailsRequestDTO;
import com.tsystems.magazin.domain.dto.request.AddressRequestDTO;
import com.tsystems.magazin.domain.dto.request.PasswordRequestDTO;
import com.tsystems.magazin.gateway.client.MagazinAccountClient;
import com.tsystems.magazin.gateway.security.model.CustomUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private MagazinAccountClient magazinAccountClient;

    @Autowired
    public void setMagazinAccountClient(MagazinAccountClient magazinAccountClient) {
        this.magazinAccountClient = magazinAccountClient;
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/profile")
    public String profileGet(Model model,
                             @ModelAttribute("message") String message,
                             @AuthenticationPrincipal CustomUser user) {
        model.addAttribute("account", magazinAccountClient.getAccountDetails(user.getId()));

        return "account/profile";
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/profile/address")
    public String addressGet(Model model) {
        model.addAttribute("address", new AddressRequestDTO());

        return "account/createAddress";
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/profile/address")
    public String addressPost(Model model,
                              RedirectAttributes redirectAttributes,
                              @ModelAttribute("address") @Valid AddressRequestDTO address,
                              BindingResult result,
                              @AuthenticationPrincipal CustomUser user) {
        if (result.hasErrors()) {
            model.addAttribute("address", address);

            return "account/createAddress";
        }

        magazinAccountClient.addAddress(user.getId(), address);

        redirectAttributes.addFlashAttribute("message", "New address was successfully added.");

        logger.info("{}: Saved new address.", user.getUsername());

        return "redirect:/profile";
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/profile/password")
    public String passwordGet(Model model) {
        model.addAttribute("passwordRequestDTO", new PasswordRequestDTO());

        return "account/changePassword";
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/profile/password")
    public String passwordPost(Model model,
                               RedirectAttributes redirectAttributes,
                               @ModelAttribute("passwordRequestDTO") @Valid PasswordRequestDTO passwordRequestDTO,
                               BindingResult result,
                               @AuthenticationPrincipal CustomUser user) {
        if (result.hasErrors()) {
            model.addAttribute("passwordRequestDTO", passwordRequestDTO);

            return "account/changePassword";
        }

        try {
            magazinAccountClient.changePassword(user.getId(), passwordRequestDTO);
        } catch (IllegalArgumentException e) {
            result.rejectValue("oldPassword", "error.oldPassword", "Specify correct password.");

            model.addAttribute("passwordRequestDTO", passwordRequestDTO);

            return "account/changePassword";
        }

        redirectAttributes.addFlashAttribute("message", "Password was changed.");

        logger.info("{}: Changed password.", user.getUsername());

        return "redirect:/profile";
    }


    @PreAuthorize("isAuthenticated()")
    @GetMapping("/profile/details")
    public String detailsGet(Model model,
                             @AuthenticationPrincipal CustomUser user) {

        model.addAttribute("accountDetails", getAccountDetailsRequestDTO(user.getId()));

        return "account/updateDetails";
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/profile/details")
    public String detailsPost(Model model,
                              RedirectAttributes redirectAttributes,
                              @ModelAttribute("accountDetails") @Valid  AccountDetailsRequestDTO accountDetailsRequestDTO,
                              BindingResult result,
                              @AuthenticationPrincipal CustomUser user) {
        if (result.hasErrors()) {
            model.addAttribute("accountDetails", accountDetailsRequestDTO);

            return "account/updateDetails";
        }

        magazinAccountClient.changeDetails(user.getId(), accountDetailsRequestDTO);

        redirectAttributes.addFlashAttribute("message", "Account details were changed.");

        logger.info("{}: Changed account details.", user.getUsername());

        return "redirect:/profile";
    }

    private AccountDetailsRequestDTO getAccountDetailsRequestDTO(int accountId) {
        AccountDTO accountDTO = magazinAccountClient.getAccountDetails(accountId);

        AccountDetailsRequestDTO accountDetailsRequestDTO = new AccountDetailsRequestDTO();
        accountDetailsRequestDTO.setName(accountDTO.getName());
        accountDetailsRequestDTO.setSurname(accountDTO.getSurname());
        accountDetailsRequestDTO.setDateOfBirth(accountDTO.getDateOfBirth());

        return accountDetailsRequestDTO;
    }
}
