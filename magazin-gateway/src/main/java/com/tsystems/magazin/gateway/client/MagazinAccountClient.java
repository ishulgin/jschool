package com.tsystems.magazin.gateway.client;

import com.tsystems.magazin.domain.dto.AccountDTO;
import com.tsystems.magazin.domain.dto.AddressDTO;
import com.tsystems.magazin.domain.dto.UserDTO;
import com.tsystems.magazin.domain.dto.request.AccountDetailsRequestDTO;
import com.tsystems.magazin.domain.dto.request.AccountRequestDTO;
import com.tsystems.magazin.domain.dto.request.AddressRequestDTO;
import com.tsystems.magazin.domain.dto.request.PasswordRequestDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@FeignClient(name = "magazin-account")
public interface MagazinAccountClient {
    @PostMapping(value = "/account", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    void createAccount(@RequestBody @NotNull AccountRequestDTO accountRequestDTO);

    @GetMapping(value = "/userDetails", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    UserDTO loadUserDetails(@RequestParam("username") @NotNull String username);

    @GetMapping(value = "/accountDetails", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    AccountDTO getAccountDetails(@RequestParam("id") int accountId);

    @PostMapping(value = "/account/{id}/address", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    void addAddress(@PathVariable("id") int accountId, @RequestBody @NotNull AddressRequestDTO addressRequestDTO);

    @GetMapping("/addresses/{id}")
    AddressDTO getAddress(@PathVariable("id") int addressId);

    @GetMapping("/account/{id}/addresses")
    List<AddressDTO> getAddresses(@PathVariable("id") int accountId);

    @PostMapping("/account/{id}/details")
    void changeDetails(@PathVariable("id") int accountId, @RequestBody @NotNull AccountDetailsRequestDTO accountDetailsRequestDTO);

    @PostMapping(value = "/account/{id}/password", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    void changePassword(@PathVariable("id") int accountId, @RequestBody @NotNull PasswordRequestDTO passwordRequestDTO);
}
