package com.tsystems.magazin.gateway.controller;

import com.tsystems.magazin.domain.dto.request.AccountRequestDTO;
import com.tsystems.magazin.gateway.client.MagazinAccountClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class AuthController {
    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    private MagazinAccountClient magazinAccountClient;

    @Autowired
    public void setMagazinAccountClient(MagazinAccountClient magazinAccountClient) {
        this.magazinAccountClient = magazinAccountClient;
    }

    @PreAuthorize("isAnonymous()")
    @GetMapping("/registration")
    public String registrationGet(Model model) {
        logger.debug("Requesting registration page.");

        model.addAttribute("account", new AccountRequestDTO());

        return "authentication/registration";
    }

    @PreAuthorize("isAnonymous()")
    @PostMapping("/registration")
    public String registrationPost(@ModelAttribute("account") @Valid AccountRequestDTO accountRequestDTO,
                                   BindingResult result,
                                   Model model) {
        if (result.hasErrors()) {
            logger.debug("Invalid registration request.");

            model.addAttribute("account", accountRequestDTO);
            return "authentication/registration";
        }

        try {
            magazinAccountClient.createAccount(accountRequestDTO);
        } catch (IllegalArgumentException e) {
            logger.debug("Account with username {} already exists.", accountRequestDTO.getUsername());

            return "authentication/registration";
        }

        logger.info("User with username {} was registered.", accountRequestDTO.getUsername());

        return "authentication/login";
    }

    @PreAuthorize("isAnonymous()")
    @GetMapping(value = "/login")
    public String loginGet(Model model, @RequestParam(value = "error", required = false) String error) {
        if (error != null) {
            logger.debug("Requesting login page with error.");

            model.addAttribute("error", "Invalid email or password.");
        }

        logger.debug("Requesting login page.");

        return "authentication/login";
    }
}
