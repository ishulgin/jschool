package com.tsystems.magazin.gateway.security;

import com.tsystems.magazin.domain.dto.UserDTO;
import com.tsystems.magazin.gateway.client.MagazinAccountClient;
import com.tsystems.magazin.gateway.security.model.CustomUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class FeignUserDetailsService implements UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(FeignUserDetailsService.class);

    private MagazinAccountClient magazinAccountClient;

    @Autowired
    public void setMagazinAccountClient(MagazinAccountClient magazinAccountClient) {
        this.magazinAccountClient = magazinAccountClient;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        try {
            UserDTO userDTO = magazinAccountClient.loadUserDetails(username);

            if (userDTO == null) {
                logger.debug("{}: User not found.", username);

                throw new UsernameNotFoundException(username);
            }

            logger.debug("{}: User with authorities {} was found.", username, userDTO.getRoles());

            return new CustomUser(userDTO.getUsername(),
                    userDTO.getPassword(),
                    userDTO.isEnabled(),
                    true,
                    true,
                    true,
                    userDTO.getRoles()
                            .stream()
                            .map(SimpleGrantedAuthority::new)
                            .collect(Collectors.toSet()),
                    userDTO.getAccountId()
            );
        } catch (Exception e) {
            logger.error("Can't connect to account service: {}.", e.getMessage());

            throw new UsernameNotFoundException("Account server is down.");
        }
    }
}
