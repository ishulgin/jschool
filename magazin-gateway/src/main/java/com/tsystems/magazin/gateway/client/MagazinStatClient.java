package com.tsystems.magazin.gateway.client;

import com.tsystems.magazin.domain.dto.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("magazin-stat")
public interface MagazinStatClient {
    @GetMapping("/stats/week/departments")
    List<DepartmentStatDTO> getDepartmentStatForLastWeek();

    @GetMapping("/stats/week/departments/{id}")
    List<CategoryStatDTO> getCategoryStat(@PathVariable int id);

    @GetMapping("/stats/week/categories/{id}")
    List<ProductStatDTO> getProductStat(@PathVariable int id);

    @GetMapping("/stats/year")
    List<MonthStatDTO> getYearStat();

    @GetMapping("/stats/accounts")
    List<AccountStatDTO> getAccountStats(@RequestParam int n);
}
