package com.tsystems.magazin.gateway.controller.rest;

import com.tsystems.magazin.domain.dto.CategoryStatDTO;
import com.tsystems.magazin.domain.dto.DepartmentStatDTO;
import com.tsystems.magazin.domain.dto.ProductStatDTO;
import com.tsystems.magazin.gateway.client.MagazinStatClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StatRestController {
    private MagazinStatClient magazinStatClient;

    @Autowired
    public void setMagazinStatClient(MagazinStatClient magazinStatClient) {
        this.magazinStatClient = magazinStatClient;
    }

    @PreAuthorize("hasAuthority('manager')")
    @GetMapping("/stats/week/departments")
    public ResponseEntity<List<DepartmentStatDTO>> getDepartmentStatForLastWeek() {
        return ResponseEntity.ok(magazinStatClient.getDepartmentStatForLastWeek());
    }

    @PreAuthorize("hasAuthority('manager')")
    @GetMapping("/stats/week/departments/{id}")
    public ResponseEntity<List<CategoryStatDTO>> getCategoryStat(@PathVariable int id) {
        return ResponseEntity.ok(magazinStatClient.getCategoryStat(id));
    }

    @PreAuthorize("hasAuthority('manager')")
    @GetMapping("/stats/week/categories/{id}")
    public ResponseEntity<List<ProductStatDTO>> getProductStat(@PathVariable int id) {
        return ResponseEntity.ok(magazinStatClient.getProductStat(id));
    }
}
