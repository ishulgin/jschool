package com.tsystems.magazin.gateway.controller.rest;

import com.tsystems.magazin.domain.dto.AddressDTO;
import com.tsystems.magazin.gateway.client.MagazinAccountClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountRestController {
    private MagazinAccountClient magazinAccountClient;

    @Autowired
    public void setMagazinAccountClient(MagazinAccountClient magazinAccountClient) {
        this.magazinAccountClient = magazinAccountClient;
    }

    @PreAuthorize("hasAuthority('manager')")
    @GetMapping("/addresses/{id}")
    public ResponseEntity<AddressDTO> getAddress(@PathVariable("id") int addressId) {
        return ResponseEntity.ok(magazinAccountClient.getAddress(addressId));
    }
}
