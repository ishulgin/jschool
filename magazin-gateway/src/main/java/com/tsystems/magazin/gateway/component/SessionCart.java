package com.tsystems.magazin.gateway.component;

import com.tsystems.magazin.domain.dto.CartItemDTO;
import com.tsystems.magazin.domain.dto.ProductDTO;
import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;
import java.util.Map;

@Component("sessionCart")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Data
public class SessionCart {
    private Map<Integer, CartItemDTO> products = new HashMap<>();

    public void addCartItem(ProductDTO product, int count) {
        int id = product.getId();

        if (products.containsKey(id)) {
            products.get(id).increaseCount(count);
        } else {
            CartItemDTO cartItemDTO = new CartItemDTO();
            cartItemDTO.setCount(count);
            cartItemDTO.setProduct(product);

            products.put(id, cartItemDTO);
        }
    }
}