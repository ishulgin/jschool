package com.tsystems.magazin.gateway.controller;

import com.tsystems.magazin.gateway.client.MagazinProductClient;
import com.tsystems.magazin.gateway.client.MagazinStatClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RouteController {
    private MagazinProductClient magazinProductClient;

    private MagazinStatClient magazinStatClient;

    @Autowired
    public void setMagazinProductClient(MagazinProductClient magazinProductClient) {
        this.magazinProductClient = magazinProductClient;
    }

    @Autowired
    public void setMagazinStatClient(MagazinStatClient magazinStatClient) {
        this.magazinStatClient = magazinStatClient;
    }

    @GetMapping({"/", "/home", "/index"})
    public String homeGet(Model model) {
        model.addAttribute("lastProducts", magazinProductClient.getLastAddedProducts(4));
        model.addAttribute("popularProduct", magazinProductClient.getMostPopularProducts(4));

        return "index";
    }

    @PreAuthorize("hasAuthority('manager')")
    @GetMapping(value = "/stats")
    public String stats(Model model) {
        model.addAttribute("clients", magazinStatClient.getAccountStats(10));
        model.addAttribute("yearStats", magazinStatClient.getYearStat());

        return "admin/stats";
    }

    @GetMapping("/serviceUnavailable")
    public String serviceUnavailableGet() {
        return "serviceUnavailable";
    }

    @GetMapping("/error")
    public String errorGet() {
        return "error";
    }
}
