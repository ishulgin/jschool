package com.tsystems.magazin.gateway.controller;

import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.CartItemRequestDTO;
import com.tsystems.magazin.domain.dto.request.FilterTypeRequestDTO;
import com.tsystems.magazin.domain.dto.request.ProductRequestDTO;
import com.tsystems.magazin.gateway.client.MagazinProductClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.imageio.ImageIO;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@Slf4j
public class ProductController {
    @Value("${config.images.directory}")
    private String imagesLocation;

    private MagazinProductClient magazinProductClient;

    @Autowired
    public void setMagazinProductClient(MagazinProductClient magazinProductClient) {
        this.magazinProductClient = magazinProductClient;
    }

    @PreAuthorize("hasAuthority('manager')")
    @GetMapping("/create/filter")
    public String createFilter(Model model) {
        model.addAttribute("filter", new FilterTypeRequestDTO());

        return "admin/createFilter";
    }

    @PreAuthorize("hasAuthority('manager')")
    @PostMapping("/create/filter")
    public String addFilter(@ModelAttribute("filter") @Valid FilterTypeRequestDTO filter,
                            BindingResult result,
                            RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "redirect:/create/filter";
        }

        magazinProductClient.createFilter(filter);

        redirectAttributes.addFlashAttribute("message", "Filter " + filter.getName() + " was created.");

        return "redirect:/create/filter";
    }

    @PreAuthorize("hasAuthority('manager')")
    @GetMapping("/update/filter")
    public String updateFilter(Model model) {
        model.addAttribute("filterTypes", magazinProductClient.getAllFilters());

        return "/admin/updateFilter";
    }

    @GetMapping("/catalogue")
    public String categories(Model model) {
        model.addAttribute("departments", magazinProductClient.getDepartments());

        return "catalogue";
    }

    @PreAuthorize("hasAuthority('manager')")
    @GetMapping("/create/category")
    public String createCategory() {
        return "admin/createCategory";
    }

    @GetMapping("/categories/{id}")
    public String getProducts(@PathVariable int id, Model model) {
        model.addAttribute("id", id);

        return "/products";
    }


    @GetMapping("/products/{id}")
    public String productInfo(@PathVariable("id") int id, Model model) {
        ProductDTO product = magazinProductClient.getProductInfo(id);

        if (product == null) {
            return "redirect:/";
        }

        model.addAttribute("product", product);

        CartItemRequestDTO cartItemRequestDTO = new CartItemRequestDTO();
        cartItemRequestDTO.setProductId(id);

        model.addAttribute("cartItemRequestDTO", cartItemRequestDTO);

        return "product";
    }

    @PreAuthorize("hasAuthority('manager')")
    @GetMapping("/create/product")
    public String createProduct(Model model) {
        model.addAttribute("product", new ProductRequestDTO());
        model.addAttribute("departments", magazinProductClient.getDepartments());

        return "admin/createProduct";
    }

    @PreAuthorize("hasAuthority('manager')")
    @PostMapping("/create/product")
    public String addProduct(@RequestParam("file") MultipartFile[] files,
                             @ModelAttribute("product") @Valid ProductRequestDTO productRequestDTO,
                             BindingResult result,
                             Model model,
                             Exception exception) {
        if (result.hasErrors() || exception instanceof MaxUploadSizeExceededException) {
            return addProductError(model, productRequestDTO);
        }

        try {
            int id = magazinProductClient.addProduct(productRequestDTO);

            List<String> savedImages = new ArrayList<>();

            try {
                Path imgDirectoryPath = Paths.get(imagesLocation + ProductDTO.getImgDirectoryName(id, productRequestDTO.getName()));

                if (files.length != 0 && !imgDirectoryPath.toFile().exists()) {
                    Files.createDirectory(imgDirectoryPath);
                }

                for (MultipartFile file : files) {
                    String imageName = saveFile(imgDirectoryPath, file);

                    if (imageName != null) {
                        savedImages.add(imageName);
                    }
                }
            } catch (IOException e) {
                log.error("{}: Exception caught while saving photos: {}.", productRequestDTO.getName(), e.getMessage());

                try {
                    magazinProductClient.deleteProduct(id);

                    return addProductError(model, productRequestDTO);
                } catch (IllegalArgumentException err) {
                    return "redirect:/";
                }
            }

            if (savedImages.isEmpty()) {
                magazinProductClient.deleteProduct(id);

                return addProductError(model, productRequestDTO);
            }

            magazinProductClient.setImages(id, savedImages);

            return "redirect:/products/" + id;
        } catch (IllegalArgumentException e) {
            return addProductError(model, productRequestDTO);
        }
    }

    private String saveFile(@NotNull Path path, @NotNull MultipartFile file) throws IOException {
        if (file.getOriginalFilename().equals("")) {
            return null;
        }

        String filename = file.getOriginalFilename().replaceAll("[\"']", "");

        Path filePath = path.resolve(filename);

        Files.write(filePath, file.getBytes());

        if (ImageIO.read(filePath.toFile()) != null) {
            Set<PosixFilePermission> permissions = new HashSet<>();
            permissions.add(PosixFilePermission.OWNER_READ);
            permissions.add(PosixFilePermission.GROUP_READ);
            permissions.add(PosixFilePermission.OTHERS_READ);

            Files.setPosixFilePermissions(filePath, permissions);

            log.debug("{}: saved.", filePath.toAbsolutePath().toString());

            return filename;
        } else {
            Files.delete(filePath);

            log.debug("{}: Not an image.", filePath.getFileName().toString());

            return null;
        }
    }

    private String addProductError(Model model, ProductRequestDTO product) {
        product.setCategoryId(null);

        model.addAttribute("product", product);
        model.addAttribute("departments", magazinProductClient.getDepartments());

        return "admin/createProduct";
    }

    @PreAuthorize("hasAuthority('manager')")
    @PostMapping("/products/{id}/count")
    public String updateCount(@PathVariable("id") int id,
                              @RequestParam("leftCount") int leftCount,
                              RedirectAttributes redirectAttributes) {
        if (leftCount <= 0) {
            return "redirect:/products/" + id;
        }

        try {
            magazinProductClient.updateCount(id, leftCount);
        } catch (IllegalArgumentException e) {
            return "redirect:/";
        }

        return "redirect:/products/" + id;
    }

    @PreAuthorize("hasAuthority('manager')")
    @PostMapping("/products/{id}/price/general")
    public String updatePrice(@PathVariable("id") int id,
                              @RequestParam("price") int price,
                              RedirectAttributes redirectAttributes) {
        try {
            if (!magazinProductClient.updatePrice(id, price)) {
                redirectAttributes.addFlashAttribute("priceError", "Invalid price.");
            }
        } catch (IllegalArgumentException e) {
            return "redirect:/";
        }
        return "redirect:/products/" + id;
    }

    @PreAuthorize("hasAuthority('manager')")
    @PostMapping("/products/{id}/price/current")
    public String updateCurrentPrice(@PathVariable("id") int id,
                                     @RequestParam("currentPrice") int currentPrice,
                                     RedirectAttributes redirectAttributes) {
        try {
            if (!magazinProductClient.updateCurrentPrice(id, currentPrice)) {
                redirectAttributes.addFlashAttribute("currentPriceError", "Invalid price.");
            }
        } catch (IllegalArgumentException e) {
            return "redirect:/";
        }

        return "redirect:/products/" + id;
    }
}
