package com.tsystems.magazin.gateway.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

@Component
public class StringToDateConverter implements Converter<String, LocalDate> {
    @Override
    public LocalDate convert(String from) {
        try {
            return from.isEmpty() ? null : LocalDate.parse(from);
        } catch (DateTimeParseException e) {
            return null;
        }
    }
}
