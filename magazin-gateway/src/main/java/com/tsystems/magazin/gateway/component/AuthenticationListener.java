package com.tsystems.magazin.gateway.component;

import com.tsystems.magazin.domain.dto.request.CartItemRequestDTO;
import com.tsystems.magazin.gateway.client.MagazinOrderClient;
import com.tsystems.magazin.gateway.security.model.CustomUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class AuthenticationListener implements ApplicationListener<AuthenticationSuccessEvent> {
    private MagazinOrderClient magazinOrderClient;

    private SessionCart sessionCart;

    @Autowired
    public void setSessionCart(SessionCart sessionCart) {
        this.sessionCart = sessionCart;
    }

    @Autowired
    public void setMagazinOrderClient(MagazinOrderClient magazinOrderClient) {
        this.magazinOrderClient = magazinOrderClient;
    }

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent authenticationListener) {
        CustomUser user = (CustomUser) authenticationListener.getAuthentication().getPrincipal();

        log.info("{}: Logged in.", user.getUsername());

        List<CartItemRequestDTO> cartItemRequestDTOs = sessionCart.getProducts().values()
                .stream()
                .map(it -> {
                    CartItemRequestDTO cartItemRequestDTO = new CartItemRequestDTO();
                    cartItemRequestDTO.setCount(it.getCount());
                    cartItemRequestDTO.setProductId(it.getProductId());

                    return cartItemRequestDTO;
                })
                .collect(Collectors.toList());

       initUpdate(user.getId(), cartItemRequestDTOs);
    }

    @Async
    public void initUpdate(int accountId, List<CartItemRequestDTO> cartItemRequestDTOs) {
        try {
            magazinOrderClient.mergeCarts(accountId, cartItemRequestDTOs);
        } catch (IllegalArgumentException e) {
            log.warn("Account {}: Unable to merge carts.", accountId);
        } catch (Exception e) {
            log.error("Feign exception was thrown: {}.", e.getMessage());
        }
    }
}
