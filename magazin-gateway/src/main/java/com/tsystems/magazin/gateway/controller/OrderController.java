package com.tsystems.magazin.gateway.controller;

import com.tsystems.magazin.domain.dto.AddressDTO;
import com.tsystems.magazin.domain.dto.CartItemDTO;
import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.CartItemRequestDTO;
import com.tsystems.magazin.domain.dto.request.OrderRequestDTO;
import com.tsystems.magazin.domain.model.OrderStatus;
import com.tsystems.magazin.domain.model.PaymentMethod;
import com.tsystems.magazin.domain.model.PaymentStatus;
import com.tsystems.magazin.gateway.client.MagazinAccountClient;
import com.tsystems.magazin.gateway.client.MagazinOrderClient;
import com.tsystems.magazin.gateway.client.MagazinProductClient;
import com.tsystems.magazin.gateway.component.SessionCart;
import com.tsystems.magazin.gateway.security.model.CustomUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@Slf4j
public class OrderController {
    private MagazinProductClient magazinProductClient;

    private MagazinOrderClient magazinOrderClient;

    private MagazinAccountClient magazinAccountClient;

    private SessionCart sessionCart;

    @Autowired
    public void setMagazinProductClient(MagazinProductClient magazinProductClient) {
        this.magazinProductClient = magazinProductClient;
    }

    @Autowired
    public void setMagazinOrderClient(MagazinOrderClient magazinOrderClient) {
        this.magazinOrderClient = magazinOrderClient;
    }

    @Autowired
    public void setMagazinAccountClient(MagazinAccountClient magazinAccountClient) {
        this.magazinAccountClient = magazinAccountClient;
    }

    @Autowired
    public void setSessionCart(SessionCart sessionCart) {
        this.sessionCart = sessionCart;
    }

    @GetMapping(value = "/cart")
    public String cartGet(Model model, @AuthenticationPrincipal CustomUser user) {
        if (user == null) {
            model.addAttribute("cartItems", sessionCart.getProducts().values());
        } else {
            try {
                List<CartItemDTO> cartItems = magazinOrderClient.getCartItems(user.getId());

                if (cartItems == null) {
                    return "redirect:/";
                }

                model.addAttribute("cartItems", cartItems);
            } catch (Exception e) {
                log.error("Error getting details from order service: {}.", e.getMessage());
            }
        }

        return "account/cart";
    }

    @PostMapping(value = "/cart")
    public String cartPost(@Valid CartItemRequestDTO cartItemRequestDTO,
                           BindingResult result,
                           RedirectAttributes redirectAttributes,
                           @AuthenticationPrincipal CustomUser user) {
        if (result.hasErrors()) {
            return "redirect:/cart";
        }

        ProductDTO product = magazinProductClient.getProductInfo(cartItemRequestDTO.getProductId());

        if (product == null) {
            return "redirect:/cart";
        }

        if (user == null) {
            sessionCart.addCartItem(product, cartItemRequestDTO.getCount());

            log.debug("Unknown user: Put {} of {} in a cart.", cartItemRequestDTO.getCount(), product.getName());
        } else {
            try {
                magazinOrderClient.addCartItem(user.getId(), cartItemRequestDTO);

                log.debug("{}: Put {} of {} in a cart.", user.getUsername(), cartItemRequestDTO.getCount(), product.getName());
            } catch (IllegalArgumentException e) {
            }
        }

        return "redirect:/cart";
    }

    @PreAuthorize("hasAuthority('manager')")
    @GetMapping("/admin/orders")
    public String getOrders(Model model) {
        try {
            model.addAttribute("orders", magazinOrderClient.getAllOrders());
            model.addAttribute("paymentStatuses", PaymentStatus.values());
            model.addAttribute("orderStatuses", OrderStatus.values());
        } catch (Exception e) {
            log.error("");
        }

        return "admin/orders";
    }


    @PreAuthorize("isAuthenticated()")
    @GetMapping("/orders")
    public String ordersGet(Model model, @AuthenticationPrincipal CustomUser user) {
        try {
            model.addAttribute("orders", magazinOrderClient.getAccountOrders(user.getId()));

            try {
                Map<Integer, String> addressMap = magazinAccountClient.getAccountDetails(user.getId()).getAddresses()
                        .stream()
                        .collect(Collectors.toMap(AddressDTO::getId,
                                it -> it.getCity() + ", " + it.getStreet() + " h." + it.getHouse() + (it.getFlat() == null ? "" : ", " + it.getFlat())));

                model.addAttribute("addressMap", addressMap);
            } catch (Exception e) {
                log.error("Error getting details from account service: {}.", e.getMessage());
            }
        } catch (Exception e) {
            log.error("Error getting details from order service: {}.", e.getMessage());
        }

        return "account/orders";
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/checkout")
    public String checkoutGet(Model model, @AuthenticationPrincipal CustomUser user) {
        model.addAttribute("order", new OrderRequestDTO());
        model.addAttribute("addresses", magazinAccountClient.getAddresses(user.getId()));
        model.addAttribute("paymentMethods", PaymentMethod.values());

        return "account/checkout";
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/checkout")
    public String addOrder(@ModelAttribute("order") OrderRequestDTO order,
                           BindingResult result,
                           RedirectAttributes redirectAttributes,
                           @AuthenticationPrincipal CustomUser user) {
        if (result.hasErrors()) {
            return "redirect:/checkout";
        }

        order.setAccountId(user.getId());

        try {
            if (!magazinOrderClient.checkout(order)) {
                redirectAttributes.addFlashAttribute("message", "Something in your cart is out of stock.");

                return "redirect:/cart";
            }

        } catch (IllegalArgumentException e) {
            return "redirect:/cart";
        }

        return "redirect:/orders";
    }
}
