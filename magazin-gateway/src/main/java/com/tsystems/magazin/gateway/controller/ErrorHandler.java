package com.tsystems.magazin.gateway.controller;

import com.netflix.client.ClientException;
import feign.RetryableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

@ControllerAdvice
@Slf4j
public class ErrorHandler {
    @ExceptionHandler({SocketTimeoutException.class})
    public String processTimeOut(SocketTimeoutException e) {
        log.error("Timeout exception was thrown: {}.", e.getMessage());

        return "redirect:/serviceUnavailable";
    }

    @ExceptionHandler({ClientException.class})
    public String processClientException(ClientException e) {
        log.error("Client exception was thrown: {}.", e.getMessage());

        return "redirect:/serviceUnavailable";
    }

    @ExceptionHandler({ConnectException.class})
    public String connectionRefusedException(ConnectException e) {
        log.error("Connection refused: {}.", e.getMessage());

        return "redirect:/serviceUnavailable";
    }

    @ExceptionHandler({RetryableException.class})
    public String retryableException(ConnectException e) {
        log.error("Read timeout: {}.", e.getMessage());

        return "redirect:/serviceUnavailable";
    }
}
