package com.tsystems.magazin.gateway.controller.rest;

import com.tsystems.magazin.domain.dto.request.CategoryRequestDTO;
import com.tsystems.magazin.domain.dto.request.FilterValueRequestDTO;
import com.tsystems.magazin.gateway.client.MagazinProductClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class ProductRestController {
    private MagazinProductClient magazinProductClient;

    @Autowired
    public void setMagazinProductClient(MagazinProductClient magazinProductClient) {
        this.magazinProductClient = magazinProductClient;
    }

    @PreAuthorize("hasAuthority('manager')")
    @PostMapping("/filters/{id}/values")
    public ResponseEntity addValue(@PathVariable int id, @RequestBody @Valid FilterValueRequestDTO filterValueRequestDTO) {
        try {
            magazinProductClient.addFilterValue(id, filterValueRequestDTO);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAuthority('manager')")
    @PostMapping(value = "/categories")
    public ResponseEntity addCategory(@RequestBody @Valid CategoryRequestDTO categoryRequestDTO) {
        try {
            magazinProductClient.addCategory(categoryRequestDTO);

            return new ResponseEntity<>(HttpStatus.OK);

        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
