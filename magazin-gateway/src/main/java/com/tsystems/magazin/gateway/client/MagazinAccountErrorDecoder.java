package com.tsystems.magazin.gateway.client;

import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.stereotype.Component;

@Component
public class MagazinAccountErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String methodKey, Response response) {
        if (response.status() == 400) {
            return new IllegalArgumentException();
        }

        return new Default().decode(methodKey, response);
    }
}
