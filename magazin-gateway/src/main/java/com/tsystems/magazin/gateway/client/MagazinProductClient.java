package com.tsystems.magazin.gateway.client;

import com.tsystems.magazin.domain.dto.CategoryDTO;
import com.tsystems.magazin.domain.dto.FilterTypeDTO;
import com.tsystems.magazin.domain.dto.FilterValueDTO;
import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.CategoryRequestDTO;
import com.tsystems.magazin.domain.dto.request.FilterTypeRequestDTO;
import com.tsystems.magazin.domain.dto.request.FilterValueRequestDTO;
import com.tsystems.magazin.domain.dto.request.ProductRequestDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@FeignClient("magazin-product")
public interface MagazinProductClient {
    @GetMapping("/filters")
    List<FilterTypeDTO> getAllFilters();

    @PostMapping("/filters")
    void createFilter(@RequestBody @NotNull FilterTypeRequestDTO filter);

    @GetMapping("/filters/{id}/values")
    List<FilterValueDTO> getFilterValues(@PathVariable int id);

    @PostMapping("/filters/{id}/values")
    void addFilterValue(@PathVariable int id, @RequestBody @NotNull FilterValueRequestDTO filterValueRequestDTO);

    @GetMapping(value = "/departments")
    List<CategoryDTO> getDepartments();

    @GetMapping(value = "/categories")
    List<CategoryDTO> getCategoriesTree();

    @PostMapping(value = "/categories")
    void addCategory(@RequestBody @NotNull CategoryRequestDTO categoryRequestDTO);

    @GetMapping(value = "/categories/{id}/children")
    List<CategoryDTO> getChildren(@PathVariable("id") int categoryId);

    @GetMapping(value = "/categories/{id}/filters")
    List<FilterTypeDTO> getFilters(@PathVariable("id") int categoryId);

    @GetMapping(value = "/categories/{id}/products")
    List<ProductDTO> getProducts(@PathVariable("id") int categoryId);

    @PostMapping("/products")
    int addProduct(@RequestBody @NotNull ProductRequestDTO productRequestDTO);

    @DeleteMapping("/products")
    void deleteProduct(@RequestBody int productId);

    @GetMapping("/products/{id}")
    ProductDTO getProductInfo(@PathVariable("id") int productId);

    @PostMapping("/products/{id}/images")
    Boolean setImages(@PathVariable("id") int productId, @RequestBody @NotNull List<String> productImages);

    @PostMapping("/products/{id}/count")
    boolean updateCount(@PathVariable("id") int productId, @RequestBody int leftCount);

    @PostMapping("/products/{id}/price/general")
    boolean updatePrice(@PathVariable("id") int id, @RequestBody int price);

    @PostMapping("/products/{id}/price/current")
    boolean updateCurrentPrice(@PathVariable("id") int id, @RequestBody int currentPrice);

    @GetMapping("/stats/top")
    List<ProductDTO> getMostPopularProducts(@RequestParam("n") int n);

    @GetMapping("/stats/last")
    List<ProductDTO> getLastAddedProducts(@RequestParam("n") int n);

    @GetMapping("/stats/sales")
    List<ProductDTO> getBestSalesOffers(@RequestParam("n") int n);
}
