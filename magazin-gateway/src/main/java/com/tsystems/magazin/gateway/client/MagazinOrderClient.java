package com.tsystems.magazin.gateway.client;

import com.tsystems.magazin.domain.dto.CartItemDTO;
import com.tsystems.magazin.domain.dto.OrderDTO;
import com.tsystems.magazin.domain.dto.OrderItemDTO;
import com.tsystems.magazin.domain.dto.request.CartItemRequestDTO;
import com.tsystems.magazin.domain.dto.request.OrderRequestDTO;
import com.tsystems.magazin.domain.model.OrderStatus;
import com.tsystems.magazin.domain.model.PaymentStatus;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@FeignClient("magazin-order")
public interface MagazinOrderClient {
    @GetMapping("/carts/{id}/")
    List<CartItemDTO> getCartItems(@PathVariable("id") int accountId);

    @PostMapping("/carts/{id}/")
    void addCartItem(@PathVariable("id") int accountId, @RequestBody @NotNull CartItemRequestDTO cartItemRequestDTO);

    @DeleteMapping("/carts/{id}/")
    void removeCartItem(@PathVariable("id") int accountId, @RequestBody int productId);

    @PostMapping("/carts/{id}/merge")
    void mergeCarts(@PathVariable("id") int accountId, @RequestBody @NotNull List<CartItemRequestDTO> cartItemRequestDTO);

    @GetMapping("/account/{id}/orders")
    List<OrderDTO> getAccountOrders(@PathVariable("id") int accountId);

    @GetMapping("/orders")
    List<OrderDTO> getAllOrders();

    @PostMapping("/orders")
    Boolean checkout(@RequestBody @Valid OrderRequestDTO orderRequestDTO);

    @PostMapping("/orders/{id}/paymentStatus")
    void updatePaymentStatus(@PathVariable int id, @RequestBody PaymentStatus paymentStatus);

    @PostMapping("/orders/{id}/orderStatus")
    void updateOrderStatus(@PathVariable int id, @RequestBody OrderStatus orderStatus);

    @GetMapping("/account/{accountId}/orders/{id}")
    List<OrderItemDTO> getOrderItems(@PathVariable("id") int orderId, @PathVariable("accountId") int accountId);

    @GetMapping("/orders/{id}")
    List<OrderItemDTO> getOrderItems(@PathVariable("id") int orderId);
}
