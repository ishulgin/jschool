let id;

let activeId;

let productsStorage;

$(() => {
    id = $('#id').val();

    getChildren().pipe((children) => {
        displayCategories(children);
    });

    $('body').on('click', '.category', function () {
        const categoryId = $(this).attr('data-id');

        if (activeId !== categoryId) {
            activeId = categoryId;

            getFilters(categoryId).pipe(displayFilters);
            getProducts(categoryId).pipe((products) => {
                productsStorage = products.map((product) => {
                    return {
                        id: product.id,
                        name: product.name,
                        price: product.currentPrice,
                        priceString: product.priceString,
                        previewImage: product.previewImage,
                        imgDirectoryName: product.imgDirectoryName,
                        description: cutDescription(product.description, 60),
                        filterValues: product.filterValues,
                        filtersMap: product.filterValues.reduce(function (map, obj) {
                            map[obj.filterTypeId] = obj.id;
                            return map;
                        }, {})
                    }
                });

                displayProducts(productsStorage);
            });
        }
    }).on('click', '.list-group-item', function () {
        $('.fas', this)
            .toggleClass('fa-chevron-right')
            .toggleClass('fa-chevron-down');
    });
});

const cutDescription = (str, length) => {
    return (str.length <= length) ?
        str :
        str.substr(0, str.lastIndexOf(' ', length)) + '...';

};

const displayFilters = (filters) => {
    const root = $('#filters');

    root.html('');

    for (const filter of filters) {
        const filterRoot = $(`<select class='selectpicker' title='${filter.name}' data-id='${filter.id}' multiple>
                              </select>`);

        for (value of filter.values) {
            filterRoot.append(`<option value='${value.id}'>${value.value}</option>`)
        }

        root.append(filterRoot);
    }

    $('.selectpicker').selectpicker('render');

    $('select').on('changed.bs.select', function () {
        displayProducts(filterByPrice(filterByFilters(productsStorage)));
    });


    $("#apply").click(function () {
        displayProducts(filterByPrice(filterByFilters(productsStorage)))
    });
};

const filterByFilters = (storage) => {
    let storageDup = storage;

    $('select').each(function () {
        const values = $(this).val().map(it => parseInt(it));
        const filterTypeId = parseInt($(this).attr('data-id'));

        if (values.length !== 0) {
            storageDup = storageDup.filter((product) => {
                return values.includes(product.filtersMap[filterTypeId])
            });
        }
    });

    return storageDup;
};

const filterByPrice = (storage) => {
    let err = false;

    const fromField = $("#from");
    const fromInt = parseInt(fromField.val());

    if (fromField.val().length !== 0) {
        if (isNaN(fromInt)) {
            fromField.addClass("is-invalid");
            err = true;
        } else {
            fromField.removeClass("is-invalid");
        }
    }

    const toField = $("#to");
    const toInt = parseInt(toField.val());

    if (toField.val().length !== 0) {
        if (isNaN(toInt)) {
            toField.addClass("is-invalid");
            err = true;
        } else {
            toField.removeClass("is-invalid");
        }
    }

    const from = (val) => {
        return fromField.val().length === 0 || val >= fromInt;
    };

    const to = (val) => {
        return toField.val().length === 0 || val <= toInt;
    };

    if (err) {
        return storage;
    }

    return storage.filter(it => from(it.price) && to(it.price));
};

displayProducts = (products) => {
    const root = $('#products');

    if (products.length === 0) {
        root.html('<h5>No products.</h5>')
    } else {
        root.html('');

        for (const product of products) {
            const productCart = $(`<div class='card'>
                                      <img class='card-img-top' src="/images/${product.imgDirectoryName}${product.previewImage}">
                                      <div class='card-body'>
                                          <h5 class='card-title'><a href='/products/${product.id}'>${product.name}</a></h5>
                                          <p class="card-text">${product.priceString}</p>
                                          <p class='card-text'>${product.description}</p>
                                          <p class='card-text'>${getCharacteristicsTable(product.filterValues)}</p>
                                      </div>
                                   </div>`);

            root.append(productCart);
        }
    }
};

const getCharacteristicsTable = (characteristics) => {
    return `<table class="table text-left mb-0">
                <tbody>
                    ${getCharacteristicsTableBody(characteristics)}
                </tbody>
            </table>`;
};

const getCharacteristicsTableBody = (characteristics) => {
    return characteristics.map(it => `<tr>
                                <th>${it.filterTypeName}</th>
                                <td>${it.value}</td>
                               </tr>`).join("\n");
};

const slice = (array, size) => {
    const chunks = [];
    let i = 0;
    let n = array.length;

    while (i < n) {
        chunks.push(array.slice(i, i += size));
    }

    return chunks;
};

const displayCategories = (categoryTree) => {
    const root = $('#categoriesView');
    const listGroup = $(`<div class='list-group list-group-root card card-block'>
                            <input id='categoryField' type='text' class='form-control text-center' readonly value='Select a category'>
                         </div>`);

    if (categoryTree.length !== 0) {
        root.html(listGroup);

        for (const [index, category] of categoryTree.entries()) {
            if (category.children === null) {
                const node = $(`<div class='list-group-item list-group-item-action hover-0 category' data-id='${category.id}'>
                                    ${category.name}
                                </div>`);

                listGroup.append(node);
            } else {
                const node = $(`<a href='#category-${index}' id='${index}' class='list-group-item hover-0' data-toggle='collapse'>
                                    <i class='fas fa-chevron-right'></i>${category.name}
                                </a>`);

                listGroup.append(node);

                for (const child of processChildren(category, index, 2)) {
                    listGroup.append(child);
                }
            }
        }
    }
};

const processChildren = (data, id, depth) => {
    const node = $(`<div class='list-group collapse' id='category-${id}'></div>`);

    for (const [index, category] of data.children.entries()) {
        if (category.children === null) {
            node.append($(`<div style='padding-left: ${depth * 15}px' class='list-group-item list-group-item-action hover-0 category' data-id='${category.id}'>
                              ${category.name}
                           </div>`))
        } else {
            const realId = id + '-' + index;

            node.append(`<a href='#category-${realId}' style='padding-left: ${depth * 15}px'
                            class='list-group-item hover-0' data-toggle='collapse'>
                            <i class='fas fa-chevron-right'></i>${category.name}
                         </a>`);

            node.append(processChildren(category, realId, depth + 1));
        }
    }

    return node;
};

const getChildren = () => {
    return getCategoryInfoWrapper(id, 'children');
};


const getFilters = (id) => {
    return getCategoryInfoWrapper(id, 'filters');
};


const getProducts = (id) => {
    return getCategoryInfoWrapper(id, 'products');
};

const getCategoryInfoWrapper = (id, endpoint) => {
    return $.ajax({
        url: `/productApi/categories/${id}/${endpoint}`,
        type: 'GET'
    }).done((data) => {
        return data;
    }).fail(() => {
        location.reload();
    })
};