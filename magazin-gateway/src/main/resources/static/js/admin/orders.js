$(() => {
    $('#ordersTable').DataTable({
        searching: false,
        bInfo: false,
        bLengthChange: false,
        pageLength: 10,
        columnDefs: [{orderable: false, targets: [1, 2, 4, 5, 6]}],
        order: [[1, "desc"]]
    });

    $('body').on('click', '.order-details', function () {
        const orderId = $(this).val();

        getOrderItems(orderId).pipe((orderItems) => showOrderDetails(orderId, orderItems));
    }).on('click', '.address-details', function () {
        const addressId = $(this).val();

        getAddressDetails(addressId).pipe((addressDetails) => showAddressDetails(addressDetails));
    });

    $('select.payment-status').on('changed.bs.select', function (e) {
        const selected = $(e.currentTarget);

        updateOrder(selected.attr('data-id'), 'paymentStatus', selected.val());
    });

    $('select.order-status').on('changed.bs.select', function (e) {
        const selected = $(e.currentTarget);

        updateOrder(selected.attr('data-id'), 'orderStatus', selected.val());
    });
});

const showModal = (title, details) => {
    $('#modalTitle').text(title);

    $('#details').html(details);

    $('#modal').modal('show');
};

const showOrderDetails = (orderId, orderItems) => {
    const table = $(`<table class="table vertical-align table-striped text-left" id="orderItemsTable">
                        <thead>
                            <tr>
                                <th></th>
                            
                                <th>Product</th>
                            
                                <th>Count</th>
                            
                                <th>Total</th>
                            </tr>
                        </thead>
                            
                        <tbody>
                            
                        </tbody>
                      </table>`);

    const tbody = table.find('tbody');

    for (const orderItem of orderItems) {
        tbody.append(`<tr>
                        <td class="text-center">
                            <img class="preview-image" 
                            src='/images/${orderItem.product.imgDirectoryName}${orderItem.product.previewImage}'>
                        </td>

                        <td><a href='/products/${orderItem.product.id}'>${orderItem.product.name}</a></td>

                        <td>${orderItem.count}</td>

                        <td>${orderItem.actualPrice * orderItem.count} $</td>
                      </tr>`);
    }

    showModal(`Order #${orderId}`, table);
};

const showAddressDetails = (addressDetails) => {
    const table = $(`<table class="table vertical-align table-striped text-left">
                        <tbody>
                            <tr>
                                <th>Name</th>
                                <td>${addressDetails.name}</td>
                            </tr>
                            
                            <tr>
                                <th>Surname</th>
                                <td>${addressDetails.surname}</td>
                            </tr>
                                
                            <tr>
                                <th>Country</th>
                                <td>${addressDetails.country}</td>
                            </tr>
                            
                            <tr>
                                <th>City</th>
                                <td>${addressDetails.city}</td>
                            </tr>
                                
                            <tr>
                                <th>Postal Code</th>
                                <td>${addressDetails.postalCode}</td>
                            </tr>
                                
                            <tr>
                                <th>Street</th>
                                <td>${addressDetails.street}</td>
                            </tr>
                                
                            <tr>
                                <th>House</th>
                                <td>${addressDetails.house}</td>
                            </tr>
                                
                            <tr>
                                <th>Flat</th>
                                <td>${addressDetails.flat == null ? '&mdash;' : addressDetails.flat}</td>
                            </tr>
                        </tbody>
                      </table>`);

    showModal(`Address details`, table);
};

const getAddressDetails = (id) => {
    return getWrapper('addresses', id);
};

const getOrderItems = (id) => {
    return getWrapper('admin/orders', id);
};

const getWrapper = (endpoint, id) => {
    return $.ajax({
        url: `/${endpoint}/${id}`,
        type: 'GET'
    }).done((data) => {
        return data;
    }).fail(() => {
        location.reload();
    });
};


const updateOrder = (id, type, data) => {
    return $.ajax({
        url: `/orders/${id}/${type}`,
        headers: {"X-CSRF-TOKEN": $("meta[name='_csrf']").attr("content")},
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        type: 'POST'
    }).done((data) => {
        return data;
    }).fail(() => {
        location.reload();
    });
};