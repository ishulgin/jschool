const deleteFilterValue = (btn) => {
    btn.closest('tr').remove();
};

const addFilterValue = () => {
    const valueField = $('#value');

    if ((/^\s*$/).test(valueField.val())) {
        valueField.addClass('is-invalid');
    } else {
        valueField.removeClass('is-invalid');

        $('#filterValuesTable > tbody')
            .append(`<tr>
                        <td class='align-middle'>${valueField.val()}</td>

                        <td class='text-right align-middle px-0'>
                            <button class='btn btn-danger' type='button' onclick='deleteFilterValue($(this))'>
                                <i class='fas fa-trash-alt'></i>
                            </button>
                        </td>
                     </tr>`);

        valueField.val('');
    }
};

const submitForm = () => {
    if ($('#filterValuesTable > tbody tr').length < 2) {
        $('body').prepend(`<div class='alert alert-danger alert-dismissible fade show my-0' role='alert'>
                              You have to specify at least 2 filter's values.
                              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                              </button>
                           </div>`);

        return false;
    }

    $('#filterValuesTable > tbody tr td:first-child').each(function () {
        $('#form').append(`<input name='values' value='${$(this).html()}' type='hidden'>`)
    });
};