const departmentData = new Map();
const categoriesLoaded = new Map();

let chart;

$(() => {
    $('#yearStats').DataTable({
        searching: false,
        bInfo: false,
        bLengthChange: false,
        paging: false,
        columnDefs: [{orderable: false, targets: [1]}],
        order: [[0, "desc"]]
    });

    const graph = $('#graph');

    $('body').on('click', '.category-item', function () {
        const id = $(this).attr('id');

        if (!categoriesLoaded.has(id)) {
            getProductStat(id).pipe((productStats) => {
                categoriesLoaded.set(id, productStats);

                const root = $(`#category-${id}`);

                for (const productStat of categoriesLoaded.get(id)) {
                    root.append(`<div class='list-group-item list-group-item-action hover-0'>
                                    <a style="padding-left: 15px" href="/products/${productStat.productId}">${productStat.productName}</a>:
                                    ${productStat.count} units sold, profit - ${productStat.profit}$.
                                 </div>`)
                }
            });
        }

        $('.fas', this)
            .toggleClass('fa-chevron-right')
            .toggleClass('fa-chevron-down');
    });

    $('#modal').on('hidden.bs.modal', function () {
        categoriesLoaded.clear();
    });

    getDepartmentStatForLastWeek().pipe(processStats).pipe((stats) => {
        console.log(stats);

        const profitArray = stats.reduce((weeklyProfit, statRecord) => statRecord.data.map((dailyProfit, index) => dailyProfit + weeklyProfit[index]),
            Array(7).fill(0));
        const datesArray = getDatesArray();

        $('#statTable').DataTable({
            data: profitArray.map((profit, index) => [datesArray[index], `${profit}$`]),
            columns: [
                {title: "Day"},
                {title: "Profit"}
            ],
            footerCallback: function () {
                const api = this.api();

                const total = api
                    .column(1)
                    .data()
                    .reduce((acc, value) => acc + (parseInt(value.replace(/\$$/, ''))), 0);

                $(api.column(1).footer()).html(`${total}$`);
            },
            searching: false,
            paging: false,
            bInfo: false
        });

        chart = new Chart(graph, {
            type: 'bar',

            data: {
                labels: datesArray,
                datasets: stats
            },

            options: {
                scales: {
                    xAxes: [{stacked: true,}],
                    yAxes: [{stacked: true}]
                },
                onClick: handleClick
            }
        });
    });
});

const getDatesArray = () => {
    const result = [];

    for (let i = 0; i < 7; i++) {
        result[6 - i] = moment().subtract(i, 'days').format('YYYY-MM-DD');
    }

    return result;
};

const processStats = (records) => {
    const result = [];

    for (const record of records) {
        const dataSetValue = {
            label: record.departmentName,
            data: Array(7).fill(0),
            backgroundColor: getRandomColor()
        };

        for (const stat in record.stats) {
            const date = moment(stat);
            const dateString = date.format('YYYY-MM-DD');

            dataSetValue.data[6 - moment().diff(date, 'days')] = record.stats[stat].profit;

            if (departmentData.has(dateString)) {
                departmentData.get(dateString).set(record.departmentName, record.stats[stat].id);
            } else {
                const tmpMap = new Map();
                tmpMap.set(record.departmentName, record.stats[stat].id);

                departmentData.set(dateString, tmpMap);
            }
        }

        result.push(dataSetValue);
    }

    return result;
};

const getDepartmentStatForLastWeek = () => {
    return $.ajax({
        url: '/stats/week/departments',
        type: 'GET'
    }).done((data) => {
        return data;
    }).fail(() => {
        location.reload();
    })
};

const getRandomColor = () => {
    return 'rgb('
        + Math.round(Math.random() * 255) + ','
        + Math.round(Math.random() * 255) + ','
        + Math.round(Math.random() * 255)
        + ')';
};

const handleClick = (event) => {
    const element = chart.getElementAtEvent(event)[0]._model;

    const date = element.label;
    const name = element.datasetLabel;

    const id = departmentData.get(date).get(name);

    getCategoryStat(id).pipe((content) => showModal(name, date, content));
};

const showModal = (departmentName, date, categoryStats) => {
    $('#modalTitle').text(`${departmentName} for ${date}`);

    const root = $('#categoriesView');

    if (categoryStats.length === 0) {
        root.html('');
    } else {
        const listGroup = $(`<div class='list-group list-group-root card card-block'>
                             </div>`);

        for (const categoryStat of categoryStats) {
            console.log(categoryStat);

            listGroup.append(`<a href='#category-${categoryStat.id}' id='${categoryStat.id}' class='list-group-item hover-0 category-item' data-toggle='collapse'>
                                <i class='fas fa-chevron-right'></i>${categoryStat.categoryName}: ${categoryStat.count} units sold, profit - ${categoryStat.profit}$.
                              </a>
                              <div class='list-group collapse' id='category-${categoryStat.id}'>
                              </div>`);
        }

        root.html(listGroup);
    }

    $('#modal').modal('show');
};

const getCategoryStat = (id) => {
    return $.ajax({
        url: `/stats/week/departments/${id}`,
        type: 'GET'
    }).done((data) => {
        return data;
    }).fail(() => {
        location.reload();
    })
};

const getProductStat = (id) => {
    return $.ajax({
        url: `/stats/week/categories/${id}`,
        type: 'GET'
    }).done((data) => {
        return data;
    }).fail(() => {
        location.reload();
    })
};
