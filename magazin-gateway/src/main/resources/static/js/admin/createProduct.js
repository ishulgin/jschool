$(() => {
    $('.custom-file-input').change(function () {
        const fileName = $(this).val().split('\\').pop();

        $(this).siblings('.custom-file-label').addClass('selected').html(fileName);
    });

    $('.department').click(function () {
        $('#departmentField').val($(this).text());

        clearFilters();

        getCategoryTree($(this).val())
            .pipe(displayCategories);
    });
});

const displayCategories = (categoryTree) => {
    const root = $('#categoriesView');
    const listGroup = $(`<div class='list-group list-group-root card card-block'>
                            <input id='categoryField' type='text' class='form-control text-center' readonly
                            value="Select a category">
                         </div>`);

    if (categoryTree.length !== 0) {
        root.html(listGroup);

        for (const [index, category] of categoryTree.entries()) {
            const node = $(`<a href='#category-${index}' class='list-group-item hover-0' data-toggle='collapse'>
                                <i class='fas fa-chevron-right'></i>${category.name}
                            </a>`);

            listGroup.append(node);

            for (const child of processChildren(category, index, 2)) {
                listGroup.append(child);
            }
        }
    } else {
        root.html('');
    }

    $('.category').click(function () {
        $('#categoryField').val($(this).text());

        $('#categoryId').val($(this).val());

        clearFilters();

        getFilters($(this).val())
            .pipe(displayFilters);
    });

    $('#categoryId').val('');

    $('.list-group-item').click(function () {
        $('.fas', this)
            .toggleClass('fa-chevron-right')
            .toggleClass('fa-chevron-down');
    });
};

const processChildren = (data, id, depth) => {
    const node = $(`<div class='list-group collapse' id='category-${id}'></div>`);

    for (const [index, category] of data.children.entries()) {
        if (category.children === null) {
            node.append($(`<button type='button' style='padding-left: ${depth * 15}px' 
                            class='list-group-item list-group-item-action hover-0 category' value='${category.id}'>
                              ${category.name}
                           </button>`))
        } else {
            node.append(`<a href='#category-${id + '-' + index}' style='padding-left: ${depth * 15}px'
                            class='list-group-item hover-0' data-toggle='collapse'>
                            <i class='fas fa-chevron-right'></i>${category.name} 
                         </a>`);

            node.append(processChildren(category, id + '-' + index, depth + 1));
        }
    }

    return node;
};

const displayFilters = (filters) => {
    const root = $('#filtersView');

    clearFilters();

    for (const [index, filterType] of filters.entries()) {
        const filterValues = renderFilterValues(filterType, index);

        root.append(`<div class="form-group">
                        <div class="input-group">
                           <input class="form-control" id="filterField${index}" type="text" style="z-index: 3" readonly
                           value="Select ${filterType.name}">
                           
                           <div class="input-group-append">
                              <button type="button" class="btn input-group-text rounded-right dropdown-toggle"
                                      data-toggle="dropdown" aria-haspopup="true"
                                      aria-expanded="false">
                                  ${filterType.name}
                              </button>
                              
                              <div class="dropdown-menu">
                                 ${filterValues.html()}
                              </div>
                           </div>
                        </div>
                        
                        <input name="filtersIds" id="filtersIds${index}" type="hidden">
                     </div>`);

        $(`.filter-${index}`).click(function () {
            $(`#filterField${index}`).val($(this).text());

            $(`#filtersIds${index}`).val($(this).val());
        });
    }
};

const renderFilterValues = (filterType, index) => {
    const root = $(`<div class="dropdown-menu"></div>`);

    for (const filterValue of filterType.values) {
        root.append($(`<button class="dropdown-item filter-${index}" type="button"
                            value="${filterValue.id}">${filterValue.value}</button>`));
    }

    return root;
};

const clearFilters = () => {
    $('#filtersView').html('');
};

const getCategoryTree = (id) => {
    return ajaxWrapper(id, 'children');
};

const getFilters = (id) => {
    return ajaxWrapper(id, 'filters');
};

const ajaxWrapper = (id, endpoint) => {
    return $.ajax({
        url: `/productApi/categories/${id}/${endpoint}`,
        type: 'GET'
    }).done((data) => {
        return data;
    }).fail(() => {
        location.reload();
    })
};