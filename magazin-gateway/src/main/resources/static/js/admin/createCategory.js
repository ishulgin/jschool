$(() => {
    getCategoryTree().pipe(displayCategories);

    getFilterTypes().pipe(displayFilters);

    const body = $('body');

    body.on('click', '.real', function (e) {
        showReal();

        $('#parentId').val($(this).attr('id'));

        e.stopPropagation();
    });

    body.on('click', '.abstract', function (e) {
        showAbstract();

        $('#parentId').val($(this).attr('id'));

        e.stopPropagation();
    });

    body.on('click', '#department', function (e) {
        showDepartment();

        e.stopPropagation();
    });

    body.on('click', '.list-group-item', function (e) {
        $('.fas', this)
            .toggleClass('fa-chevron-right')
            .toggleClass('fa-chevron-down');
    });

    body.tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    $('#close').click(function (e) {
        $("#alert").hide();

        e.stopPropagation();
    });

    $('#modal').on('hidden.bs.modal', function () {
        const categoryFilters = $('#categoryFilters');

        categoryFilters.val('default');
        categoryFilters.selectpicker('refresh');

        $('#name').val('').removeClass('is-invalid');
        $('#parentId').val('');

        $("#alert").hide();
    });

    $('#submit').on('click', function () {
        let valid = true;

        const nameField = $('#name');
        const name = nameField.val();
        const filters = $('#categoryFilters').val();

        if (name.length >= 4 && name.length <= 64) {
            nameField.removeClass('is-invalid');
        } else {
            nameField.addClass('is-invalid');

            valid = false;
        }

        if ($('#categoryFiltersWrapper').is(":visible") && filters.length === 0) {
            $('.bootstrap-select').addClass('is-invalid');

            valid = false;
        } else {
            $('.bootstrap-select').removeClass('is-invalid');
        }

        if (valid) {
            sendCategory($('#parentId').val(), name, filters).pipe(getCategoryTree).pipe(displayCategories);

            $('#name').val('');
            $('#alert').show();
        }
    });
});

const showDepartment = () => {
    showModal('Add department', false)
};

const showAbstract = () => {
    showModal('Add abstract category', false)
};

const showReal = () => {
    showModal('Add real category', true);
};

const showModal = (modalTitle, showFilters) => {
    $('#modalTitle').text(modalTitle);

    if (showFilters === false) {
        $('#categoryFiltersWrapper').hide();
    } else {
        $('#categoryFiltersWrapper').show();
    }

    $('#modal').modal('show');
};

const displayFilters = (filters) => {
    const root = $('#categoryFilters');

    for (const filter of filters) {
        root.append(`<option value='${filter.id}'>${filter.name}</option>`)
    }

    root.selectpicker('refresh');
};

const displayCategories = (categoryTree) => {
    const root = $('#categoriesView');
    const listGroup = $(`<div class='list-group list-group-root card card-block'>
                            <input id='categoryField' type='text' class='form-control text-center' readonly value='Select a category'>
                         </div>`);

    if (categoryTree.length !== 0) {
        root.html(listGroup);

        for (const [index, category] of categoryTree.entries()) {
            const node = $(`<a href='#category-${index}' id='${index}' class='list-group-item hover-0' data-toggle='collapse'>
                                <i class='fas fa-chevron-right'></i>${category.name}
                                
                                ${abstractCategory(category.id)} 
                            </a>`);

            listGroup.append(node);

            for (const child of processChildren(category, index, 2)) {
                listGroup.append(child);
            }
        }
    } else {
        root.html();
    }
};

const processChildren = (data, id, depth) => {
    const node = $(`<div class='list-group collapse' id='category-${id}'></div>`);

    for (const [index, category] of data.children.entries()) {
        if (category.children === null) {
            node.append($(`<div style='padding-left: ${depth * 15}px' class='list-group-item list-group-item-action hover-0'>
                              ${category.name}
                           </div>`))
        } else {
            const realId = id + '-' + index;

            const buttons = (category.children.length === 0) ?
                bothCategories(category.id) :
                (category.children.every((element) => {
                    return element.children !== null
                }) ? abstractCategory(category.id) : realCategory(category.id));

            node.append(`<a href='#category-${realId}' style='padding-left: ${depth * 15}px'
                            class='list-group-item hover-0' data-toggle='collapse'>
                            <i class='fas fa-chevron-right'></i>${category.name}
                            
                            ${buttons}
                         </a>`);

            node.append(processChildren(category, realId, depth + 1));
        }
    }

    return node;
};

const bothCategories = (id) => {
    return `<div class='btn-group float-right'>
                ${abstractCategory(id) + realCategory(id)}
            </div>`;
};

const abstractCategory = (id) => {
    return categorySpan(id, 'btn-success', 'abstract');
};

const realCategory = (id) => {
    return categorySpan(id, 'btn-primary', 'real');
};

const categorySpan = (id, buttonType, category) => {
    return `<span id='${id}' class='btn ${buttonType} ${category} float-right' data-toggle='tooltip' 
                data-placement='top' title='Add ${category} category'>
                <i class='fas fa-plus m-0'></i>
            </span>`;
};

const getFilterTypes = () => {
    return getWrapper('/productApi/filters');
};

const getCategoryTree = () => {
    return getWrapper('/productApi/categories');
};

const sendCategory = (parentId, name, filters) => {
    return $.ajax({
        url: '/categories',
        headers: {'X-CSRF-TOKEN': $('meta[name="_csrf"]').attr('content')},
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            parentId: parentId,
            name: name,
            filterTypesIds: filters
        })
    }).done((data) => {
        return data;
    }).fail(() => {
        location.reload();
    })
};

const getWrapper = (url) => {
    return $.ajax({
        url: url,
        type: 'GET'
    }).done((data) => {
        return data;
    }).fail(() => {
        location.reload();
    })
};