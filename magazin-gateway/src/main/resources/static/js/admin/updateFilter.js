$(() => {
    $(".filter").click(function () {
        $("#modalTitle").text($(this).text());

        $("#id").val($(this).val());

        getValues($(this).val()).pipe(showFilterValues);

        $("#modal").modal("show");
    });

    $("#updateFilter").click(function () {
        const valueField = $("#value");
        const value = valueField.val();

        if (value.length === 0) {
            valueField.addClass('is-invalid');
        } else {
            valueField.removeClass('is-invalid');

            addValue($("#id").val(), value);
        }
    });
});

const showFilterValues = (values) => {
    const filterValuesList = $("#filterValues");
    filterValuesList.html('');

    for (const value of values) {
        filterValuesList.append(`<div class='text-center list-group-item'>
                                    ${value.value}
                                 </div>`);
    }
};

const addValue = (id, value) => {
    return $.ajax({
        url: `/filters/${id}/values`,
        headers: {'X-CSRF-TOKEN': $('meta[name="_csrf"]').attr('content')},
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            value: value
        })
    }).done((data) => {
        $("#filterValues").append(`<div class='text-center list-group-item'>
                                     ${value}
                                   </div>`);

        return data;
    }).fail(() => {
        location.reload();
    })
};

const getValues = (id) => {
    return $.ajax({
        url: `/productApi/filters/${id}/values`,
        type: 'GET'
    }).done((data) => {
        return data;
    }).fail(() => {
        location.reload();
    })
};