$(() => {
    $('#addressTable').DataTable({
        searching: false,
        paging: false,
        bInfo: false,
        columnDefs: [{orderable: false, targets: [4, 5]}]
    });
});