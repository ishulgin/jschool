$(() => {
    checkRadio('addressId');
    checkRadio('paymentMethod');
});

const checkRadio = (groupName) => {
    $(`input:radio[name=${groupName}]:first`).attr('checked', true);
};