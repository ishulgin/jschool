$(() => {
    $('#ordersTable').DataTable({
        searching: false,
        bInfo: false,
        bLengthChange: false,
        pageLength: 10,
        columnDefs: [{orderable: false, targets: [1, 2, 6]}],
        order: [[1, "desc"]]
    });

    $('body').on('click', '.order-details', function () {
        const orderId = $(this).val();

        getOrderItems(orderId).pipe((orderItems) => showOrderDetails(orderId, orderItems));
    });
});

const showOrderDetails = (orderId, orderItems) => {
    $('#modalTitle').text(`Order #${orderId}`);

    const tbody = $('#orderItemsTable > tbody');
    tbody.html('');

    for (const orderItem of orderItems) {
        tbody.append(`<tr>
                         <td class='text-center'>
                            <img class='preview-image' 
                                src='/images/${orderItem.product.imgDirectoryName}${orderItem.product.previewImage}'>
                            </td>

                         <td><a href='/products/${orderItem.product.id}'>${orderItem.product.name}</a></td>

                         <td>${orderItem.count}</td>

                         <td>${orderItem.actualPrice * orderItem.count} $</td>
                      </tr>`);
    }

    $('#modal').modal('show');
};

const getOrderItems = (id) => {
    return $.ajax({
        url: `/orders/${id}`,
        type: 'GET'
    }).done((data) => {
        return data;
    }).fail(() => {
        location.reload();
    });
};