deleteItem = (id, btn) => {
    $.ajax({
        url: '/cart',
        headers: {"X-CSRF-TOKEN": $("meta[name='_csrf']").attr("content")},
        type: 'DELETE',
        contentType: 'application/json; charset=utf-8',
        data: id.toString(),
        statusCode: {
            200: (data) => {
                btn.closest("tr").remove();

                if ($("#cartTable tbody").children().length === 0) {
                    $("#tableArea").remove();
                    $("#cardBody").html("<h1>Empty cart.</h1>");
                }
            },
            400: () => {
                location.reload();
            }
        }
    })
};