let previousValue = 1;
let previousUpdateValue = 1;

$(() => {
    const totalCount = parseInt($("#leftCount").text());

    $("#plus").click(function () {
        if ($("#count").val().length === 0) {
            $("#count").val('1');
        } else {
            if ($("#count").val() < totalCount) {
                $("#count").get(0).value++;
            }
        }
    });

    $("#minus").click(function () {
        if ($("#count").val().length === 0) {
            $("#count").val('1');
        } else {
            if ($("#count").val() > 1) {
                $("#count").get(0).value--;
            }
        }

    });

    $("#count").keypress(function () {
        const val = parseInt($(this).val());

        if (!isNaN(val) && val >= 1 && val <= totalCount) {
            previousValue = val;
        }
    }).keyup(function () {
        const value = $(this).val();

        if (value.length === 0) {
            $(this).val('');
        } else if (!isNaN(value)) {
            const parsed = parseInt(value);

            if (parsed < 1 || parsed > totalCount) {
                $(this).val(previousValue);
            } else {
                $(this).val(parsed);
            }
        } else {
            $(this).val(previousUpdateValue);
        }
    });

    $("#plusUpdate").click(function () {
        if ($("#updateLeftCount").val().length === 0) {
            $("#updateLeftCount").val('1');
        } else {
            $("#updateLeftCount").get(0).value++;
        }
    });

    $("#minusUpdate").click(function () {
        if ($("#updateLeftCount").val().length === 0) {
            $("#updateLeftCount").val('1');
        } else {
            if ($("#updateLeftCount").val() > 1) {
                $("#updateLeftCount").get(0).value--;
            }
        }
    });

    $("#updateLeftCount").keypress(function () {
        const val = parseInt($(this).val());

        if (!isNaN(val) && val >= 1) {
            previousUpdateValue = val;
        }
    }).keyup(function () {
        const value = $(this).val();

        if (value.length === 0) {
            $(this).val('');
        } else if (!isNaN(value)) {
            const parsed = parseInt($(this).val());

            if (parsed < 1) {
                $(this).val(previousUpdateValue);
            } else {
                $(this).val(parsed);
            }
        } else {
            $(this).val(previousUpdateValue);
        }
    });
});