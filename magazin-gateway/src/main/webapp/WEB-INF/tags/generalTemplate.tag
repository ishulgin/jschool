<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<%@ attribute name="include" fragment="true" %>
<%@ attribute name="title" fragment="true" %>
<%@ attribute name="alert" fragment="true" %>

<html>
<head>
    <meta charset="utf-8">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css"
          integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css">

    <link rel="stylesheet" type="text/css" href="<c:url value="/css/general.css"/>">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
            integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
            crossorigin="anonymous"></script>

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>

    <script src="<c:url value="/js/general.js"/>"></script>
    <jsp:invoke fragment="include"/>

    <title>
        <jsp:invoke fragment="title"/>
    </title>
</head>

<body>
<jsp:invoke fragment="alert"/>

<header>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="<c:url value="/"/>">Magazin</a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/"/>">Home</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/catalogue"/>">Catalogue</a>
                    </li>
                </ul>

                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/cart"/>"><i class="fas fa-shopping-cart"></i> Cart</a>
                    </li>

                    <li class="nav-item">
                        <sec:authorize access="isAnonymous()">
                            <a class="nav-link" href="<c:url value="/login"/>"><i class="fas fa-sign-in-alt"></i> Sign
                                In</a>
                        </sec:authorize>

                        <sec:authorize access="isAuthenticated()">
                            <div class="dropdown">
                                <a class="nav-link" id="profileMenu" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">
                                    <i class="fas fa-user"></i> Profile
                                </a>

                                <div class="dropdown-menu" aria-labelledby="profileMenu">
                                    <a class="dropdown-item" href="<c:url value="/profile"/>">My data</a>

                                    <a class="dropdown-item" href="<c:url value="/orders"/>">Orders</a>

                                    <div class="dropdown-divider"></div>

                                    <form class="my-0" action="<c:url value="/logout"/>" method="POST" id="logout">
                                        <a class="dropdown-item" href="javascript:{}"
                                           onclick="document.getElementById('logout').submit();">
                                            Log out
                                        </a>

                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    </form>
                                </div>
                            </div>
                        </sec:authorize>
                    </li>

                    <sec:authorize access="hasAuthority('manager')">
                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="nav-link" id="adminMenu" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">
                                    <i class="fas fa-wrench"></i> Administration
                                </a>

                                <div class="dropdown-menu" aria-labelledby="adminMenu">
                                    <a class="dropdown-item" href="<c:url value="/create/product"/>">Add product</a>

                                    <a class="dropdown-item" href="<c:url value="/create/category"/>">Add category</a>

                                    <a class="dropdown-item" href="<c:url value="/create/filter"/>">Add filter</a>

                                    <a class="dropdown-item" href="<c:url value="/update/filter"/>">Update filter</a>

                                    <div class="dropdown-divider"></div>

                                    <a class="dropdown-item" href="<c:url value="/admin/orders"/>">Orders</a>

                                    <a class="dropdown-item" href="<c:url value="/stats"/>">Statistic</a>
                                </div>
                            </div>
                        </li>
                    </sec:authorize>
                </ul>
            </div>
        </div>
    </nav>
</header>

<main role="main" class="container">
    <div class="card bg-light">
        <jsp:doBody/>
    </div>
</main>

<footer class="footer">
    <div class="container">
        :thinking:
    </div>
</footer>

<sec:csrfMetaTags/>
</body>
</html>
