<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>

<t:authTemplate>
    <jsp:attribute name="title">
        Sign Up
    </jsp:attribute>

    <jsp:body>
        <c:url var="changeDetails" value="/profile/details"/>

        <form:form action="${changeDetails}" modelAttribute="accountDetails" method="POST">
            <c:set var="nameError">
                <form:errors path="name"/>
            </c:set>

            <c:set var="surnameError">
                <form:errors path="surname"/>
                <form:errors/>
            </c:set>

            <c:set var="dateOfBirthError">
                <form:errors path="dateOfBirth"/>
                <form:errors/>
            </c:set>

            <div class="form-group">
                <form:input required="required" path="name"
                               cssClass='form-control ${not empty nameError ? "is-invalid" : ""}'/>
                <div class="invalid-feedback is-invalid">${nameError}</div>
            </div>

            <div class="form-group">
                <form:input required="required" path="surname"
                               cssClass='form-control ${not empty surnameError ? "is-invalid" : ""}'/>
                <div class="invalid-feedback is-invalid">${surnameError}</div>
            </div>

            <div class="form-group">
                <form:input required="required" path="dateOfBirth"
                               cssClass='form-control ${not empty dateOfBirthError ? "is-invalid" : ""}'/>
                <div class="invalid-feedback is-invalid">${dateOfBirthError}</div>
            </div>

            <div class="form-group text-center">
                <button type="submit" class="btn btn-outline-success">Change Details</button>
            </div>
        </form:form>

        <div class="text-center">
            Changed your mind? <a href="<c:url value='/profile'/>">Back to profile</a>
        </div>
    </jsp:body>
</t:authTemplate>
