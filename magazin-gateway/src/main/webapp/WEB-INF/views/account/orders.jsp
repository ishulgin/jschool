<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>

<t:generalTemplate>
    <jsp:attribute name="include">
        <script src="<c:url value="/js/account/orders.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="title">
        My Orders
    </jsp:attribute>

    <jsp:body>
        <div class="card-body container-fluid">

            <c:if test="${!empty orders}">
                <table id="ordersTable" class="table vertical-align table-striped text-left" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>

                        <th>Order Time</th>

                        <c:if test="${!empty addressMap}">
                            <th>Shipping Addr.</th>
                        </c:if>

                        <th>Payment Method</th>

                        <th>Payment Status</th>

                        <th>Status</th>

                        <th>Additional</th>
                    </tr>
                    </thead>

                    <tbody>
                    <c:forEach items="${orders}" var="order">
                        <tr>
                            <td>${order.id}</td>

                            <td><javatime:format value="${order.orderedAt}" pattern="yyyy-MM-dd HH:mm"/></td>

                            <c:if test="${!empty addressMap}">
                                <td>
                                    ${addressMap[order.addressId]}
                                </td>
                            </c:if>

                            <td>${order.paymentMethod}</td>

                            <td>${order.paymentStatus}</td>

                            <td>${order.orderStatus}</td>

                            <td>
                                <button type="button" class="btn btn-link p-0 order-details" value="${order.id}">
                                    Details
                                </button>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>

            <c:if test="${empty orders}">
                <h1 class="text-center">You have no orders.</h1>
            </c:if>
        </div>

        <div class="modal fade" id="modal" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalTitle"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="form-group" id="orderDetails">
                            <table class="table vertical-align table-striped text-left" id="orderItemsTable">
                                <thead>
                                <tr>
                                    <th></th>

                                    <th>Product</th>

                                    <th>Count</th>

                                    <th>Total</th>
                                </tr>
                                </thead>

                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:generalTemplate>