<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generalTemplate>
    <jsp:attribute name="title">
        Add Address
    </jsp:attribute>

    <jsp:body>
        <div class="card-body mx-auto" style="width: 400px;">
            <c:url var="createAdress" value="/profile/address"/>

            <form:form method="POST" action="${createAdress}" modelAttribute="address">
                <c:set var="cityError">
                    <form:errors path="city"/>
                </c:set>

                <c:set var="countryError">
                    <form:errors path="country"/>
                </c:set>

                <c:set var="postalCodeError">
                    <form:errors path="postalCode"/>
                </c:set>

                <c:set var="streetError">
                    <form:errors path="street"/>
                </c:set>

                <c:set var="houseError">
                    <form:errors path="house"/>
                </c:set>

                <c:set var="flatError">
                    <form:errors path="flat"/>
                </c:set>

                <div class="form-group">
                    <form:input required="required" path="country"
                                cssClass='form-control ${not empty countryError ? "is-invalid" : ""}'
                                placeholder="Country"/>
                    <div class="invalid-feedback">${countryError}</div>
                </div>

                <div class="form-group">
                    <form:input required="required" path="city"
                                cssClass='form-control ${not empty cityError ? "is-invalid" : ""}'
                                placeholder="City"/>
                    <div class="invalid-feedback">${cityError}</div>
                </div>

                <div class="form-group">
                    <form:input required="required" path="postalCode"
                                cssClass='form-control ${not empty postalCodeError ? "is-invalid" : ""}'
                                placeholder="Postal Code"/>
                    <div class="invalid-feedback">${postalCodeError}</div>
                </div>

                <div class="form-group">
                    <form:input required="required" path="street"
                                cssClass='form-control ${not empty streetError ? "is-invalid" : ""}'
                                placeholder="Street"/>
                    <div class="invalid-feedback">${streetError}</div>
                </div>

                <div class="form-group">
                    <form:input required="required" path="house"
                                cssClass='form-control ${not empty houseError ? "is-invalid" : ""}'
                                placeholder="House Number"/>
                    <div class="invalid-feedback">${houseError}</div>
                </div>

                <div class="form-group">
                    <form:input path="flat"
                                cssClass='form-control ${not empty flatError ? "is-invalid" : ""}'
                                placeholder="Flat Number"/>
                    <div class="invalid-feedback">${flatError}</div>
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-outline-success">Add Address</button>
                </div>
            </form:form>
        </div>
    </jsp:body>
</t:generalTemplate>