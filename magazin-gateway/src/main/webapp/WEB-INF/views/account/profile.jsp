<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>

<t:generalTemplate>
    <jsp:attribute name="include">
        <script src="<c:url value="/js/account/profile.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="title">
        Profile
    </jsp:attribute>

    <jsp:attribute name="alert">
        <c:if test="${not empty message}">
        <div class="alert alert-success alert-dismissible fade show my-0" role="alert">
                ${message}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        </c:if>
    </jsp:attribute>

    <jsp:body>
        <div class="card-body container-fluid">
            <div class="row">
                <div class="col-5">
                    <h2>
                        About

                        <a href="<c:url value="/profile/details"/>"><i class="fas fa-edit"></i></a>
                    </h2>

                    <table class="table text-left">
                        <tbody>
                        <tr>
                            <th scope="row">Name</th>
                            <td>${account.name}</td>
                        </tr>

                        <tr>
                            <th scope="row">Surname</th>
                            <td>${account.surname}</td>
                        </tr>

                        <tr>
                            <th scope="row">Date of birth</th>
                            <td>${account.dateOfBirth}</td>
                        </tr>
                        </tbody>
                    </table>

                    <div class="text-center">
                        Btw, you can change your password <a href="<c:url value='/profile/password'/>">here</a>.
                    </div>
                </div>

                <div class="col-7">
                    <h2>Addresses</h2>

                    <c:if test="${!empty account.addresses}">
                        <table class="table text-left" id="addressTable" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Country</th>

                                <th>City</th>

                                <th>Postal code</th>

                                <th>Street</th>

                                <th>House</th>

                                <th>Flat</th>
                            </tr>
                            </thead>

                            <tbody>
                            <c:forEach items="${account.addresses}" var="address">
                                <tr>
                                    <td>${address.country}</td>

                                    <td>${address.city}</td>

                                    <td>${address.postalCode}</td>

                                    <td>${address.street}</td>

                                    <td>${address.house}</td>

                                    <td>${address.flat != null ? address.flat : "&mdash;"}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>

                        <div class="text-center">
                            You can add more addresses by clicking <a href="<c:url value='/profile/address'/>">here</a>.
                        </div>
                    </c:if>

                    <c:if test="${empty account.addresses}">
                        You have no shipping addresses <i class="far fa-frown"></i>.
                        <br>
                        But you can create them by clicking <a href="<c:url value='/profile/address'/>">here</a>.
                    </c:if>
                </div>
            </div>
        </div>
    </jsp:body>
</t:generalTemplate>