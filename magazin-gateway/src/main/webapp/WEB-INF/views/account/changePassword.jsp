<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>

<t:authTemplate>
    <jsp:attribute name="title">
        Sign Up
    </jsp:attribute>

    <jsp:body>
        <c:url var="changePassword" value="/profile/password"/>

        <form:form action="${changePassword}" modelAttribute="passwordRequestDTO" method="POST">
            <c:set var="oldPasswordError">
                <form:errors path="oldPassword"/>
            </c:set>

            <c:set var="passwordError">
                <form:errors path="password"/>
                <form:errors/>
            </c:set>

            <div class="form-group">
                <form:password required="required" path="oldPassword"
                               cssClass='form-control ${not empty oldPasswordError ? "is-invalid" : ""}'
                               placeholder="Previous Password"/>
                <div class="invalid-feedback is-invalid">${oldPasswordError}</div>
            </div>

            <div class="form-group">
                <form:password required="required" path="password"
                               cssClass='form-control ${not empty passwordError ? "is-invalid" : ""}'
                               placeholder="New Password"/>
                <div class="invalid-feedback is-invalid">${passwordError}</div>
            </div>

            <div class="form-group">
                <form:password required="required" path="passwordConfirmation"
                               cssClass='form-control ${not empty passwordError ? "is-invalid" : ""}'
                               placeholder="Confirm New Password"/>
            </div>

            <div class="form-group text-center">
                <button type="submit" class="btn btn-outline-success">Change Password</button>
            </div>
        </form:form>

        <div class="text-center">
            Changed your mind? <a href="<c:url value='/profile'/>">Back to profile</a>
        </div>
    </jsp:body>
</t:authTemplate>
