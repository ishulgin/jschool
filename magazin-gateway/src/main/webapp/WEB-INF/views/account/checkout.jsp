<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>

<t:generalTemplate>
    <jsp:attribute name="title">
        Checkout
    </jsp:attribute>

    <jsp:attribute name="include">
        <script src="<c:url value="/js/account/checkout.js"/>"></script>
    </jsp:attribute>

    <jsp:body>
        <div class="card-body mx-auto">
            <c:url var="checkout" value="/checkout"/>

            <form:form method="POST" action="${checkout}" modelAttribute="order">
                <c:if test="${not empty addresses}">
                    <div class="form-group">
                        <table class="table text-left">
                            <thead>
                            <tr>
                                <th>Country</th>

                                <th>City</th>

                                <th>Postal code</th>

                                <th>Street</th>

                                <th>House</th>

                                <th>Flat</th>

                                <th>Select</th>
                            </tr>
                            </thead>

                            <tbody>
                            <c:forEach items="${addresses}" var="address">
                                <tr>
                                    <td>${address.country}</td>

                                    <td>${address.city}</td>

                                    <td>${address.postalCode}</td>

                                    <td>${address.street}</td>

                                    <td>${address.house}</td>

                                    <td>${address.flat != null ? address.flat : "&mdash;"}</td>

                                    <td><form:radiobutton path="addressId" value="${address.id}"/></td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group">
                        <table class="table text-left">
                            <thead>
                            <tr>
                                <th>Payment method</th>
                                <th>Select</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${paymentMethods}" var="paymentMethod">
                                <tr>
                                    <td>${paymentMethod.description}</td>
                                    <td><form:radiobutton path="paymentMethod" value="${paymentMethod}"/></td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </c:if>

                <c:if test="${empty addresses}">
                    You have no shipping addresses <i class="far fa-frown"></i>.
                    <br>
                    But you can create them by clicking <a href="<c:url value='/profile/address'/>">here</a>.
                </c:if>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-outline-success">Order</button>
                </div>
            </form:form>
        </div>
    </jsp:body>
</t:generalTemplate>
