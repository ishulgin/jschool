<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generalTemplate>
    <jsp:attribute name="include">
        <script src="<c:url value="/js/account/cart.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="alert">
        <c:if test="${not empty message}">
        <div class="alert alert-danger alert-dismissible fade show my-0" role="alert">
                ${message}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        </c:if>
    </jsp:attribute>

    <jsp:attribute name="title">
        Cart
    </jsp:attribute>

    <jsp:body>
        <div class="card-body mx-auto" id="cardBody">
            <c:if test="${!empty cartItems}">
                <div class="table-responsive" id="tableArea">
                    <table class="table vertical-align table-striped text-left" id="cartTable">
                        <thead>
                        <tr>
                            <th scope="col"></th>

                            <th scope="col">Product</th>

                            <th scope="col">Count</th>

                            <th scope="col">Total</th>

                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        <c:forEach items="${cartItems}" var="cartItem">
                            <tr>
                                <td class="text-center"><img class="preview-image"
                                                             src="/images/${cartItem.product.imgDirectoryName}${cartItem.product.previewImage}"/>
                                </td>

                                <td><a href="/products/${cartItem.productId}">${cartItem.product.name}</a></td>

                                <td>${cartItem.count}</td>

                                <td>${cartItem.product.currentPrice * cartItem.count} $</td>

                                <td class="text-center">
                                    <button class="btn btn-danger"
                                            onclick="deleteItem(${cartItem.product.id}, $(this))"><i
                                            class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>

                    <div class="text-center">
                        <a class="btn btn-outline-success" href="<c:url value="/checkout"/>">Check Out</a>
                    </div>
                </div>
            </c:if>

            <c:if test="${empty cartItems}">
                <h1>Empty cart.</h1>
            </c:if>
        </div>
    </jsp:body>
</t:generalTemplate>
