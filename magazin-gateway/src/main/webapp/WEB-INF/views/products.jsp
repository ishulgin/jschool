<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>

<t:generalTemplate>
    <jsp:attribute name="include">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/categoryTree.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/bootstrapSelect.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/products.css"/>">

        <script src="<c:url value="/js/products.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="title">
        Products
    </jsp:attribute>

    <jsp:body>
        <div class="card-body container-fluid">
            <div class="row">
                <div class="col-4">
                    <div id="categoriesView">
                        <h3>No categories found</h3>
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">
                        <h5>Price:</h5>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-3">
                                <input type="text" class="form-control" id="from" placeholder="From">
                            </div>
                            <div class="col-3">
                                <input type="text" class="form-control" id="to" placeholder="To">
                            </div>
                            <div class="col-auto">
                                <button type="button" class="btn btn-primary" id="apply">Apply</button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Filters:</h5>
                    </div>

                    <div class="form-group" id="filters">

                    </div>

                    <div id="products">

                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" id="id" value="${id}">
    </jsp:body>
</t:generalTemplate>