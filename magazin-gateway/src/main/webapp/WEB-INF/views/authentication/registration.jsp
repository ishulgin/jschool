<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>

<t:authTemplate>
    <jsp:attribute name="title">
        Sign Up
    </jsp:attribute>

    <jsp:body>
        <c:url var="register" value="/registration"/>

        <form:form action="${register}" modelAttribute="account" method="POST">
            <c:set var="nameError">
                <form:errors path="name"/>
            </c:set>
            <c:set var="surnameError">
                <form:errors path="surname"/>
            </c:set>
            <c:set var="usernameError">
                <form:errors path="username"/>
            </c:set>
            <c:set var="dateOfBirthError">
                <form:errors path="dateOfBirth"/>
            </c:set>
            <c:set var="passwordError">
                <form:errors path="password"/>
                <form:errors/>
            </c:set>

            <h2 class="text-center">Register</h2>

            <p class="hint-text">Create your account. It's free and only takes a minute.</p>

            <div class="form-group">
                <form:input required="required" path="name"
                            cssClass='form-control ${not empty nameError ? "is-invalid" : ""}'
                            placeholder="First Name"/>
                <div class="invalid-feedback">${nameError}</div>
            </div>

            <div class="form-group">
                <form:input required="required" path="surname"
                            cssClass='form-control ${not empty surnameError ? "is-invalid" : ""}'
                            placeholder="Surname"/>
                <div class="invalid-feedback">${surnameError}</div>
            </div>

            <div class="form-group">
                <form:input type="date" required="required" path="dateOfBirth"
                            cssClass='form-control ${not empty dateOfBirthError ? "is-invalid" : ""}'
                            placeholder="Date of birth"/>
                <div class="invalid-feedback">${dateOfBirthError}</div>
            </div>

            <div class="form-group">
                <form:input required="required" path="username"
                            cssClass='form-control ${not empty usernameError ? "is-invalid" : ""}'
                            placeholder="Email"/>
                <div class="invalid-feedback">${usernameError}</div>
            </div>

            <div class="form-group">
                <form:password required="required" path="password"
                               cssClass='form-control ${not empty passwordError ? "is-invalid" : ""}'
                               placeholder="Password"/>
                <div class="invalid-feedback is-invalid">${passwordError}</div>
            </div>

            <div class="form-group">
                <form:password required="required" path="passwordConfirmation"
                               cssClass='form-control ${not empty passwordError ? "is-invalid" : ""}'
                               placeholder="Password Confirmation"/>
            </div>

            <div class="form-group text-center">
                <button type="submit" class="btn btn-outline-success">Register Now</button>
            </div>
        </form:form>

        <div class="text-center">
            Already have an account? <a href="<c:url value='/login'/>">Sign in</a>
        </div>
    </jsp:body>
</t:authTemplate>
