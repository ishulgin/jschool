<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>

<t:authTemplate>
    <jsp:attribute name="title">
        Sign In
    </jsp:attribute>

    <jsp:attribute name="alert">
        <c:if test="${not empty error}">
            <div class="alert alert-danger alert-dismissible fade show my-0" role="alert">
                    ${error}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </c:if>
    </jsp:attribute>

    <jsp:body>
        <form action="<c:url value='/login'/>" method='POST'>
            <div class="form-group">
                <input type="text" required="required" class="form-control" name="username" placeholder="Email"/>
            </div>

            <div class="form-group">
                <input type="password" required="required" class="form-control" name="password"
                       placeholder="Password"/>
            </div>

            <div class="form-group text-center">
                <button type="submit" class="btn btn-outline-success">Sign In</button>
            </div>

            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <div class="text-center">
            Not registered yet? <a href="<c:url value='/registration'/>">Create an account</a>
        </div>
    </jsp:body>
</t:authTemplate>
