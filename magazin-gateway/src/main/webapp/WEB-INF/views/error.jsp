<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Page Not Found</title>
</head>
<body>
<div style="text-align: center">
    <img style='border:1px solid #002244; border-radius: 5px;' src='<c:url value="/img/error.gif"/>'>

    <h1>Nothing found</h1>

    <a href="<c:url value="/"/>">Go Home</a>
</div>
</body>
</html>