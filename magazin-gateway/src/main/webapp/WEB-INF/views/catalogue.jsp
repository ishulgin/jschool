<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>

<t:generalTemplate>
    <jsp:attribute name="title">
        Catalogue
    </jsp:attribute>

    <jsp:body>
        <c:if test="${not empty departments}">
            <div class="card-body container-fluid">
                <div class="card-columns">
                    <c:forEach items="${departments}" var="department">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">${department.name}</h4>

                                <c:if test="${empty department.children}">
                                    <h5>Coming soon!</h5>
                                </c:if>

                                <c:if test="${not empty department.children}">
                                    <c:forEach items="${department.children}" var="child">
                                        <a href="/categories/${child.id}">${child.name}</a>
                                        <br>
                                    </c:forEach>
                                </c:if>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </c:if>

        <c:if test="${empty departments}">
            <h1>Nothing to show.</h1>
        </c:if>
    </jsp:body>
</t:generalTemplate>