<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<%@taglib tagdir="/WEB-INF/tags" prefix="t" %>

<t:generalTemplate>
    <jsp:attribute name="title">
        ${product.name}
    </jsp:attribute>

    <jsp:attribute name="include">
        <style>
            .table td.fit,
            .table th.fit {
                white-space: nowrap;
                width: 1%;
            }
        </style>

        <link rel="stylesheet" type="text/css" href="<c:url value="/css/product.css"/>">

        <script src="<c:url value="/js/product.js"/>"></script>
    </jsp:attribute>

    <jsp:body>
        <div class="card-body container-fluid">
            <div class="row">
                <div class="col-5">
                    <div id="slider" class="carousel slide" data-ride="carousel">
                        <c:if test="${fn:length(product.productImages) > 1}">
                            <ol class="carousel-indicators">
                                <c:forEach begin="0" end="${fn:length(product.productImages)}" varStatus="status">
                                    <li class="${status.index == 0 ? 'active' : ''}"
                                        data-target="#carouselExampleIndicators"
                                        data-slide-to="${status.index}"></li>
                                </c:forEach>
                            </ol>
                        </c:if>

                        <div class="carousel-inner">
                            <c:forEach items="${product.productImages}" var="image" varStatus="status">
                                <div class="carousel-item ${status.index == 0 ? 'active' : ''}">
                                    <img class="carousel-image"
                                         src="<c:url value="/images/${product.imgDirectoryName}${image}"/>"
                                         alt="">
                                </div>
                            </c:forEach>
                        </div>

                        <c:if test="${fn:length(product.productImages) > 1}">
                            <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>

                            <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </c:if>
                    </div>
                </div>

                <div class="col-7">
                    <div class="form-group">
                        <h4>${product.name}</h4>
                    </div>

                    <hr>

                    <div class="form-group">
                        <h5>Price: ${product.priceString}</h5>
                    </div>

                    <div class="form-group">
                        <h5>Left in stock: <span id="leftCount">${product.leftCount}</span></h5>
                    </div>

                    <div class="container form-group">
                        <div class="row">
                            <div class="col-6 p-0">
                                <c:url var="addToCart" value="/cart"/>

                                <form:form action="${addToCart}" modelAttribute="cartItemRequestDTO">
                                    <div class="form-group">
                                        <div class="input-group w-50">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-danger" id="minus" type="button">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                            </div>

                                            <form:input cssClass="form-control text-center" path="count" value="1"
                                                        required="required"/>

                                            <div class="input-group-append">
                                                <button class="btn btn-success" id="plus" type="button">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <form:hidden path="productId"/>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-outline-success">Add To Cart</button>
                                    </div>
                                </form:form>

                                <sec:authorize access="hasAuthority('manager')">
                                    <form method="POST" action="/products/${product.id}/count">
                                        <div class="form-group">
                                            <div class="input-group w-50">
                                                <div class="input-group-prepend">
                                                    <button class="btn btn-danger" id="minusUpdate" type="button">
                                                        <i class="fas fa-minus"></i>
                                                    </button>
                                                </div>

                                                <input type="text" class="form-control text-center" id="updateLeftCount"
                                                       name="leftCount" value="${product.leftCount}"/>

                                                <div class="input-group-append">
                                                    <button class="btn btn-success" id="plusUpdate" type="button">
                                                        <i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                                            <button type="submit" class="btn btn-outline-success">Update Count</button>
                                        </div>
                                    </form>
                                </sec:authorize>
                            </div>

                            <div class="col-6 p-0">
                                <sec:authorize access="hasAuthority('manager')">
                                    <form method="POST" action="/products/${product.id}/price/current">
                                        <div class="form-group">
                                            <div class="w-50">
                                                <input type="text"
                                                       class='form-control ${not empty currentPriceError ? "is-invalid" : ""}'
                                                       name="currentPrice" value="${product.currentPrice}"/>

                                                <div class="invalid-feedback">${currentPriceError}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                                            <button type="submit" class="btn btn-outline-success">
                                                Set Current Price
                                            </button>
                                        </div>
                                    </form>

                                    <form method="POST" action="/products/${product.id}/price/general">
                                        <div class="form-group">
                                            <div class="w-50">
                                                <input type="text"
                                                       class='form-control ${not empty priceError ? "is-invalid" : ""}'
                                                       name="price" value="${product.price}"/>

                                                <div class="invalid-feedback">${priceError}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                                            <button type="submit" class="btn btn-outline-success">
                                                Set General Price
                                            </button>
                                        </div>
                                    </form>
                                </sec:authorize>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#nav-characteristics" role="tab">Characteristics</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#nav-profile">Description</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="nav-characteristics" role="tabpanel">
                            <table class="table table-nonfluid">
                                <tbody>
                                <c:forEach items="${product.filterValues}" var="filterValue">
                                    <tr>
                                        <td>${filterValue.filterTypeName}</td>
                                        <td>${filterValue.value}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>

                        <div class="tab-pane fade" id="nav-profile" role="tabpanel">
                            <div class="form-group">
                                    ${product.description}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:generalTemplate>
