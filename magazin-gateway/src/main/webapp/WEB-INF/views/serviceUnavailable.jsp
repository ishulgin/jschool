<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Page Not Found</title>
</head>
<body>
<div style="text-align: center">
    <img height="400px" src='<c:url value="/img/hmm.png"/>'>

    <h1>Can't establish connection with service.</h1>

    <a href="<c:url value="/"/>">Go Home</a>
</div>
</body>
</html>