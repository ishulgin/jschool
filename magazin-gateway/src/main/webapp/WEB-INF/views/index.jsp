<%@taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:generalTemplate>
    <jsp:attribute name="title">
        Home
    </jsp:attribute>
    <jsp:body>
        <div class="card-body container-fluid">
            <div class="form-group">
                <h1 class="text-center">New Arrivals</h1>

                <div class="card-deck">
                    <c:forEach items="${lastProducts}" var="product">
                        <div class="card">
                            <img class="card-img-top"
                                 src="/images/${product.imgDirectoryName}${product.previewImage}">

                            <div class="card-body">
                                <h5 class="card-title"><a href="/products/${product.id}">${product.name}</a></h5>

                                <p class="card-text">${product.priceString}</p>

                                <p class="card-text">${product.shortDescription}</p>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>

            <div class="form-group">
                <h1 class="text-center">Best Sellers</h1>

                <div class="card-deck">
                    <c:forEach items="${popularProduct}" var="product">
                        <div class="card">
                            <img class="card-img-top"
                                 src="/images/${product.imgDirectoryName}${product.previewImage}">

                            <div class="card-body">
                                <h5 class="card-title"><a href="/products/${product.id}">${product.name}</a></h5>

                                <p class="card-text">${product.priceString}</p>

                                <p class="card-text">${product.shortDescription}</p>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
    </jsp:body>
</t:generalTemplate>