<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generalTemplate>
    <jsp:attribute name="include">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/categoryTree.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/bootstrapSelect.css"/>">

        <script src="<c:url value="/js/admin/createCategory.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="title">
        Add Category
    </jsp:attribute>

    <jsp:body>
        <div class="card-body mx-auto" style="width: 400px;">
            <div class="form-group">
                <div id="categoriesView">
                </div>
            </div>

            <div class="form-group text-center">
                <button type="submit" id="department" class="btn btn-outline-success">Add Department</button>
            </div>
        </div>

        <div class="modal fade" id="modal" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalTitle">Add real category</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body pb-0">
                        <div class="alert alert-success fade show" role="alert" id="alert" style="display: none;">
                            New category was successfully created.

                            <button type="button" class="close" id="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" id="name" placeholder="Name">
                            <div class="invalid-feedback">
                                Name's length must be from 4 to 64 characters.
                            </div>
                        </div>

                        <div class="form-group" id="categoryFiltersWrapper">
                            <select id="categoryFilters" class="selectpicker" data-width="100%"
                                    data-live-search="true"
                                    title="Select filter's types" multiple>

                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" id="submit" class="btn btn-outline-success">Add Category</button>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" id="parentId">
    </jsp:body>
</t:generalTemplate>