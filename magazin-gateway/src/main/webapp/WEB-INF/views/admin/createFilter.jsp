<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generalTemplate>
    <jsp:attribute name="include">
        <script src="<c:url value="/js/admin/createFilter.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="title">
        Add Filter
    </jsp:attribute>

    <jsp:attribute name="alert">
        <c:if test="${not empty message}">
            <div class="alert alert-success alert-dismissible fade show my-0" role="alert">
                    ${message}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </c:if>
    </jsp:attribute>

    <jsp:body>
        <div class="card-body mx-auto" style="width: 400px;">
            <c:url var="createFilter" value="/create/filter"/>

            <form:form method="POST" action="${createFilter}" modelAttribute="filter" id="form"
                       onsubmit="return submitForm()">
                <div class="form-group">
                    <form:input required="required" path="name" cssClass='form-control' placeholder="Name"/>
                </div>

                <div class="form-group">
                    <table class="table" id="filterValuesTable">
                        <thead>
                        <tr>
                            <td class="align-middle px-0">
                                <input type="text" class="form-control" id="value" placeholder="Value">

                                <div class="invalid-feedback">
                                    Value can't be empty.
                                </div>
                            </td>

                            <td class="text-right align-middle px-0">
                                <button class="btn btn-success" type="button" onclick="addFilterValue()">
                                    <i class="fas fa-plus"></i>
                                </button>
                            </td>
                        </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-outline-success">Add Filter</button>
                </div>
            </form:form>
        </div>
    </jsp:body>
</t:generalTemplate>