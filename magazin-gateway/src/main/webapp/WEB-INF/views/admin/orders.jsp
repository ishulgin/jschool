<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>

<t:generalTemplate>
    <jsp:attribute name="include">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/bootstrapSelect.css"/>">

        <script src="<c:url value="/js/admin/orders.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="title">
        Orders
    </jsp:attribute>

    <jsp:body>
        <div class="card-body container-fluid">

            <c:if test="${!empty orders}">
                <table id="ordersTable" class="table vertical-align table-striped text-left" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>

                        <th>Order Time</th>

                        <th>Shipping Addr.</th>

                        <th>Payment Method</th>

                        <th>Payment Status</th>

                        <th>Status</th>

                        <th>Additional</th>
                    </tr>
                    </thead>

                    <tbody>
                    <c:forEach items="${orders}" var="order">
                        <tr>
                            <td>${order.id}</td>

                            <td><javatime:format value="${order.orderedAt}" pattern="yyyy-MM-dd HH:mm"/></td>

                            <td>
                                <button type="button" class="btn btn-link p-0 address-details"
                                        value="${order.addressId}">
                                    Click for details
                                </button>
                            </td>

                            <td>${order.paymentMethod}</td>

                            <td>
                                <select class="selectpicker payment-status" data-width="fit" data-id="${order.id}">
                                    <c:forEach items="${paymentStatuses}" var="paymentStatus">
                                        <option value="${paymentStatus}" ${order.paymentStatus.equals(paymentStatus) ? 'selected' : ''}>${paymentStatus}</option>
                                    </c:forEach>
                                </select>
                            </td>

                            <td>
                                <select class="selectpicker order-status" data-width="fit" data-id="${order.id}">
                                    <c:forEach items="${orderStatuses}" var="orderStatus">
                                        <option value="${orderStatus}" ${order.orderStatus.equals(orderStatus) ? 'selected' : ''}>${orderStatus}</option>
                                    </c:forEach>
                                </select>
                            </td>

                            <td>
                                <button type="button" class="btn btn-link p-0 order-details" value="${order.id}">
                                    Details
                                </button>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>

            <c:if test="${empty orders}">
                <h1 class="text-center">There are no orders or order service is dead.</h1>
            </c:if>
        </div>

        <div class="modal fade" id="modal" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalTitle"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="form-group" id="details">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:generalTemplate>