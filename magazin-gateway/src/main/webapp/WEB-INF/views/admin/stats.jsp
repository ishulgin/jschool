<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generalTemplate>
    <jsp:attribute name="include">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/categoryTree.css"/>">

        <script src="<c:url value="/js/admin/stats.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="title">
        Stats
    </jsp:attribute>

    <jsp:body>
        <div class="card-body">
            <h5 class="text-center">Graph</h5>
            <canvas id="graph"></canvas>
        </div>

        <div class="card-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-4">
                        <h5 class="text-center">Stats for last week</h5>

                        <table id="statTable" class="table table-striped text-left" cellspacing="0">
                            <tfoot>
                            <tr>
                                <th style="text-align: right">Total:</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="col-4">
                        <h5 class="text-center">Stats for last year</h5>

                        <c:set var="total" value="${0}"/>

                        <table id="yearStats" class="table table-striped text-left">
                            <thead>
                            <th>Month</th>

                            <th>Year</th>

                            <th>Profit</th>
                            </thead>

                            <tbody>
                            <c:forEach items="${yearStats}" var="yearStat">
                                <tr>
                                    <td>${yearStat.month}</td>

                                    <td>${yearStat.year}</td>

                                    <td>${yearStat.profit}$</td>
                                </tr>

                                <c:set var="total" value="${total + yearStat.profit}" />
                            </c:forEach>
                            </tbody>

                            <tfoot>
                            <tr>
                                <th style="text-align: right">Total:</th>
                                <th>${total}$</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="col-4">
                        <h5 class="text-center">Best clients</h5>

                        <table class="table table-striped text-left">
                            <thead>
                            <th>Name</th>

                            <th>Surname</th>

                            <th>Profit</th>
                            </thead>

                            <tbody>
                            <c:forEach items="${clients}" var="client">
                                <tr>
                                    <td>${client.name}</td>

                                    <td>${client.surname}</td>

                                    <td>${client.profit}$</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalTitle"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <div id="categoriesView"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:generalTemplate>