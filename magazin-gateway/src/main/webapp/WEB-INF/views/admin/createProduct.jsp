<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generalTemplate>
    <jsp:attribute name="include">
        <script src="<c:url value="/js/admin/createProduct.js"/>"></script>

        <link rel="stylesheet" type="text/css" href="<c:url value="/css/categoryTree.css"/>">
    </jsp:attribute>

    <jsp:attribute name="title">
        Add Product
    </jsp:attribute>

    <jsp:body>
        <div class="card-body mx-auto" style="width: 400px;">
            <c:url var="createProduct" value="/create/product?${_csrf.parameterName}=${_csrf.token}"/>

            <form:form method="POST" action="${createProduct}" modelAttribute="product" enctype="multipart/form-data"
                       id="productForm">
                <c:set var="nameError">
                    <form:errors path="name"/>
                </c:set>

                <c:set var="descriptionError">
                    <form:errors path="description"/>
                </c:set>

                <c:set var="priceError">
                    <form:errors path="price"/>
                </c:set>

                <c:set var="countError">
                    <form:errors path="leftCount"/>
                </c:set>

                <c:set var="weightError">
                    <form:errors path="weight"/>
                </c:set>

                <c:set var="categoryIdError">
                    <form:errors path="categoryId"/>
                </c:set>

                <c:set var="filtersError">
                    <form:errors path="filtersIds"/>
                </c:set>

                <div class="form-group">
                    <form:input required="required" path="name"
                                cssClass='form-control ${not empty nameError ? "is-invalid" : ""}'
                                placeholder="Product Name"/>
                    <div class="invalid-feedback">${nameError}</div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <input id="departmentField" type="text"
                               class='form-control ${(not empty categoryIdError || not empty filtersError) ? "is-invalid" : ""}'
                               style="z-index: 3"
                               value="Select a department" readonly>

                        <div class="input-group-append">
                            <button type="button" class="btn input-group-text rounded-right dropdown-toggle"
                                    data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                Department
                            </button>

                            <div class="dropdown-menu">
                                <c:forEach items="${departments}" var="department">
                                    <button class="dropdown-item department" type="button"
                                            value="${department.id}">${department.name}</button>
                                </c:forEach>
                            </div>
                        </div>

                        <div class="invalid-feedback">${categoryIdError}</div>
                        <div class="invalid-feedback">${filtersError}</div>
                    </div>
                </div>

                <div id="categoriesView" class="form-group">
                </div>

                <div id="filtersView">
                </div>

                <div class="form-group">
                    <form:textarea required="required" path="description"
                                   cssClass='form-control ${not empty descriptionError ? "is-invalid" : ""}'
                                   placeholder="Description"/>
                    <div class="invalid-feedback">${descriptionError}</div>
                </div>

                <div class="form-group">
                    <form:input required="required" path="price"
                                cssClass='form-control ${not empty priceError ? "is-invalid" : ""}'
                                placeholder="Price"/>
                    <div class="invalid-feedback">${priceError}</div>
                </div>

                <div class="form-group">
                    <form:input required="required" path="leftCount"
                                cssClass='form-control ${not empty countError ? "is-invalid" : ""}'
                                placeholder="In Stock Count"/>
                    <div class="invalid-feedback">${countError}</div>
                </div>

                <div class="form-group">
                    <form:input required="required" path="weight"
                                cssClass='form-control ${not empty countError ? "is-invalid" : ""}'
                                placeholder="Weight (Grams)"/>
                    <div class="invalid-feedback">${weightError}</div>
                </div>

                <form:hidden path="categoryId"/>

                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" name="file" class="custom-file-input" id="file1" required="required"
                               accept=".jpg,.jpeg,.png,.bmp,.gif">
                        <label class="custom-file-label" for="file1">Set preview image...</label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" name="file" class="custom-file-input" id="file2"
                               accept=".jpg,.jpeg,.png,.bmp,.gif">
                        <label class="custom-file-label" for="file2">Add image...</label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" name="file" class="custom-file-input" id="file3"
                               accept=".jpg,.jpeg,.png,.bmp,.gif">
                        <label class="custom-file-label" for="file3">Add image...</label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" name="file" class="custom-file-input" id="file4"
                               accept=".jpg,.jpeg,.png,.bmp,.gif">
                        <label class="custom-file-label" for="file4">Add image...</label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" name="file" class="custom-file-input" id="file5"
                               accept=".jpg,.jpeg,.png,.bmp,.gif">
                        <label class="custom-file-label" for="file5">Add image...</label>
                    </div>
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-outline-success">Add Product</button>
                </div>
            </form:form>
        </div>
    </jsp:body>
</t:generalTemplate>