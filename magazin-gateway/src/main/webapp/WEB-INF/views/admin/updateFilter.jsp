<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generalTemplate>
    <jsp:attribute name="include">
        <script src="<c:url value="/js/admin/updateFilter.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="title">
        Update Filter
    </jsp:attribute>

    <jsp:body>
        <div class="card-body mx-auto" style="width: 400px">
            <div class="list-group">
                <div class='text-center list-group-item'>
                    Select a filter
                </div>

                <c:forEach items="${filterTypes}" var="filterType">
                    <button type="button" class="list-group-item list-group-item-action filter"
                            value="${filterType.id}">${filterType.name}
                    </button>
                </c:forEach>
            </div>
        </div>

        <div class="modal fade" id="modal" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalTitle"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body pb-0">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" id="value" placeholder="Value">

                                <div class="input-group-append">
                                    <button class="btn btn-success rounded-right" type="button" id="updateFilter">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                </div>

                                <div class="invalid-feedback">
                                    Value can't be empty.
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="list-group" id="filterValues">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" id="id">
    </jsp:body>
</t:generalTemplate>