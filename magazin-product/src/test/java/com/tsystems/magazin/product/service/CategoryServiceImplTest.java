package com.tsystems.magazin.product.service;

import com.tsystems.magazin.product.dao.CategoryDAO;
import com.tsystems.magazin.product.dao.FilterDAO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CategoryServiceImplTest {
    @Mock
    private CategoryDAO categoryDAO;

    @Mock
    private FilterDAO filterDAO;

    @InjectMocks
    private CategoryServiceImpl categoryService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAbstractCategoryById_categoryNotFound_null() {
        when(categoryDAO.getAbstractCategoryById(1337)).thenReturn(null);

        assertNull(categoryService.getAbstractCategoryById(1337));
    }

    @Test
    public void getRealCategoryById_categoryNotFound_null() {
        when(categoryDAO.getCategoryById(1488)).thenReturn(null);

        assertNull(categoryService.getRealCategoryById(1488));
    }

    @Test
    public void saveCategory_department() {
        categoryService.saveCategory(TestData.getDepartmentCategoryRequestDTO1());

        verify(categoryDAO).saveAbstractCategory(TestData.getMappedDepartment1());
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveCategory_realCategoryWithoutParent_IllegalArgumentException() {
        categoryService.saveCategory(TestData.getRealCategoryWithoutParentRequestDTO1());
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveCategory_realCategoryParentNotExist_IllegalArgumentException() {
        when(categoryDAO.getAbstractCategoryById(11)).thenReturn(null);

        categoryService.saveCategory(TestData.getRealCategoryRequestDTO1());
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveCategory_realCategoryParentIsDepartment_IllegalArgumentException() {
        when(categoryDAO.getAbstractCategoryById(11)).thenReturn(TestData.getDepartmentWithoutChildren11());
        when(categoryDAO.getTopLevelCategories()).thenReturn(Collections.singletonList(TestData.getDepartmentWithoutChildren11()));

        categoryService.saveCategory(TestData.getRealCategoryRequestDTO1());
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveCategory_realCategoryParentHasAbstractChildren_IllegalArgumentException() {
        when(categoryDAO.getAbstractCategoryById(11)).thenReturn(TestData.getAbstractCategoryWithAbstractChildren11());
        when(categoryDAO.getTopLevelCategories()).thenReturn(Collections.singletonList(TestData.getDepartmentWithoutChildren1()));

        categoryService.saveCategory(TestData.getRealCategoryRequestDTO1());
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveCategory_realCategoryParentHasNoChildrenInvalidFilter_IllegalArgumentException() {
        when(categoryDAO.getAbstractCategoryById(11)).thenReturn(TestData.getAbstractCategoryWithoutChildren11());
        when(categoryDAO.getTopLevelCategories()).thenReturn(Collections.singletonList(TestData.getDepartmentWithoutChildren1()));
        when(filterDAO.getFilterTypeById(1)).thenReturn(null);

        categoryService.saveCategory(TestData.getRealCategoryRequestDTO1());
    }

    @Test
    public void saveCategory_realCategoryParentHasNoChildren() {
        when(categoryDAO.getAbstractCategoryById(11)).thenReturn(TestData.getAbstractCategoryWithoutChildren11());
        when(categoryDAO.getTopLevelCategories()).thenReturn(Collections.singletonList(TestData.getDepartmentWithoutChildren1()));
        when(filterDAO.getFilterTypeById(1)).thenReturn(TestData.getFilterType1());
        when(filterDAO.getFilterTypeById(2)).thenReturn(TestData.getFilterType2());

        categoryService.saveCategory(TestData.getRealCategoryRequestDTO1());

        verify(categoryDAO).saveCategory(TestData.getMappedRealCategoryParentHasNoChildren1());
    }

    @Test
    public void saveCategory_realCategoryParentHasRealChildren() {
        when(categoryDAO.getAbstractCategoryById(11)).thenReturn(TestData.getAbstractCategoryWithRealChildren11());
        when(categoryDAO.getTopLevelCategories()).thenReturn(Collections.singletonList(TestData.getDepartmentWithoutChildren1()));
        when(filterDAO.getFilterTypeById(1)).thenReturn(TestData.getFilterType1());
        when(filterDAO.getFilterTypeById(2)).thenReturn(TestData.getFilterType2());

        categoryService.saveCategory(TestData.getRealCategoryRequestDTO1());

        verify(categoryDAO).saveCategory(TestData.getMappedRealCategoryParentHasRealChildren1());
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveCategory_abstractCategoryParentNotExist_IllegalArgumentException() {
        when(categoryDAO.getAbstractCategoryById(11)).thenReturn(null);

        categoryService.saveCategory(TestData.getAbstractCategoryRequestDTO1());
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveCategory_abstractCategoryParentHasRealChildren_IllegalArgumentException() {
        when(categoryDAO.getAbstractCategoryById(11)).thenReturn(TestData.getAbstractCategoryWithRealChildren11());
        when(categoryDAO.getTopLevelCategories()).thenReturn(Collections.singletonList(TestData.getDepartmentWithoutChildren11()));

        categoryService.saveCategory(TestData.getAbstractCategoryRequestDTO1());
    }

    @Test
    public void saveCategory_abstractCategoryParentHasAbstractChildren() {
        when(categoryDAO.getAbstractCategoryById(11)).thenReturn(TestData.getAbstractCategoryWithAbstractChildren11());
        when(categoryDAO.getTopLevelCategories()).thenReturn(Collections.singletonList(TestData.getDepartmentWithoutChildren11()));

        categoryService.saveCategory(TestData.getAbstractCategoryRequestDTO1());

        verify(categoryDAO).saveAbstractCategory(TestData.getMappedAbstractCategoryParentHasAbstractChildren1());
    }

    @Test
    public void saveCategory_abstractCategoryParentHasNoChildren() {
        when(categoryDAO.getAbstractCategoryById(11)).thenReturn(TestData.getAbstractCategoryWithAbstractChildren11());
        when(categoryDAO.getTopLevelCategories()).thenReturn(Collections.singletonList(TestData.getDepartmentWithoutChildren11()));

        categoryService.saveCategory(TestData.getAbstractCategoryRequestDTO1());

        verify(categoryDAO).saveAbstractCategory(TestData.getMappedAbstractCategoryParentHasNoChildren1());
    }

    @Test
    public void getFilters_categoryNotFound_null() {
        when(categoryDAO.getCategoryById(1)).thenReturn(null);

        assertNull(categoryService.getFilters(1));
    }

    @Test
    public void getProducts_categoryNotFound_null() {
        when(categoryDAO.getCategoryById(1)).thenReturn(null);

        assertNull(categoryService.getProducts(1));
    }
}