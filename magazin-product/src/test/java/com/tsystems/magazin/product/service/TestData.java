package com.tsystems.magazin.product.service;

import com.tsystems.magazin.domain.dto.request.CategoryRequestDTO;
import com.tsystems.magazin.domain.dto.request.ProductRequestDTO;
import com.tsystems.magazin.product.model.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

public class TestData {
    public static AbstractCategory getDepartmentWithoutChildren1() {
        AbstractCategory abstractCategory = new AbstractCategory();
        abstractCategory.setId(1);
        abstractCategory.setName("Department");
        abstractCategory.setChildren(Collections.emptyList());
        abstractCategory.setAbstractChildren(Collections.emptyList());

        return abstractCategory;
    }

    public static AbstractCategory getDepartmentWithoutChildren11() {
        AbstractCategory abstractCategory = new AbstractCategory();
        abstractCategory.setId(1);
        abstractCategory.setName("Department");
        abstractCategory.setChildren(Collections.emptyList());
        abstractCategory.setAbstractChildren(Collections.emptyList());

        return abstractCategory;
    }

    public static AbstractCategory getAbstractCategoryWithoutChildren11() {
        AbstractCategory abstractCategory = new AbstractCategory();
        abstractCategory.setId(11);
        abstractCategory.setName("Abstract category");
        abstractCategory.setChildren(Collections.emptyList());
        abstractCategory.setAbstractChildren(Collections.emptyList());

        AbstractCategory parent = getDepartmentWithoutChildren1();
        parent.setAbstractChildren(Collections.singletonList(abstractCategory));
        abstractCategory.setParent(parent);

        return abstractCategory;
    }

    public static AbstractCategory getAbstractCategoryWithAbstractChildren11() {
        AbstractCategory abstractCategory = new AbstractCategory();
        abstractCategory.setId(12);
        abstractCategory.setName("Abstract category with abstract children");
        abstractCategory.setChildren(Collections.emptyList());

        AbstractCategory parent = getDepartmentWithoutChildren1();
        parent.setAbstractChildren(Collections.singletonList(abstractCategory));
        abstractCategory.setParent(parent);

        AbstractCategory child1 = new AbstractCategory();
        child1.setId(121);
        child1.setName("Child 1");
        child1.setParent(abstractCategory);
        child1.setChildren(Collections.emptyList());
        child1.setChildren(Collections.emptyList());

        AbstractCategory child2 = new AbstractCategory();
        child1.setId(122);
        child1.setName("Child 2");
        child1.setParent(abstractCategory);
        child1.setChildren(Collections.emptyList());
        child1.setChildren(Collections.emptyList());

        abstractCategory.setAbstractChildren(Arrays.asList(child1, child2));

        return abstractCategory;
    }

    public static AbstractCategory getAbstractCategoryWithRealChildren11() {
        AbstractCategory abstractCategory = new AbstractCategory();
        abstractCategory.setId(13);
        abstractCategory.setName("Abstract category with abstract children");
        abstractCategory.setChildren(Collections.emptyList());

        AbstractCategory parent = getDepartmentWithoutChildren1();
        parent.setAbstractChildren(Collections.singletonList(abstractCategory));
        abstractCategory.setParent(parent);

        abstractCategory.setAbstractChildren(Collections.emptyList());
        abstractCategory.setChildren(Collections.singletonList(getRealCategory1()));

        return abstractCategory;
    }

    public static FilterType getFilterType1() {
        FilterType filterType = new FilterType();
        filterType.setId(1);
        filterType.setName("Filter type 1");

        FilterValue filterValue11 = new FilterValue();
        filterValue11.setId(11);
        filterValue11.setValue("Value 11");
        filterValue11.setFilterType(filterType);

        FilterValue filterValue12 = new FilterValue();
        filterValue12.setId(12);
        filterValue12.setValue("Value 12");
        filterValue12.setFilterType(filterType);

        filterType.setValues(Arrays.asList(filterValue11, filterValue12));

        return filterType;
    }

    public static FilterType getFilterType2() {
        FilterType filterType = new FilterType();
        filterType.setId(2);
        filterType.setName("Filter type 2");

        FilterValue filterValue21 = new FilterValue();
        filterValue21.setId(21);
        filterValue21.setValue("Value 21");
        filterValue21.setFilterType(filterType);

        FilterValue filterValue22 = new FilterValue();
        filterValue22.setId(22);
        filterValue22.setValue("Value 22");
        filterValue22.setFilterType(filterType);

        filterType.setValues(Arrays.asList(filterValue21, filterValue22));

        return filterType;
    }

    public static Category getRealCategory1() {
        Category category = new Category();
        category.setId(1);
        category.setName("Category 1");
        category.setProducts(Collections.emptySet());

        AbstractCategory parent = getAbstractCategoryWithoutChildren11();
        parent.setChildren(Collections.singletonList(category));
        category.setParent(parent);
        category.setDepartmentId(1);

        category.setFilterTypes(Arrays.asList(getFilterType1(), getFilterType2()));

        Product product11 = new Product();
        product11.setId(11);
        product11.setName("Product 11");
        product11.setDescription("Description");
        product11.setPreviewImage("image");
        product11.setProductImages(Arrays.asList("image", "otherImage"));
        product11.setCurrentPrice(10);
        product11.setPrice(10);
        product11.setDiscount(0);
        product11.setLeftCount(100);
        product11.setFilterValues(Arrays.asList(getFilterType1().getValues().get(0),
                getFilterType2().getValues().get(0)));
        product11.setCategory(category);

        Product product12 = new Product();
        product12.setId(12);
        product12.setName("Product 12");
        product12.setDescription("Description");
        product12.setPreviewImage("image");
        product12.setProductImages(Arrays.asList("image", "otherImage"));
        product12.setCurrentPrice(100);
        product12.setPrice(1000);
        product12.setDiscount(90);
        product12.setLeftCount(12);
        product12.setFilterValues(Arrays.asList(getFilterType1().getValues().get(1),
                getFilterType2().getValues().get(0)));
        product12.setCategory(category);

        Product product13 = new Product();
        product13.setId(13);
        product13.setName("Product 13");
        product13.setDescription("Description");
        product13.setPreviewImage("image");
        product13.setCurrentPrice(120);
        product13.setProductImages(Collections.emptyList());
        product13.setPrice(120);
        product13.setDiscount(0);
        product13.setLeftCount(1144);
        product13.setFilterValues(Arrays.asList(getFilterType1().getValues().get(1),
                getFilterType2().getValues().get(1)));
        product13.setCategory(category);

        category.setProducts(new HashSet<>(Arrays.asList(product11, product12, product13)));

        return category;
    }

    public static CategoryRequestDTO getDepartmentCategoryRequestDTO1() {
        CategoryRequestDTO categoryRequestDTO = new CategoryRequestDTO();
        categoryRequestDTO.setName("New department");
        categoryRequestDTO.setFilterTypesIds(Collections.emptyList());

        return categoryRequestDTO;
    }

    public static AbstractCategory getMappedDepartment1() {
        AbstractCategory abstractCategory = new AbstractCategory();
        abstractCategory.setName("New department");

        return abstractCategory;
    }

    public static CategoryRequestDTO getRealCategoryWithoutParentRequestDTO1() {
        CategoryRequestDTO categoryRequestDTO = new CategoryRequestDTO();
        categoryRequestDTO.setFilterTypesIds(Arrays.asList(1, 2, 3));
        categoryRequestDTO.setName("New real category");

        return categoryRequestDTO;
    }

    public static CategoryRequestDTO getRealCategoryRequestDTO1() {
        CategoryRequestDTO categoryRequestDTO = new CategoryRequestDTO();
        categoryRequestDTO.setName("New real category");
        categoryRequestDTO.setParentId(11);
        categoryRequestDTO.setFilterTypesIds(Arrays.asList(1, 2));

        return categoryRequestDTO;
    }

    public static Category getMappedRealCategoryParentHasNoChildren1() {
        Category category = new Category();
        category.setName("New real category");
        category.setParent(getAbstractCategoryWithoutChildren11());
        category.setFilterTypes(Arrays.asList(getFilterType1(), getFilterType2()));

        return category;
    }

    public static Category getMappedRealCategoryParentHasRealChildren1() {
        Category category = new Category();
        category.setName("New real category");
        category.setParent(getAbstractCategoryWithRealChildren11());
        category.setFilterTypes(Arrays.asList(getFilterType1(), getFilterType2()));

        return category;
    }

    public static CategoryRequestDTO getAbstractCategoryRequestDTO1() {
        CategoryRequestDTO categoryRequestDTO = new CategoryRequestDTO();
        categoryRequestDTO.setName("New abstract category");
        categoryRequestDTO.setParentId(11);
        categoryRequestDTO.setFilterTypesIds(Collections.emptyList());

        return categoryRequestDTO;
    }

    public static AbstractCategory getMappedAbstractCategoryParentHasNoChildren1() {
        AbstractCategory abstractCategory = new AbstractCategory();
        abstractCategory.setName("New abstract category");
        abstractCategory.setParent(getAbstractCategoryWithoutChildren11());

        return abstractCategory;
    }

    public static AbstractCategory getMappedAbstractCategoryParentHasAbstractChildren1() {
        AbstractCategory abstractCategory = new AbstractCategory();
        abstractCategory.setName("New abstract category");
        abstractCategory.setParent(getAbstractCategoryWithAbstractChildren11());

        return abstractCategory;
    }

    public static ProductRequestDTO getProductRequestDTOWithoutFilters1() {
        ProductRequestDTO productRequestDTO = new ProductRequestDTO();
        productRequestDTO.setCategoryId(1);
        productRequestDTO.setName("asdasd");
        productRequestDTO.setPrice(100);
        productRequestDTO.setLeftCount(100);
        productRequestDTO.setDescription("asdasdasd");

        return productRequestDTO;
    }

    public static ProductRequestDTO getProductRequestDTO1() {
        ProductRequestDTO productRequestDTO = getProductRequestDTOWithoutFilters1();
        productRequestDTO.setFiltersIds(Arrays.asList(11, 21));

        return productRequestDTO;
    }

    public static ProductRequestDTO getProductRequestDTOWithSameFilterTypes1() {
        ProductRequestDTO productRequestDTO = getProductRequestDTOWithoutFilters1();
        productRequestDTO.setFiltersIds(Arrays.asList(11, 21, 12));

        return productRequestDTO;
    }

    public static ProductRequestDTO getProductRequestDTOWithSingleFilter1() {
        ProductRequestDTO productRequestDTO = getProductRequestDTOWithoutFilters1();
        productRequestDTO.setFiltersIds(Collections.singletonList(11));

        return productRequestDTO;
    }

    public static Product getMappedProductRequestDTO1() {
        ProductRequestDTO productRequestDTO = getProductRequestDTOWithoutFilters1();

        Product product = new Product();
        product.setName(productRequestDTO.getName());
        product.setPrice(productRequestDTO.getPrice());
        product.setCurrentPrice(productRequestDTO.getPrice());
        product.setLeftCount(productRequestDTO.getLeftCount());
        product.setDescription(productRequestDTO.getDescription());

        return product;
    }
}
