package com.tsystems.magazin.product.service;

import com.tsystems.magazin.product.client.MagazinStatClient;
import com.tsystems.magazin.product.dao.CategoryDAO;
import com.tsystems.magazin.product.dao.FilterDAO;
import com.tsystems.magazin.product.dao.ProductDAO;
import com.tsystems.magazin.product.model.FilterType;
import com.tsystems.magazin.product.model.FilterValue;
import com.tsystems.magazin.product.model.Product;
import com.tsystems.magazin.product.util.ModelMapperWrapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;

public class ProductServiceImplTest {
    @Mock
    private ProductDAO productDAO;

    @Mock
    private CategoryDAO categoryDAO;

    @Mock
    private FilterDAO filterDAO;

    @Mock
    private MagazinStatClient magazinStatClient;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @Mock
    private ModelMapperWrapper modelMapperWrapper;

    @InjectMocks
    private ProductServiceImpl productService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getProductInfo_productNotFound_null() {
        when(productDAO.getProductById(1)).thenReturn(null);

        assertNull(productService.getProductInfo(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteProduct_productNotFound_IllegalArgumentException() {
        when(productDAO.getProductById(1)).thenReturn(null);

        productService.deleteProduct(1);
    }

    @Test
    public void deleteProduct_deleted() {
        Product product = Mockito.mock(Product.class);

        when(productDAO.getProductById(1)).thenReturn(product);

        productService.deleteProduct(1);

        verify(productDAO).deleteProduct(product);
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveProduct_noFiltersSpecified_IllegalArgumentException() {
        productService.saveProduct(TestData.getProductRequestDTOWithoutFilters1());
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveProduct_categoryNotExist_IllegalArgumentException() {
        when(categoryDAO.getCategoryById(1)).thenReturn(null);

        productService.saveProduct(TestData.getProductRequestDTOWithSameFilterTypes1());
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveProduct_filterNotExist_IllegalArgumentException() {
        when(categoryDAO.getCategoryById(1)).thenReturn(TestData.getRealCategory1());
        when(modelMapperWrapper.mapProductRequestDTO(TestData.getProductRequestDTOWithSameFilterTypes1()))
                .thenReturn(TestData.getMappedProductRequestDTO1());
        when(filterDAO.getFilterValueById(anyInt())).thenReturn(null);

        productService.saveProduct(TestData.getProductRequestDTOWithSameFilterTypes1());
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveProduct_filtersOfSameType_IllegalArgumentException() {
        FilterType filterType1 = new FilterType();
        filterType1.setId(1);

        FilterValue filterValue1 = new FilterValue();
        filterValue1.setFilterType(filterType1);

        FilterType filterType2 = new FilterType();
        filterType2.setId(2);

        FilterValue filterValue2 = new FilterValue();
        filterValue2.setFilterType(filterType2);

        when(categoryDAO.getCategoryById(1)).thenReturn(TestData.getRealCategory1());
        when(modelMapperWrapper.mapProductRequestDTO(TestData.getProductRequestDTOWithSameFilterTypes1()))
                .thenReturn(TestData.getMappedProductRequestDTO1());

        when(filterDAO.getFilterValueById(or(eq(11), eq(12)))).thenReturn(filterValue1);
        when(filterDAO.getFilterValueById(eq(21))).thenReturn(filterValue2);

        productService.saveProduct(TestData.getProductRequestDTOWithSameFilterTypes1());
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveProduct_notEqualFilterTypes_IllegalArgumentException() {
        FilterType filterType1 = new FilterType();
        filterType1.setId(1);

        FilterValue filterValue1 = new FilterValue();
        filterValue1.setFilterType(filterType1);

        when(categoryDAO.getCategoryById(1)).thenReturn(TestData.getRealCategory1());
        when(modelMapperWrapper.mapProductRequestDTO(TestData.getProductRequestDTOWithSingleFilter1()))
                .thenReturn(TestData.getMappedProductRequestDTO1());

        when(filterDAO.getFilterValueById(11)).thenReturn(filterValue1);

        productService.saveProduct(TestData.getProductRequestDTOWithSingleFilter1());
    }

    @Test
    public void saveProduct_saved() {
        FilterType filterType1 = new FilterType();
        filterType1.setId(1);

        FilterValue filterValue1 = new FilterValue();
        filterValue1.setFilterType(filterType1);

        FilterType filterType2 = new FilterType();
        filterType2.setId(2);

        FilterValue filterValue2 = new FilterValue();
        filterValue2.setFilterType(filterType2);

        when(categoryDAO.getCategoryById(1)).thenReturn(TestData.getRealCategory1());
        when(modelMapperWrapper.mapProductRequestDTO(TestData.getProductRequestDTO1()))
                .thenReturn(TestData.getMappedProductRequestDTO1());

        when(filterDAO.getFilterValueById(11)).thenReturn(filterValue1);
        when(filterDAO.getFilterValueById(21)).thenReturn(filterValue2);

        Product savedProduct = TestData.getMappedProductRequestDTO1();
        savedProduct.setFilterValues(Arrays.asList(filterValue1, filterValue2));

        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            ((Product) args[0]).setId(228);
            return null;
        }).when(productDAO).saveProduct(savedProduct);

        assertEquals(228, productService.saveProduct(TestData.getProductRequestDTO1()));
    }

    @Test
    public void getLastAddedProducts_negative_null() {
        when(productDAO.getLastProducts(-1)).thenThrow(new IllegalArgumentException());

        assertNull(productService.getLastAddedProducts(-1));
    }

    @Test
    public void getMostPopularProducts_negative_null() {
        when(magazinStatClient.getMostPopularProductsIds(-1)).thenReturn(null);

        assertNull(productService.getMostPopularProducts(-1));
    }

    @Test
    public void getBestSalesOffers_negative_null() {
        when(productDAO.getBestSalesOffers(-1)).thenThrow(new IllegalArgumentException());

        assertNull(productService.getBestSalesOffers(-1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setProductImages_productNotFound_IllegalArgumentException() {
        when(productDAO.getProductById(1)).thenReturn(null);

        productService.setProductImages(1, Arrays.asList("qq", "ww", "ee"));
    }

    @Test
    public void setProductImages_imagesAlreadySet_false() {
        Product product = Mockito.mock(Product.class);

        when(productDAO.getProductById(1)).thenReturn(product);
        when(product.getProductImages()).thenReturn(Arrays.asList("1", "2"));

        assertFalse(productService.setProductImages(1, Arrays.asList("qq", "ww", "ee")));

        verify(product, never()).setProductImages(anyList());
    }

    @Test
    public void setProductImages_true() {
        Product product = Mockito.mock(Product.class);

        when(productDAO.getProductById(1)).thenReturn(product);
        when(product.getProductImages()).thenReturn(Collections.emptyList());

        assertTrue(productService.setProductImages(1, Arrays.asList("qq", "ww", "ee")));

        verify(product).setProductImages(Arrays.asList("qq", "ww", "ee"));
    }

    @Test
    public void updateProductLeftCount() {
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateProductPrice_productNotFound_IllegalArgumentException() {
        when(productDAO.getProductById(1)).thenReturn(null);

        productService.updateProductPrice(1, 100);
    }

    @Test
    public void updateProductPrice_negative_false() {
        Product product = Mockito.mock(Product.class);

        when(productDAO.getProductById(1)).thenReturn(product);

        assertFalse(productService.updateProductPrice(1, -10));

        verify(product, never()).setLeftCount(anyInt());
    }

    @Test
    public void updateProductPrice_0_false() {
        Product product = Mockito.mock(Product.class);

        when(productDAO.getProductById(1)).thenReturn(product);

        assertFalse(productService.updateProductPrice(1, 0));

        verify(product, never()).setPrice(anyInt());
    }

    @Test
    public void updateProductPrice_positive_true() {
        Product product = Mockito.mock(Product.class);

        when(productDAO.getProductById(1)).thenReturn(product);

        productService.updateProductPrice(1, 100);

        verify(product).setPrice(100);
        verify(product).setCurrentPrice(100);
        verify(product).setDiscount(0);
    }

    @Test
    public void updateProductCurrentPrice_equalsToPrice_true() {
        Product product = Mockito.mock(Product.class);

        when(productDAO.getProductById(1)).thenReturn(product);
        when(product.getPrice()).thenReturn(1000);
        when(product.getCurrentPrice()).thenReturn(1000);

        assertTrue(productService.updateProductCurrentPrice(1, 1000));

        verify(product).setCurrentPrice(1000);
        verify(product).setDiscount(0);
    }

    @Test
    public void updateProductCurrentPrice_positiveLessThanPrice_true() {
        Product product = Mockito.mock(Product.class);

        when(productDAO.getProductById(1)).thenReturn(product);
        when(product.getPrice()).thenReturn(1000);
        when(product.getCurrentPrice()).thenReturn(500);

        assertTrue(productService.updateProductCurrentPrice(1, 500));

        verify(product).setCurrentPrice(500);
        verify(product).setDiscount(50);
    }

    @Test
    public void updateProductCurrentPrice_greaterThanPrice_false() {
        Product product = Mockito.mock(Product.class);

        when(productDAO.getProductById(1)).thenReturn(product);
        when(product.getPrice()).thenReturn(1000);

        assertFalse(productService.updateProductCurrentPrice(1, 1000000));

        verify(product, never()).setCurrentPrice(anyInt());
        verify(product, never()).setDiscount(anyInt());
    }

    @Test
    public void updateProductCurrentPrice_0_false() {
        Product product = Mockito.mock(Product.class);

        when(productDAO.getProductById(1)).thenReturn(product);
        when(product.getPrice()).thenReturn(1000);

        assertFalse(productService.updateProductCurrentPrice(1, 0));

        verify(product, never()).setCurrentPrice(anyInt());
        verify(product, never()).setDiscount(anyInt());
    }

    @Test
    public void updateProductCurrentPrice_negative_false() {
        Product product = Mockito.mock(Product.class);

        when(productDAO.getProductById(1)).thenReturn(product);
        when(product.getPrice()).thenReturn(1000);

        assertFalse(productService.updateProductCurrentPrice(1, -10));

        verify(product, never()).setCurrentPrice(anyInt());
        verify(product, never()).setDiscount(anyInt());
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateProductLeftCount_productNotFound_IllegalArgumentException() {
        when(productDAO.getProductById(1)).thenReturn(null);

        productService.updateProductLeftCount(1, 100);
    }

    @Test
    public void updateProductLeftCount_negative_false() {
        Product product = Mockito.mock(Product.class);

        when(productDAO.getProductById(1)).thenReturn(product);

        assertFalse(productService.updateProductLeftCount(1, -10));

        verify(product, never()).setLeftCount(anyInt());
    }

    @Test
    public void updateProductLeftCount_0_true() {
        Product product = Mockito.mock(Product.class);

        when(productDAO.getProductById(1)).thenReturn(product);

        assertTrue(productService.updateProductLeftCount(1, 0));

        verify(product).setLeftCount(0);
    }

    @Test
    public void updateProductLeftCount_positive_true() {
        Product product = Mockito.mock(Product.class);

        when(productDAO.getProductById(1)).thenReturn(product);

        assertTrue(productService.updateProductLeftCount(1, 100));

        verify(product).setLeftCount(100);
    }
}