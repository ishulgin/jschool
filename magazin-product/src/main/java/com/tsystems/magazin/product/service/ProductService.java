package com.tsystems.magazin.product.service;

import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.CountUpdateRequestDTO;
import com.tsystems.magazin.domain.dto.request.ProductRequestDTO;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface ProductService {
    @NotNull List<ProductDTO> getProducts();

    ProductDTO getProductInfo(int productId);

    int saveProduct(@NotNull ProductRequestDTO productRequestDTO);

    void deleteProduct(int productId);

    List<ProductDTO> getLastAddedProducts(int n);

    List<ProductDTO> getMostPopularProducts(int n);

    List<ProductDTO> getBestSalesOffers(int n);

    boolean setProductImages(int productId, @NotNull List<String> productImages);

    boolean updateProductLeftCount(int id, int count);

    boolean updateProductPrice(int id, int price);

    boolean updateProductCurrentPrice(int id, int currentPrice);

    void updateProductsCount(@NotNull List<CountUpdateRequestDTO> countUpdateRequestDTO);
}
