package com.tsystems.magazin.product.service;

import com.tsystems.magazin.domain.dto.CategoryDTO;
import com.tsystems.magazin.domain.dto.FilterTypeDTO;
import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.CategoryRequestDTO;
import com.tsystems.magazin.product.dao.CategoryDAO;
import com.tsystems.magazin.product.dao.FilterDAO;
import com.tsystems.magazin.product.dao.ProductDAO;
import com.tsystems.magazin.product.model.AbstractCategory;
import com.tsystems.magazin.product.model.Category;
import com.tsystems.magazin.product.model.FilterType;
import com.tsystems.magazin.product.util.ModelMapperWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CategoryServiceImpl implements CategoryService {
    private CategoryDAO categoryDAO;

    private FilterDAO filterDAO;

    private ProductDAO productDAO;

    private ModelMapperWrapper modelMapperWrapper;

    @Autowired
    public void setCategoryDAO(CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    @Autowired
    public void setFilterDAO(FilterDAO filterDAO) {
        this.filterDAO = filterDAO;
    }

    @Autowired
    public void setProductDAO(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    @Autowired
    public void setModelMapperWrapper(ModelMapperWrapper modelMapperWrapper) {
        this.modelMapperWrapper = modelMapperWrapper;
    }

    @Override
    public CategoryDTO getAbstractCategoryById(int abstractCategoryId) {
        AbstractCategory abstractCategory = categoryDAO.getAbstractCategoryById(abstractCategoryId);

        if (abstractCategory == null) {
            log.debug("Id {}: Can't find abstract category.", abstractCategoryId);

            return null;
        }

        return modelMapperWrapper.mapCategory(abstractCategory);
    }

    @Override
    public CategoryDTO getRealCategoryById(int categoryId) {
        Category category = categoryDAO.getCategoryById(categoryId);

        if (category == null) {
            log.debug("Id {}: Can't find category.", categoryId);

            return null;
        }

        return modelMapperWrapper.mapCategory(category);
    }

    @Transactional
    @Override
    public void saveCategory(@NotNull CategoryRequestDTO categoryRequestDTO) {
        if (categoryRequestDTO.getParentId() == null) {
            if (!categoryRequestDTO.getFilterTypesIds().isEmpty()) {
                log.debug("{}: Department should not have filters.", categoryRequestDTO.getName());

                throw new IllegalArgumentException();
            }

            AbstractCategory department = new AbstractCategory();
            department.setName(categoryRequestDTO.getName());

            categoryDAO.saveAbstractCategory(department);

            log.info("{}: New department was created.", categoryRequestDTO.getName());
        } else {
            AbstractCategory parent = categoryDAO.getAbstractCategoryById(categoryRequestDTO.getParentId());

            if (parent == null) {
                log.debug("{}: Invalid parent category id ({}) was specified.", categoryRequestDTO.getName(), categoryRequestDTO.getParentId());

                throw new IllegalArgumentException();
            }

            if (categoryRequestDTO.getFilterTypesIds().isEmpty()) {
                if (!parent.getChildren().isEmpty()) {
                    log.info("{}: Category can't contain children of different types.", categoryRequestDTO.getName());

                    throw new IllegalArgumentException();
                }

                AbstractCategory abstractCategory = new AbstractCategory();
                abstractCategory.setName(categoryRequestDTO.getName());
                abstractCategory.setParent(parent);

                categoryDAO.saveAbstractCategory(abstractCategory);

                log.info("{}: Abstract category was created.", categoryRequestDTO.getName());
            } else {
                if (!parent.getAbstractChildren().isEmpty()) {
                    log.info("{}: Category can't contain children of different types.", categoryRequestDTO.getName());

                    throw new IllegalArgumentException();
                }

                if (categoryDAO.getTopLevelCategories().contains(parent)) {
                    log.info("{}: Department can't contain real categories.", categoryRequestDTO.getName());

                    throw new IllegalArgumentException();
                }

                Category category = new Category();
                category.setName(categoryRequestDTO.getName());
                category.setParent(parent);

                List<FilterType> filterTypes = new ArrayList<>();

                for (Integer filterTypeId : categoryRequestDTO.getFilterTypesIds()) {
                    FilterType filterType = filterDAO.getFilterTypeById(filterTypeId);

                    if (filterType == null) {
                        log.debug("{}: Filter type with id {} doesn't exist.", categoryRequestDTO.getName(), filterTypeId);

                        throw new IllegalArgumentException();
                    }

                    filterTypes.add(filterType);
                }

                category.setFilterTypes(filterTypes);

                log.info("{}: Category was created.", categoryRequestDTO.getName());

                categoryDAO.saveCategory(category);
            }
        }
    }

    @NotNull
    @Override
    @Transactional
    public List<CategoryDTO> getCategoriesTree() {
        return categoryDAO.getTopLevelCategories()
                .stream()
                .map(modelMapperWrapper::mapCategoryWithChildren)
                .peek(it -> it.setChildren(getChildrenTree(it.getId())))
                .collect(Collectors.toList());
    }

    @NotNull
    @Transactional
    @Override
    public List<CategoryDTO> getDepartmentsWithFirstLevelChildren() {
        return categoryDAO.getTopLevelCategories()
                .stream()
                .map(modelMapperWrapper::mapCategoryWithChildren)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<CategoryDTO> getChildrenTreeWithoutDefectiveChildren(int categoryId) {
        AbstractCategory category = categoryDAO.getAbstractCategoryById(categoryId);

        if (category == null) {
            log.debug("Id {}: Category doesn't exist.", categoryId);

            return null;
        }

        if (!category.getAbstractChildren().isEmpty()) {
            List<CategoryDTO> categories = modelMapperWrapper.mapCategoryWithChildren(category).getChildren();
            Iterator<CategoryDTO> iterator = categories.iterator();

            while (iterator.hasNext()) {
                CategoryDTO currentCategory = iterator.next();

                List<CategoryDTO> children = getChildrenTreeWithoutDefectiveChildren(currentCategory.getId());

                if (children.isEmpty() && currentCategory.getChildren().isEmpty()) {
                    iterator.remove();
                } else if (!children.isEmpty()) {
                    currentCategory.setChildren(children);
                }
            }

            return categories;
        } else if (!category.getChildren().isEmpty()) {
            return getRealChildren(category);
        }

        log.debug("Id {}: No children found.");

        return Collections.emptyList();
    }

    @Transactional
    @Override
    public List<CategoryDTO> getChildrenTree(int categoryId) {
        AbstractCategory category = categoryDAO.getAbstractCategoryById(categoryId);

        if (category == null) {
            log.debug("Id {}: Category doesn't exist.", categoryId);

            return null;
        }

        if (!category.getAbstractChildren().isEmpty()) {
            return modelMapperWrapper.mapCategoryWithChildren(category).getChildren()
                    .stream()
                    .peek(it -> it.setChildren(getChildrenTree(it.getId())))
                    .collect(Collectors.toList());
        } else if (!category.getChildren().isEmpty()) {
            return getRealChildren(category);
        }

        log.debug("Id {}: No children found.");

        return Collections.emptyList();
    }

    @NotNull
    private List<CategoryDTO> getRealChildren(@NotNull AbstractCategory category) {
        return category.getChildren()
                .stream()
                .map(modelMapperWrapper::mapCategory)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<FilterTypeDTO> getFilters(int categoryId) {
        Category category = categoryDAO.getCategoryById(categoryId);

        if (category == null) {
            log.debug("Id {}: Category doesn't exist.", categoryId);

            return null;
        }

        return category.getFilterTypes()
                .stream()
                .map(modelMapperWrapper::mapFilterType)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<ProductDTO> getProducts(int categoryId) {
        Category category = categoryDAO.getCategoryById(categoryId);

        if (category == null) {
            log.debug("Id {}: Can't find category.", categoryId);

            return null;
        }

        return productDAO.getProductsByCategoryId(categoryId)
                .stream()
                .map(modelMapperWrapper::mapProduct)
                .collect(Collectors.toList());
    }
}
