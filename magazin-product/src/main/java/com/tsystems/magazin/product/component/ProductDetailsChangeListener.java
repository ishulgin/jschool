package com.tsystems.magazin.product.component;

import com.tsystems.magazin.product.client.MagazinUpdateClient;
import com.tsystems.magazin.product.event.PriceChangeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
public class ProductDetailsChangeListener {
    private MagazinUpdateClient magazinUpdateClient;

    @Autowired
    public void setMagazinUpdateClient(MagazinUpdateClient magazinUpdateClient) {
        this.magazinUpdateClient = magazinUpdateClient;
    }

    @Async
    @TransactionalEventListener
    public void onChange(PriceChangeEvent priceChangeEvent) {
        magazinUpdateClient.initGeneralUpdate();
    }
}
