package com.tsystems.magazin.product.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@EqualsAndHashCode(exclude = "category")
@ToString(exclude = "category")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private int currentPrice;

    @Column(nullable = false)
    private int price;

    @Column(nullable = false, columnDefinition = "default '0'")
    private int discount;

    @Column
    private String description;

    @Column(nullable = false)
    private int leftCount;

    @Column(nullable = false)
    private int weight;

    @Column(nullable = false)
    private String previewImage;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "ProductImage", joinColumns = @JoinColumn(name = "productId"))
    @Column(name = "path")
    private List<String> productImages;

    @ManyToMany
    @JoinTable(name = "ProductFilterValue", joinColumns = @JoinColumn(name = "productId"), inverseJoinColumns = @JoinColumn(name = "filterValueId"))
    @OrderBy("value")
    private List<FilterValue> filterValues;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryId")
    private Category category;

    public void decreaseCount(int n) {
        leftCount -= n;
    }

    public void addFilterValue(FilterValue filterValue) {
        getFilterValues().add(filterValue);
    }

    public String getImgDirectoryName() {
        return id + "_" + name.replaceAll(" ", "_")
                .replaceAll("\\W", "")
                .toLowerCase() + "/";
    }
}
