package com.tsystems.magazin.product.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("magazin-stat")
public interface MagazinStatClient {
    @GetMapping("/stats/products/top")
    List<Integer> getMostPopularProductsIds(@RequestParam("n") int n);
}
