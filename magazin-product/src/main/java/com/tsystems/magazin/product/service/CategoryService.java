package com.tsystems.magazin.product.service;

import com.tsystems.magazin.domain.dto.CategoryDTO;
import com.tsystems.magazin.domain.dto.FilterTypeDTO;
import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.CategoryRequestDTO;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface CategoryService {
    CategoryDTO getAbstractCategoryById(int abstractCategoryId);

    CategoryDTO getRealCategoryById(int categoryId);

    void saveCategory(@NotNull CategoryRequestDTO categoryRequestDTO);

    @NotNull List<CategoryDTO> getCategoriesTree();

    @NotNull List<CategoryDTO> getDepartmentsWithFirstLevelChildren();

    List<CategoryDTO> getChildrenTreeWithoutDefectiveChildren(int id);

    List<CategoryDTO> getChildrenTree(int id);

    List<FilterTypeDTO> getFilters(int categoryId);

    List<ProductDTO> getProducts(int categoryId);
}
