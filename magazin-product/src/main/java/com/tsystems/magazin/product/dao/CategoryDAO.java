package com.tsystems.magazin.product.dao;

import com.tsystems.magazin.product.model.AbstractCategory;
import com.tsystems.magazin.product.model.Category;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface CategoryDAO {
    void saveAbstractCategory(@NotNull AbstractCategory abstractCategory);

    void saveCategory(@NotNull Category category);

    AbstractCategory getAbstractCategoryById(int id);

    Category getCategoryById(int id);

    @NotNull List<AbstractCategory> getTopLevelCategories();
}
