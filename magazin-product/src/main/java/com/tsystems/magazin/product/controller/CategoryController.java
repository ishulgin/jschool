package com.tsystems.magazin.product.controller;

import com.tsystems.magazin.domain.dto.CategoryDTO;
import com.tsystems.magazin.domain.dto.FilterTypeDTO;
import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.CategoryRequestDTO;
import com.tsystems.magazin.product.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CategoryController {
    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(value = "/departments")
    public ResponseEntity<List<CategoryDTO>> getDepartments() {
        return ResponseEntity.ok(categoryService.getDepartmentsWithFirstLevelChildren());
    }

    @GetMapping(value = "/categories")
    public ResponseEntity<List<CategoryDTO>> getCategoriesTree() {
        return ResponseEntity.ok(categoryService.getCategoriesTree());
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping(value = "/categories")
    public ResponseEntity addCategory(@RequestBody @Valid CategoryRequestDTO categoryRequestDTO) {
        try {
            categoryService.saveCategory(categoryRequestDTO);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping(value = "/categories/abstract/{id}")
    public ResponseEntity<CategoryDTO> getRealCategoryInfo(@PathVariable("id") int categoryId) {
        return ResponseEntity.ok(categoryService.getRealCategoryById(categoryId));
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping(value = "/categories/real/{id}")
    public ResponseEntity<CategoryDTO> getAbstractCategoryInfo(@PathVariable("id") int categoryId) {
        return ResponseEntity.ok(categoryService.getAbstractCategoryById(categoryId));
    }

    @GetMapping(value = "/categories/{id}/children")
    public ResponseEntity<List<CategoryDTO>> getChildren(@PathVariable("id") int categoryId) {
        return ResponseEntity.ok(categoryService.getChildrenTreeWithoutDefectiveChildren(categoryId));
    }

    @GetMapping(value = "/categories/{id}/filters")
    public ResponseEntity<List<FilterTypeDTO>> getFilters(@PathVariable("id") int categoryId) {
        return ResponseEntity.ok(categoryService.getFilters(categoryId));
    }

    @GetMapping(value = "/categories/{id}/products")
    public ResponseEntity<List<ProductDTO>> getProducts(@PathVariable("id") int categoryId) {
        return ResponseEntity.ok(categoryService.getProducts(categoryId));
    }
}
