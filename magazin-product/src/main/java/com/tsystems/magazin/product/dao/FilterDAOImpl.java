package com.tsystems.magazin.product.dao;

import com.tsystems.magazin.product.model.FilterType;
import com.tsystems.magazin.product.model.FilterValue;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public class FilterDAOImpl implements FilterDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public FilterType getFilterTypeById(int id) {
        return entityManager.find(FilterType.class, id);
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<FilterType> getFilters() {
        return (List<FilterType>) entityManager.createQuery("select f from FilterType f").getResultList();
    }

    @Override
    public void saveFilterType(@NotNull FilterType filterType) {
        entityManager.persist(filterType);
    }

    @Override
    public FilterValue getFilterValueById(int id) {
        return entityManager.find(FilterValue.class, id);
    }
}
