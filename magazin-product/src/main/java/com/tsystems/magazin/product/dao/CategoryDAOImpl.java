package com.tsystems.magazin.product.dao;

import com.tsystems.magazin.product.model.AbstractCategory;
import com.tsystems.magazin.product.model.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public class CategoryDAOImpl implements CategoryDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void saveAbstractCategory(@NotNull AbstractCategory abstractCategory) {
        entityManager.persist(abstractCategory);
    }

    @Override
    public void saveCategory(@NotNull Category category) {
        entityManager.persist(category);
    }

    @Override
    public AbstractCategory getAbstractCategoryById(int id) {
        return entityManager.find(AbstractCategory.class, id);
    }

    @Override
    public Category getCategoryById(int id) {
        return entityManager.find(Category.class, id);
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<AbstractCategory> getTopLevelCategories() {
        Query query = entityManager.createQuery("select c from AbstractCategory c where c.parent is null order by c.name");

        return (List<AbstractCategory>) query.getResultList();
    }
}
