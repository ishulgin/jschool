package com.tsystems.magazin.product.service;

import com.tsystems.magazin.domain.dto.FilterTypeDTO;
import com.tsystems.magazin.domain.dto.FilterValueDTO;
import com.tsystems.magazin.domain.dto.request.FilterTypeRequestDTO;
import com.tsystems.magazin.domain.dto.request.FilterValueRequestDTO;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface FilterService {
    List<FilterValueDTO> getFilterValuesByFilterTypeId(int filterTypeId);

    @NotNull List<FilterTypeDTO> getAllFilters();

    void saveFilter(@NotNull FilterTypeRequestDTO filterTypeRequestDTO);

    void addFilterValue(int filterTypeId, @NotNull FilterValueRequestDTO value);
}
