package com.tsystems.magazin.product.dao;

import com.tsystems.magazin.product.model.Product;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface ProductDAO {
    @NotNull List<Product> getProducts();

    Product getProductById(int id);

    @NotNull List<Product> getProductsByCategoryId(int categoryId);

    void saveProduct(@NotNull Product product);

    @NotNull List<Product> getLastProducts(int n);

    @NotNull List<Product> getBestSalesOffers(int n);

    void deleteProduct(@NotNull Product product);
}
