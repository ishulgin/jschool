package com.tsystems.magazin.product.service;

import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.CountUpdateRequestDTO;
import com.tsystems.magazin.domain.dto.request.ProductRequestDTO;
import com.tsystems.magazin.product.client.MagazinStatClient;
import com.tsystems.magazin.product.dao.CategoryDAO;
import com.tsystems.magazin.product.dao.FilterDAO;
import com.tsystems.magazin.product.dao.ProductDAO;
import com.tsystems.magazin.product.model.Category;
import com.tsystems.magazin.product.model.FilterType;
import com.tsystems.magazin.product.model.FilterValue;
import com.tsystems.magazin.product.model.Product;
import com.tsystems.magazin.product.util.ModelMapperWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductServiceImpl implements ProductService {
    private ProductDAO productDAO;

    private CategoryDAO categoryDAO;

    private FilterDAO filterDAO;

    private MagazinStatClient magazinStatClient;

    private ModelMapperWrapper modelMapperWrapper;

    private ApplicationEventPublisher applicationEventPublisher;

    private static void setDiscount(Product product) {
        int price = product.getPrice();
        int currentPrice = product.getCurrentPrice();

        product.setDiscount(100 * (price - currentPrice) / price);
    }

    @Autowired
    public void setProductDAO(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    @Autowired
    public void setCategoryDAO(CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    @Autowired
    public void setFilterDAO(FilterDAO filterDAO) {
        this.filterDAO = filterDAO;
    }

    @Autowired
    public void setMagazinStatClient(MagazinStatClient magazinStatClient) {
        this.magazinStatClient = magazinStatClient;
    }

    @Autowired
    public void setModelMapperWrapper(ModelMapperWrapper modelMapperWrapper) {
        this.modelMapperWrapper = modelMapperWrapper;
    }

    @Autowired
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @NotNull
    @Transactional
    @Override
    public List<ProductDTO> getProducts() {
        return productDAO.getProducts()
                .stream()
                .map(modelMapperWrapper::mapProduct)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public ProductDTO getProductInfo(int productId) {
        Product product = productDAO.getProductById(productId);

        if (product == null) {
            log.debug("Id {}: Product not found.", productId);

            return null;
        }

        return modelMapperWrapper.mapProduct(product);
    }

    @Transactional
    @Override
    public int saveProduct(@NotNull ProductRequestDTO productRequestDTO) {
        if (productRequestDTO.getFiltersIds() == null) {
            log.debug("{}: No filters specified", productRequestDTO.getName());

            throw new IllegalArgumentException();
        }

        Category category = categoryDAO.getCategoryById(productRequestDTO.getCategoryId());

        if (category == null) {
            log.debug("{}: Category with id {} doesn't exist.", productRequestDTO.getName(), productRequestDTO.getCategoryId());

            throw new IllegalArgumentException();
        }

        Product product = modelMapperWrapper.mapProductRequestDTO(productRequestDTO);
        product.setCategory(category);
        product.setFilterValues(new ArrayList<>());

        Set<Integer> associatedFilterTypes = new HashSet<>();

        for (Integer filterValueId : productRequestDTO.getFiltersIds()) {
            FilterValue filterValue = filterDAO.getFilterValueById(filterValueId);

            if (filterValue == null) {
                log.debug("{}: Filter value with id {} doesn't exist.", productRequestDTO.getName(), filterValueId);

                throw new IllegalArgumentException();
            }

            int filterTypeId = filterValue.getFilterType().getId();

            if (associatedFilterTypes.contains(filterTypeId)) {
                log.debug("{}: Product can't have 2 filters of the same type.", productRequestDTO.getName());

                throw new IllegalArgumentException();
            }

            associatedFilterTypes.add(filterTypeId);
            product.addFilterValue(filterValue);
        }

        Set<Integer> realFilterTypes = category.getFilterTypes()
                .stream()
                .map(FilterType::getId)
                .collect(Collectors.toSet());

        if (!realFilterTypes.equals(associatedFilterTypes)) {
            log.debug("{}: Filters associated with category and product don't match.", productRequestDTO.getName());

            throw new IllegalArgumentException();
        }

        productDAO.saveProduct(product);

        log.info("{}: Product was saved with id {}.", product.getName(), product.getId());

        return product.getId();
    }

    @Transactional
    @Override
    public void deleteProduct(int productId) {
        Product product = productDAO.getProductById(productId);

        if (product == null) {
            log.debug("Id {}: Product not found.", productId);

            throw new IllegalArgumentException();
        }

        productDAO.deleteProduct(product);

        log.info("Id {}: product was deleted.", productId);
    }

    @Transactional
    @Override
    public List<ProductDTO> getLastAddedProducts(int n) {
        try {
            return productDAO.getLastProducts(n)
                    .stream()
                    .map(modelMapperWrapper::mapProduct)
                    .collect(Collectors.toList());
        } catch (IllegalArgumentException e) {
            log.debug("Unable to get {} last added products.", n);

            return null;
        }
    }

    @Transactional
    @Override
    public List<ProductDTO> getMostPopularProducts(int n) {
        List<Integer> productsIds = magazinStatClient.getMostPopularProductsIds(n);

        if (productsIds == null) {
            log.debug("Unable to get {} most popular products.", n);

            return null;
        }

        List<ProductDTO> result = new ArrayList<>();

        for (int productId : productsIds) {
            Product product = productDAO.getProductById(productId);

            if (product == null) {
                log.warn("Id {}: Can't find product.", productId);

                return null;
            }

            result.add(modelMapperWrapper.mapProduct(product));
        }

        return result;
    }

    @Transactional
    @Override
    public List<ProductDTO> getBestSalesOffers(int n) {
        try {
            return productDAO.getBestSalesOffers(n)
                    .stream()
                    .map(modelMapperWrapper::mapProduct)
                    .collect(Collectors.toList());
        } catch (IllegalArgumentException e) {
            log.debug("Unable to get {} products with highest discount.", n);

            return null;
        }
    }

    @Transactional
    @Override
    public boolean setProductImages(int productId, @NotNull List<String> productImages) {
        Product product = productDAO.getProductById(productId);

        if (product == null) {
            log.debug("Id {}: Product not found.", productId);

            throw new IllegalArgumentException();
        }


        if (product.getProductImages().isEmpty()) {
            product.setProductImages(productImages);
            product.setPreviewImage(productImages.get(0));

            log.info("Id {}: Images were set.", productId);

            return true;
        }

        log.debug("Id {}: Product's images are already set.", productId);

        return false;
    }

    @Transactional
    @Override
    public boolean updateProductLeftCount(int productId, int count) {
        Product product = productDAO.getProductById(productId);

        if (product == null) {
            log.debug("Id {}: Product not found.", productId);

            throw new IllegalArgumentException();
        }

        if (count < 0) {
            log.info("Id {}: Unable to set count equals to {}.", productId, count);

            return false;
        }

        product.setLeftCount(count);

        log.info("Id {}: Count was set to {}.", productId, count);

        return true;
    }

    @Transactional
    @Override
    public boolean updateProductPrice(int productId, int price) {
        Product product = productDAO.getProductById(productId);

        if (product == null) {
            log.debug("Id {}: Product not found.", productId);

            throw new IllegalArgumentException();
        }

        if (price <= 0) {
            log.info("Id {}: Unable to set current price equals to {}.", productId, price);

            return false;
        }

        product.setPrice(price);
        product.setCurrentPrice(price);
        product.setDiscount(0);

        log.info("Id {}: Price was set to {}.", productId, price);

        applicationEventPublisher.publishEvent(modelMapperWrapper.mapProductToPriceChangeEvent(product));

        return true;
    }

    @Transactional
    @Override
    public boolean updateProductCurrentPrice(int productId, int currentPrice) {
        Product product = productDAO.getProductById(productId);

        if (product == null) {
            log.debug("Id {}: Product not found.", productId);

            throw new IllegalArgumentException();
        }

        if (currentPrice > product.getPrice() || currentPrice <= 0) {
            log.info("Id {}: Unable to set current price equals to {}.", productId, currentPrice);

            return false;
        }

        product.setCurrentPrice(currentPrice);
        setDiscount(product);

        log.info("Id {}: Current price was set to {}.", productId, currentPrice);

        applicationEventPublisher.publishEvent(modelMapperWrapper.mapProductToPriceChangeEvent(product));

        return true;
    }

    @Transactional
    @Override
    public void updateProductsCount(@NotNull List<CountUpdateRequestDTO> countUpdateRequestDTOs) {
        List<String> updates = new ArrayList<>();

        for (CountUpdateRequestDTO countUpdateRequestDTO : countUpdateRequestDTOs) {
            Product product = productDAO.getProductById(countUpdateRequestDTO.getProductId());

            if (product == null) {
                log.debug("Id {}: Product not found.", countUpdateRequestDTO.getProductId());

                throw new IllegalArgumentException();
            }

            if (product.getLeftCount() < countUpdateRequestDTO.getCount()) {
                log.debug("Id {}: Product not found.", countUpdateRequestDTO.getProductId());

                throw new IllegalArgumentException();
            }

            String record = "{" +
                    product.getName() +
                    "; " +
                    product.getLeftCount() +
                    " -> " +
                    (product.getLeftCount() - countUpdateRequestDTO.getCount()) +
                    "}";

            updates.add(record);

            product.decreaseCount(countUpdateRequestDTO.getCount());
        }

        log.info("Products' count was updated: {}.", updates);
    }
}
