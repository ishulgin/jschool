package com.tsystems.magazin.product.controller;

import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.CountUpdateRequestDTO;
import com.tsystems.magazin.domain.dto.request.ProductRequestDTO;
import com.tsystems.magazin.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
public class ProductController {
    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/products")
    public ResponseEntity<Integer> addProduct(@RequestBody @Valid ProductRequestDTO productRequestDTO) {
        try {
            return ResponseEntity.ok(productService.saveProduct(productRequestDTO));

        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @DeleteMapping("/products")
    public ResponseEntity deleteProduct(@RequestBody int productId) {
        try {
            productService.deleteProduct(productId);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<ProductDTO> getProductInfo(@PathVariable("id") int productId) {
        return ResponseEntity.ok(productService.getProductInfo(productId));
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/products/{id}/images")
    public ResponseEntity<Boolean> setImages(@PathVariable("id") int productId, @RequestBody @NotNull List<@NotNull String> productImages) {
        try {
            return ResponseEntity.ok(productService.setProductImages(productId, productImages));
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/products/{id}/count")
    public ResponseEntity<Boolean> updateCount(@PathVariable("id") int productId, @RequestBody int leftCount) {
        try {
            return ResponseEntity.ok(productService.updateProductLeftCount(productId, leftCount));
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/products/{id}/price/general")
    public ResponseEntity<Boolean> updatePrice(@PathVariable("id") int id, @RequestBody int price) {
        try {
            return ResponseEntity.ok(productService.updateProductPrice(id, price));
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/products/{id}/price/current")
    public ResponseEntity<Boolean> updateCurrentPrice(@PathVariable("id") int id, @RequestBody int currentPrice) {
        try {
            return ResponseEntity.ok(productService.updateProductCurrentPrice(id, currentPrice));
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/products/count")
    public ResponseEntity updateProductsCount(@RequestBody @Valid List<CountUpdateRequestDTO> countUpdateRequestDTO) {
        try {
            productService.updateProductsCount(countUpdateRequestDTO);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/stats/top")
    public ResponseEntity<List<ProductDTO>> getMostPopularProducts(@RequestParam("n") int n) {
        return ResponseEntity.ok(productService.getMostPopularProducts(n));
    }

    @GetMapping("/stats/last")
    public ResponseEntity<List<ProductDTO>> getLastAddedProducts(@RequestParam("n") int n) {
        return ResponseEntity.ok(productService.getLastAddedProducts(n));
    }

    @GetMapping("/stats/sales")
    public ResponseEntity<List<ProductDTO>> getBestSalesOffers(@RequestParam("n") int n) {
        return ResponseEntity.ok(productService.getBestSalesOffers(n));
    }
}
