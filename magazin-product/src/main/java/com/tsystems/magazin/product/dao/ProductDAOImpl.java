package com.tsystems.magazin.product.dao;

import com.tsystems.magazin.product.model.Product;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public class ProductDAOImpl implements ProductDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<Product> getProducts() {
        return entityManager.createQuery("select p from Product p").getResultList();
    }

    @Override
    public Product getProductById(int id) {
        return entityManager.find(Product.class, id);
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<Product> getProductsByCategoryId(int categoryId) {
        Query query = entityManager.createQuery("select p from Product p where p.category.id = :categoryId and p.leftCount > 0")
                .setParameter("categoryId", categoryId);

        return (List<Product>) query.getResultList();
    }

    @Override
    public void saveProduct(@NotNull Product product) {
        entityManager.persist(product);
        entityManager.flush();
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<Product> getLastProducts(int n) {
        Query query = entityManager.createQuery("select p from Product p order by p.id desc")
                .setMaxResults(n);

        return (List<Product>) query.getResultList();
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<Product> getBestSalesOffers(int n) {
        Query query = entityManager.createQuery("select p from Product p where p.discount <> 0 order by p.discount desc")
                .setMaxResults(n);

        return (List<Product>) query.getResultList();
    }

    @Override
    public void deleteProduct(@NotNull Product product) {
        entityManager.remove(product);
    }
}
