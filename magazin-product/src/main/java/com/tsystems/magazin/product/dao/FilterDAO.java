package com.tsystems.magazin.product.dao;

import com.tsystems.magazin.product.model.FilterType;
import com.tsystems.magazin.product.model.FilterValue;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface FilterDAO {
    FilterType getFilterTypeById(int id);

    @NotNull List<FilterType> getFilters();

    void saveFilterType(@NotNull FilterType filterType);

    FilterValue getFilterValueById(int id);
}
