package com.tsystems.magazin.product.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient("magazin-update")
public interface MagazinUpdateClient {
    @PostMapping("/products/top")
    void initTopUpdate();

    @PostMapping("/products/sales")
    void initSalesUpdate();

    @PostMapping("/products/general")
    void initGeneralUpdate();
}
