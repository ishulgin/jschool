package com.tsystems.magazin.product.service;

import com.tsystems.magazin.domain.dto.FilterTypeDTO;
import com.tsystems.magazin.domain.dto.FilterValueDTO;
import com.tsystems.magazin.domain.dto.request.FilterTypeRequestDTO;
import com.tsystems.magazin.domain.dto.request.FilterValueRequestDTO;
import com.tsystems.magazin.product.dao.FilterDAO;
import com.tsystems.magazin.product.model.FilterType;
import com.tsystems.magazin.product.model.FilterValue;
import com.tsystems.magazin.product.util.ModelMapperWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class FilterServiceImpl implements FilterService {
    private FilterDAO filterDAO;

    private ModelMapperWrapper modelMapperWrapper;

    @Autowired
    public void setFilterDAO(FilterDAO filterDAO) {
        this.filterDAO = filterDAO;
    }

    @Autowired
    public void setModelMapperWrapper(ModelMapperWrapper modelMapperWrapper) {
        this.modelMapperWrapper = modelMapperWrapper;
    }

    @Override
    @Transactional
    public List<FilterValueDTO> getFilterValuesByFilterTypeId(int filterTypeId) {
        FilterType filterType = filterDAO.getFilterTypeById(filterTypeId);

        if (filterType == null) {
            log.debug("Id {}: not found.", filterTypeId);

            return null;
        }

        return modelMapperWrapper.mapFilterType(filterType).getValues();
    }

    @NotNull
    @Override
    @Transactional
    public List<FilterTypeDTO> getAllFilters() {
        return filterDAO.getFilters()
                .stream()
                .map(modelMapperWrapper::mapFilterType)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void saveFilter(@NotNull FilterTypeRequestDTO filterTypeRequestDTO) {
        filterDAO.saveFilterType(modelMapperWrapper.mapFilterTypeRequestDTO(filterTypeRequestDTO));

        log.info("{}: Created.", filterTypeRequestDTO.getName());
    }

    @Override
    @Transactional
    public void addFilterValue(int filterTypeId, @NotNull FilterValueRequestDTO value) {
        FilterType filterType = filterDAO.getFilterTypeById(filterTypeId);

        if (filterType == null) {
            log.debug("Id {}: not found.", filterTypeId);

            throw new IllegalArgumentException();
        }

        FilterValue filterValue = new FilterValue();
        filterValue.setFilterType(filterType);
        filterValue.setValue(value.getValue());

        filterType.addValue(filterValue);

        log.info("Id {}: Value {} was added.", filterTypeId, value.getValue());
    }
}
