package com.tsystems.magazin.product.controller;

import com.tsystems.magazin.domain.dto.FilterTypeDTO;
import com.tsystems.magazin.domain.dto.FilterValueDTO;
import com.tsystems.magazin.domain.dto.request.FilterTypeRequestDTO;
import com.tsystems.magazin.domain.dto.request.FilterValueRequestDTO;
import com.tsystems.magazin.product.service.FilterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class FilterController {
    private FilterService filterService;

    @Autowired
    public void setFilterService(FilterService filterService) {
        this.filterService = filterService;
    }

    @GetMapping("/filters")
    public ResponseEntity<List<FilterTypeDTO>> getAllFilters() {
        return ResponseEntity.ok(filterService.getAllFilters());
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/filters")
    public ResponseEntity createFilter(@RequestBody @Valid FilterTypeRequestDTO filter) {
        try {
            filterService.saveFilter(filter);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/filters/{id}/values")
    public ResponseEntity<List<FilterValueDTO>> getFilterValues(@PathVariable int id) {
        List<FilterValueDTO> filterValues = filterService.getFilterValuesByFilterTypeId(id);

        if (filterValues == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(filterValues);
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/filters/{id}/values")
    public ResponseEntity addFilterValue(@PathVariable int id, @RequestBody @Valid FilterValueRequestDTO filterValueRequestDTO) {
        try {
            filterService.addFilterValue(id, filterValueRequestDTO);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
