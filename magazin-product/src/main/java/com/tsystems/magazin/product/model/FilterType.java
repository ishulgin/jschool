package com.tsystems.magazin.product.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class FilterType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @OneToMany(mappedBy = "filterType", cascade = CascadeType.ALL)
    @OrderBy("value")
    private List<FilterValue> values;

    public void addValue(FilterValue filterValue) {
        getValues().add(filterValue);
    }
}
