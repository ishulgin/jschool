package com.tsystems.magazin.product.util;

import com.tsystems.magazin.domain.dto.CategoryDTO;
import com.tsystems.magazin.domain.dto.FilterTypeDTO;
import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.FilterTypeRequestDTO;
import com.tsystems.magazin.domain.dto.request.ProductRequestDTO;
import com.tsystems.magazin.product.event.PriceChangeEvent;
import com.tsystems.magazin.product.model.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.stream.Collectors;

@Component
public class ModelMapperWrapper {
    private ModelMapper modelMapper = new ModelMapper();

    public ModelMapperWrapper() {
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.LOOSE);

        modelMapper.getConfiguration().setAmbiguityIgnored(true);

        modelMapper.createTypeMap(ProductRequestDTO.class, Product.class)
                .addMappings(mapper -> mapper.skip(Product::setId));

        modelMapper.createTypeMap(Category.class, CategoryDTO.class)
                .addMappings(mapper -> mapper.skip(CategoryDTO::setChildren));
    }

    @NotNull
    public FilterTypeDTO mapFilterType(@NotNull FilterType filterType) {
        return modelMapper.map(filterType, FilterTypeDTO.class);
    }

    @NotNull
    public FilterType mapFilterTypeRequestDTO(@NotNull FilterTypeRequestDTO filterTypeRequestDTO) {
        FilterType filterType = modelMapper.map(filterTypeRequestDTO, FilterType.class);
        filterType.setValues(filterTypeRequestDTO.getValues()
                .stream()
                .map(it -> mapFilterValue(it, filterType))
                .collect(Collectors.toList()));

        return filterType;
    }

    @NotNull
    private FilterValue mapFilterValue(@NotNull String value, @NotNull FilterType filterType) {
        FilterValue filterValue = new FilterValue();
        filterValue.setValue(value);
        filterValue.setFilterType(filterType);

        return filterValue;
    }

    @NotNull
    public CategoryDTO mapCategoryWithChildren(@NotNull AbstractCategory category) {
        CategoryDTO parent = modelMapper.map(category, CategoryDTO.class);
        parent.setChildren(category.getAbstractChildren()
                .stream()
                .map(this::mapCategory)
                .collect(Collectors.toList()));

        return parent;
    }

    @NotNull
    public CategoryDTO mapCategory(@NotNull AbstractCategory category) {
        return modelMapper.map(category, CategoryDTO.class);
    }

    @NotNull
    public CategoryDTO mapCategory(@NotNull Category category) {
        return modelMapper.map(category, CategoryDTO.class);
    }

    @NotNull
    public ProductDTO mapProduct(@NotNull Product product) {
        return modelMapper.map(product, ProductDTO.class);
    }

    @NotNull
    public PriceChangeEvent mapProductToPriceChangeEvent(@NotNull Product product) {
        return modelMapper.map(product, PriceChangeEvent.class);
    }

    @NotNull
    public Product mapProductRequestDTO(@NotNull ProductRequestDTO productRequestDTO) {
        return modelMapper.map(productRequestDTO, Product.class);
    }
}
