CREATE TABLE FilterType (
  id   INT AUTO_INCREMENT,
  name VARCHAR(64) NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;

CREATE TABLE FilterValue (
  id           INT AUTO_INCREMENT,
  value        VARCHAR(64) NOT NULL,
  filterTypeId INT,
  PRIMARY KEY (id),
  FOREIGN KEY (filterTypeId) REFERENCES FilterType (id)
)
  ENGINE = InnoDB;

CREATE TABLE AbstractCategory (
  id       INT AUTO_INCREMENT,
  name     VARCHAR(64) NOT NULL,
  parentId INT,
  PRIMARY KEY (id),
  FOREIGN KEY (parentId) REFERENCES AbstractCategory (id)
)
  ENGINE = InnoDB;

CREATE TABLE Category (
  id           INT AUTO_INCREMENT,
  name         VARCHAR(64) NOT NULL,
  parentId     INT,
  departmentId INT,
  PRIMARY KEY (id),
  FOREIGN KEY (parentId) REFERENCES AbstractCategory (id),
  FOREIGN KEY (departmentId) REFERENCES AbstractCategory (id)
)
  ENGINE = InnoDB;

CREATE TRIGGER SetDepartment
  BEFORE INSERT
  ON Category
  FOR EACH ROW
  BEGIN
    DECLARE tmpParentId INT;
    DECLARE tmpDepartmentId INT;

    SET tmpParentId = NEW.parentId;

    WHILE tmpParentId IS NOT NULL DO
      SELECT AbstractCategory.id, AbstractCategory.parentId
          INTO
            tmpDepartmentId, tmpParentId
      FROM AbstractCategory
      WHERE AbstractCategory.id = tmpParentId;
    END WHILE;

    SET NEW.departmentId = tmpDepartmentId;
  END;

CREATE TABLE CategoryFilterType (
  categoryId   INT,
  filterTypeId INT,
  PRIMARY KEY (categoryId, filterTypeId),
  FOREIGN KEY (categoryId) REFERENCES Category (id),
  FOREIGN KEY (filterTypeId) REFERENCES FilterType (id)
)
  ENGINE = InnoDB;

CREATE TABLE Product (
  id           INT AUTO_INCREMENT,
  name         VARCHAR(128)  NOT NULL,
  currentPrice INT           NOT NULL,
  price        INT           NOT NULL,
  discount     INT DEFAULT 0 NOT NULL,
  description  TEXT,
  leftCount    INT           NOT NULL,
  previewImage VARCHAR(256),
  categoryId   INT,
  PRIMARY KEY (id),
  FOREIGN KEY (categoryId) REFERENCES Category (id)
)
  ENGINE = InnoDB;

CREATE TRIGGER SetCurrentPrice
  BEFORE INSERT
  ON Product
  FOR EACH ROW
  BEGIN
    SET NEW.currentPrice = NEW.price;
  END;

CREATE TABLE ProductFilterValue (
  productId     INT,
  filterValueId INT,
  PRIMARY KEY (productId, filterValueId),
  FOREIGN KEY (productId) REFERENCES Product (id),
  FOREIGN KEY (filterValueId) REFERENCES FilterValue (id)
)
  ENGINE = InnoDB;

CREATE TABLE ProductImage (
  path      VARCHAR(128) NOT NULL,
  productId INT,
  FOREIGN KEY (productId) REFERENCES Product (id)
)
  ENGINE = InnoDB;