package com.tsystems.magazin.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class MagazinConfigApplication {
    public static void main(String[] args) {
        SpringApplication.run(MagazinConfigApplication.class, args);
    }
}
