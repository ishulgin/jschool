package com.tsystems.magazin.stand.config;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import javax.ejb.Singleton;

@Singleton
public class ResteasyWrapper {
    //    TODO gateway address
    public static final String BASIC_URI = "http://localhost:8894/updates";

    public static final int MAX_PRODUCTS = 10;

    public static final ResteasyClient RESTEASY_CLIENT = new ResteasyClientBuilder().build();
}
