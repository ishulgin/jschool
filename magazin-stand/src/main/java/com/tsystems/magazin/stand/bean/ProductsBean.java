package com.tsystems.magazin.stand.bean;

import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.stand.model.SubscriptionType;
import lombok.Getter;
import lombok.Setter;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Named
@SessionScoped
public class ProductsBean implements Serializable {
    @Setter
    @Getter
    private int count = 10;
    @Setter
    @Getter
    private SubscriptionType type = SubscriptionType.TOP;

    @Inject
    private UpdateBean updateBean;

    public List<ProductDTO> getProducts() {
        List<ProductDTO> products = Collections.emptyList();

        switch (type) {
            case TOP:
                products = cut(updateBean.getTopProducts(), count);
                break;
            case SALES:
                products = cut(updateBean.getSalesProducts(), count);
                break;
        }

        return products;
    }

    private List<ProductDTO> cut(List<ProductDTO> list, int n) {
        return list
                .stream()
                .limit(n)
                .collect(Collectors.toList());
    }
}
