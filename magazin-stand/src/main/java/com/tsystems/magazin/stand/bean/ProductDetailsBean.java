package com.tsystems.magazin.stand.bean;

import com.tsystems.magazin.domain.dto.ProductDTO;
import lombok.Getter;
import lombok.Setter;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class ProductDetailsBean implements Serializable {
    @Setter
    @Getter
    private ProductDTO product;
}
