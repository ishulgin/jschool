package com.tsystems.magazin.stand.service;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.stand.config.MQConfig;
import com.tsystems.magazin.stand.config.ResteasyWrapper;
import com.tsystems.magazin.stand.model.ProductMQNotification;
import lombok.extern.log4j.Log4j2;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import javax.ejb.Singleton;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

@Singleton
@Log4j2
public class UpdateService {
    @Inject
    private BeanManager beanManager;

    @Inject
    private MQConfig mqConfig;

    public void init() {
        ConnectionFactory factory = buildConnectionFactory();

        try {
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            bindQueue(channel, MQConfig.Queue.TOP, ((consumerTag, message) ->
                    beanManager.fireEvent(ProductMQNotification.TOP_UPDATE)));

            bindQueue(channel, MQConfig.Queue.SALES, ((consumerTag, message) ->
                    beanManager.fireEvent(ProductMQNotification.SALES_UPDATE)));

            bindQueue(channel, MQConfig.Queue.GENERAL, ((consumerTag, message) ->
                    beanManager.fireEvent(ProductMQNotification.GENERAL_UPDATE)));

            log.info("MQ listeners were initialized.");
        } catch (IOException | TimeoutException e) {
            log.error("Can't establish connection with MQ.", e);
        }
    }

    private ConnectionFactory buildConnectionFactory() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(MQConfig.ConnectionDetails.HOST);
        factory.setPort(MQConfig.ConnectionDetails.PORT);
        factory.setUsername(MQConfig.ConnectionDetails.USER);
        factory.setPassword(MQConfig.ConnectionDetails.PASSWORD);

        return factory;
    }

    private void bindQueue(Channel channel, MQConfig.Queue queue, DeliverCallback deliverCallback) throws IOException {
        channel.queueBind(queue.getName(), mqConfig.getExchangeName(), queue.getKey());
        channel.queuePurge(queue.getName());

        channel.basicConsume(queue.getName(), true, deliverCallback, consumerTag -> {
        });
    }

    public List<ProductDTO> updateTopProducts() {
        log.info("Fetching top update.");

        return getProductsResponse("/products/top/");
    }

    public List<ProductDTO> updateSalesProducts() {
        log.info("Fetching sales update.");

        return getProductsResponse("/products/sales/");
    }

    private List<ProductDTO> getProductsResponse(String endpoint) {
        ResteasyWebTarget target = ResteasyWrapper.RESTEASY_CLIENT.target(ResteasyWrapper.BASIC_URI +
                endpoint +
                ResteasyWrapper.MAX_PRODUCTS);

        Response response = target.request().get();

        return response.readEntity(new GenericType<List<ProductDTO>>() {

        });
    }
}
