package com.tsystems.magazin.stand.bean;

import com.tsystems.magazin.stand.config.ResteasyWrapper;
import com.tsystems.magazin.stand.model.SubscriptionType;
import lombok.Getter;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@Named
@ApplicationScoped
public class DataBean {
    @Getter
    private int minProducts = 1;
    @Getter
    private int maxProducts = ResteasyWrapper.MAX_PRODUCTS;
    @Getter
    private SubscriptionType[] subscriptionTypes = SubscriptionType.values();
}
