package com.tsystems.magazin.stand.bean;

import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.stand.model.ProductMQNotification;
import com.tsystems.magazin.stand.model.SubscriptionType;
import com.tsystems.magazin.stand.service.UpdateService;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
@Log4j2
public class UpdateBean {
    @Getter
    private List<ProductDTO> topProducts;

    @Getter
    private List<ProductDTO> salesProducts;

    @Inject
    private UpdateService updateService;

    @Inject
    @Push(channel = "products")
    private PushContext productsContext;

    @PostConstruct
    public void init() {
        updateService.init();
        updateAllProducts();
    }

    private void onUpdate(@Observes ProductMQNotification notification) {
        log.debug("Going to update.");

        switch (notification) {
            case TOP_UPDATE:
                updateTopProducts();
                break;
            case SALES_UPDATE:
                updateSalesProducts();
                break;
            case GENERAL_UPDATE:
                updateAllProducts();
                break;
        }
    }

    private void updateTopProducts() {
        log.info("Top update was triggered.");

        topProducts = updateService.updateTopProducts();

        triggerUpdate(SubscriptionType.TOP);
    }

    private void updateSalesProducts() {
        log.info("Sales update was triggered.");

        salesProducts = updateService.updateSalesProducts();

        triggerUpdate(SubscriptionType.SALES);
    }

    private void updateAllProducts() {
        log.info("General update was triggered.");

        topProducts = updateService.updateTopProducts();
        salesProducts = updateService.updateSalesProducts();

        log.info("Triggering general client update.");
        productsContext.send("general");
    }

    private void triggerUpdate(SubscriptionType subscriptionType) {
        log.info("Triggering {} client update.", subscriptionType.getTrigger());

        productsContext.send(subscriptionType.getTrigger());
    }
}
