package com.tsystems.magazin.stand.model;

public enum ProductMQNotification {
    TOP_UPDATE,
    SALES_UPDATE,
    GENERAL_UPDATE
}
