package com.tsystems.magazin.stand.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.ejb.Singleton;

@Singleton
@Getter
public class MQConfig {
    private String exchangeName = "magazin-exchange";

    @AllArgsConstructor
    @Getter
    public enum Queue {
        TOP("top-queue", "top.*"),
        SALES("sales-queue", "sales.*"),
        GENERAL("general-queue", "general.*");

        private String name;
        private String key;
    }

    @AllArgsConstructor
    @Getter
    public class ConnectionDetails {
        public static final String HOST = "127.0.0.1";
        public static final int PORT = 5672;
        public static final String USER = "user";
        public static final String PASSWORD = "user";
    }
}
