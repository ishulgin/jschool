package com.tsystems.magazin.stand.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum SubscriptionType {
    TOP("Top", "top"),
    SALES("Sales", "sales");

    @Getter
    private String value;
    @Getter
    private String trigger;
}
