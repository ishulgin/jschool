package com.tsystems.magazin.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDTO {
    private int id;
    private String name;

    private int currentPrice;
    private int discount;
    private int price;
    private int weight;

    private String description;

    private int leftCount;

    private int categoryId;
    private int departmentId;

    private String previewImage;
    private List<String> productImages;

    private List<FilterValueDTO> filterValues;

    public static String getImgDirectoryName(int id, String name) {
        return id + "_" + name.replaceAll(" ", "_").toLowerCase() + "/";
    }

    public String getImgDirectoryName() {
        return getImgDirectoryName(id, name);
    }

    public String getShortDescription() {
        if (description.length() < 100) {
            return description;
        } else {
            int cutIndex = description.indexOf(' ', 100);
            return description.substring(0, cutIndex) + "...";
        }
    }

    public String getPriceString() {
        return currentPrice == price ?
                price + "$" :
                currentPrice + "$ <s>" + price + "$</s>";
    }
}
