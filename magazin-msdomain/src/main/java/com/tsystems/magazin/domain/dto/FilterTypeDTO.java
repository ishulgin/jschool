package com.tsystems.magazin.domain.dto;

import lombok.Data;

import java.util.List;

@Data
public class FilterTypeDTO {
    private int id;
    private String name;

    private List<FilterValueDTO> values;
}
