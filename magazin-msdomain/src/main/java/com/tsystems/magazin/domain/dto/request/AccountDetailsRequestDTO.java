package com.tsystems.magazin.domain.dto.request;

import com.tsystems.magazin.domain.dto.validation.DateInRange;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
public class AccountDetailsRequestDTO {
    @NotNull
    @Size(min = 1, max = 64, message = "{registration.name.size}")
    private String name;

    @NotNull
    @Size(min = 1, max = 64, message = "{registration.surname.size}")
    private String surname;

    @NotNull(message = "{registration.date.notNull}")
    @DateInRange(message = "{registration.date.dateInRange}")
    private LocalDate dateOfBirth;
}
