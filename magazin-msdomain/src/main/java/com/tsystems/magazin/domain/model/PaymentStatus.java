package com.tsystems.magazin.domain.model;

public enum PaymentStatus {
    PENDING, COMPLETED
}
