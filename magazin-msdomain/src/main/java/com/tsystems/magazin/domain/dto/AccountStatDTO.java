package com.tsystems.magazin.domain.dto;

import lombok.Data;

@Data
public class AccountStatDTO {
    int accountId;
    int profit;

    String name;
    String surname;
}
