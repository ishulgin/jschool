package com.tsystems.magazin.domain.model;

public enum OrderStatus {
    PROCESSING, PENDING_PAYMENT, PENDING_SHIPPING, SHIPPED, DELIVERED
}
