package com.tsystems.magazin.domain.dto.validation;

public interface ConfirmablePassword {
    String getPassword();

    String getPasswordConfirmation();
}
