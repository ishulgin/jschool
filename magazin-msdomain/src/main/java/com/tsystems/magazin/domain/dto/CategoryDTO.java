package com.tsystems.magazin.domain.dto;

import lombok.Data;

import java.util.List;

@Data
public class CategoryDTO {
    private int id;
    private String name;

    private List<CategoryDTO> children;
}
