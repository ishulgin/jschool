package com.tsystems.magazin.domain.dto.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserRequestDTO {
    @NotNull
    @Size(max = 64)
    @Email
    private String username;

    @NotNull
    @Size(min = 6, max = 32)
    private String password;
}
