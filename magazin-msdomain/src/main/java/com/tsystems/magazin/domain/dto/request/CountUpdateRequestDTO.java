package com.tsystems.magazin.domain.dto.request;

import lombok.Data;

import javax.validation.constraints.Positive;

@Data
public class CountUpdateRequestDTO {
    private int productId;

    @Positive
    private int count;
}
