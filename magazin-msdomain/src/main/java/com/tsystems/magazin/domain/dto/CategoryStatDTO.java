package com.tsystems.magazin.domain.dto;

import lombok.Data;

@Data
public class CategoryStatDTO {
    private int id;

    private int count;
    private int profit;

    private String categoryName;
}
