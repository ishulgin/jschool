package com.tsystems.magazin.domain.dto;

import lombok.Data;

@Data
public class StatUnit {
    private int id;
    private int count;

    private int profit;
}
