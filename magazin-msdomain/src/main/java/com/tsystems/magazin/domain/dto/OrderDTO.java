package com.tsystems.magazin.domain.dto;

import com.tsystems.magazin.domain.model.OrderStatus;
import com.tsystems.magazin.domain.model.PaymentMethod;
import com.tsystems.magazin.domain.model.PaymentStatus;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class OrderDTO {
    private int id;

    private PaymentMethod paymentMethod;
    private PaymentStatus paymentStatus;
    private OrderStatus orderStatus;

    private LocalDateTime orderedAt;

    private int addressId;
}
