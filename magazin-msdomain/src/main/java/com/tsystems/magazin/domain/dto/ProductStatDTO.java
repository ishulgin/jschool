package com.tsystems.magazin.domain.dto;

import lombok.Data;

@Data
public class ProductStatDTO {
    private int count;

    private int profit;
    private int productId;

    private String productName;
}
