package com.tsystems.magazin.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum PaymentMethod {
    CARD("Pay with a card"), CASH("Pay with cash");

    @Getter
    private String description;
}
