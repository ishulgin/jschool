package com.tsystems.magazin.domain.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class CartItemRequestDTO {
    @NotNull
    private int productId;

    @NotNull
    @Positive
    private int count;
}
