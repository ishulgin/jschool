package com.tsystems.magazin.domain.dto.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;

public class NotEmptyElementsValidator implements ConstraintValidator<NotNullElements, Collection<String>> {
    @Override
    public boolean isValid(Collection<String> value, ConstraintValidatorContext context) {
        return value == null || value.stream().allMatch(it -> it == null || !it.isEmpty());
    }
}
