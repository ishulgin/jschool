package com.tsystems.magazin.domain.dto;

import lombok.Data;

@Data
public class MonthStatDTO {
    private int month;
    private int year;

    private int profit;
}
