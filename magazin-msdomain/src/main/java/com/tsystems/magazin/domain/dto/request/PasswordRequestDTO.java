package com.tsystems.magazin.domain.dto.request;

import com.tsystems.magazin.domain.dto.validation.ConfirmablePassword;
import com.tsystems.magazin.domain.dto.validation.PasswordsEquality;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@PasswordsEquality(message = "{registration.passwordsEquality}")
public class PasswordRequestDTO implements ConfirmablePassword {
    @NotNull
    private String oldPassword;

    @NotNull
    @Size(min = 6, max = 32, message = "{registration.password.size}")
    private String password;

    @NotNull
    private String passwordConfirmation;
}
