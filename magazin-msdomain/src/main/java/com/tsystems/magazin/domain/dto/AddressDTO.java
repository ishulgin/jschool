package com.tsystems.magazin.domain.dto;

import lombok.Data;

@Data
public class AddressDTO {
    private int id;
    private String country;
    private String city;
    private String postalCode;
    private String street;
    private short house;
    private Short flat;

    private String name;
    private String surname;

    private int accountId;
}
