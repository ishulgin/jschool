package com.tsystems.magazin.domain.dto.request;

import com.tsystems.magazin.domain.dto.validation.NotEmptyElements;
import com.tsystems.magazin.domain.dto.validation.NotNullElements;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class FilterTypeRequestDTO {
    @NotNull
    @NotEmpty
    private String name;

    @NotNull
    @Size(min = 2)
    @NotNullElements
    @NotEmptyElements
    private List<String> values;
}
