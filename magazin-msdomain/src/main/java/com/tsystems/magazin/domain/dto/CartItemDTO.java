package com.tsystems.magazin.domain.dto;

import lombok.Data;

@Data
public class CartItemDTO {
    private int count;

    private ProductDTO product;

    public void increaseCount(int n) {
        count += n;
    }

    public int getProductId() {
        return product.getId();
    }
}
