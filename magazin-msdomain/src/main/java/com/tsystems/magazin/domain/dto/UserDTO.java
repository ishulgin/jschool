package com.tsystems.magazin.domain.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserDTO {
    private int id;
    private String username;
    private String password;
    private boolean enabled;
    private List<String> roles;

    private int accountId;
}
