package com.tsystems.magazin.domain.dto.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordsEqualityValidation implements ConstraintValidator<PasswordsEquality, ConfirmablePassword> {
    @Override
    public boolean isValid(ConfirmablePassword object, ConstraintValidatorContext context) {
        return object.getPassword().equals(object.getPasswordConfirmation());
    }
}
