package com.tsystems.magazin.domain.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Data
public class AddressRequestDTO {
    @NotNull
    @Size(min = 3, max = 64, message = "{address.country.size}")
    private String country;

    @NotNull
    @Size(min = 2, max = 128, message = "{address.city.size}")
    private String city;

    @NotNull
    @Size(min = 4, max = 12, message = "{address.postalCode.size}")
    private String postalCode;

    @NotNull
    @Size(min = 2, max = 64, message = "{address.street.size}")
    private String street;

    @NotNull
    @Positive(message = "{address.house.positive}")
    private Short house;

    @Positive(message = "{address.flat.positive}")
    private Short flat;
}
