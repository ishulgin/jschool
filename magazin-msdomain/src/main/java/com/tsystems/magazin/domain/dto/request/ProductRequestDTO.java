package com.tsystems.magazin.domain.dto.request;

import com.tsystems.magazin.domain.dto.validation.NotNullElements;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class ProductRequestDTO {
    @NotNull
    @Size(min = 6, max = 128, message = "{product.name.size}")
    private String name;

    @NotNull
    @Positive(message = "{product.price.positive}")
    private Integer price;

    @NotNull
    @Positive(message = "{product.weight.positive}")
    private Integer weight;

    private String description;

    @NotNull(message = "{product.categoryId.notNull}")
    @Positive(message = "{product.categoryId.positive}")
    private Integer categoryId;

    @NotNull
    @Positive(message = "{product.leftCount.positive}")
    private Integer leftCount;

    @NotNullElements(message = "{product.filtersIds.NotNullElements}")
    private List<Integer> filtersIds;
}
