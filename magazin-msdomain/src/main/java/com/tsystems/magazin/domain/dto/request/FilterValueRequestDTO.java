package com.tsystems.magazin.domain.dto.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class FilterValueRequestDTO {
    @NotNull
    @NotEmpty
    private String value;
}
