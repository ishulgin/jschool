package com.tsystems.magazin.domain.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class AccountDTO {
    private int id;
    private String name;
    private String surname;
    private LocalDate dateOfBirth;

    private String username;

    private List<AddressDTO> addresses;
}
