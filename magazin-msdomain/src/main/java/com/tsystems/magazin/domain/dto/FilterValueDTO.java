package com.tsystems.magazin.domain.dto;

import lombok.Data;

@Data
public class FilterValueDTO {
    private int id;
    private String value;

    private int filterTypeId;
    private String filterTypeName;
}
