package com.tsystems.magazin.domain.dto.request;

import com.tsystems.magazin.domain.model.PaymentMethod;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class OrderRequestDTO {
    private int accountId;

    private int addressId;

    @NotNull
    private PaymentMethod paymentMethod;
}
