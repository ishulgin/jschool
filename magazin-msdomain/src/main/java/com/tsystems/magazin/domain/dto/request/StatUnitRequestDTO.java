package com.tsystems.magazin.domain.dto.request;

import lombok.Data;

import javax.validation.constraints.Positive;

@Data
public class StatUnitRequestDTO {
    int departmentId;
    int categoryId;
    int productId;

    @Positive
    int profit;
    @Positive
    int count;
}
