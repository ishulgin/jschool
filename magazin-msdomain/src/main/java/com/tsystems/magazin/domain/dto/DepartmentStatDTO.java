package com.tsystems.magazin.domain.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Data
public class DepartmentStatDTO {
    private String departmentName;

    private Map<LocalDate, StatUnit> stats = new HashMap<>();

    public void addStat(LocalDate date, StatUnit statUnit) {
        stats.put(date, statUnit);
    }
}
