package com.tsystems.magazin.domain.dto.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;
import java.util.Objects;

public class NotNullElementsValidator implements ConstraintValidator<NotNullElements, Collection<?>> {
    @Override
    public boolean isValid(Collection<?> value, ConstraintValidatorContext context) {
        return value == null || value.stream().noneMatch(Objects::isNull);
    }
}
