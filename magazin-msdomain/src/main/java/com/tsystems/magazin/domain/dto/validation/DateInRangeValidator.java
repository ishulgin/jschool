package com.tsystems.magazin.domain.dto.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class DateInRangeValidator implements ConstraintValidator<DateInRange, LocalDate> {
    private DateInRange constraintAnnotation;

    @Override
    public void initialize(DateInRange constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(LocalDate date, ConstraintValidatorContext context) {
        LocalDate min = LocalDate.parse(constraintAnnotation.min());
        LocalDate max = LocalDate.now();

        return date == null || (date.isAfter(min) && date.isBefore(max));
    }
}