package com.tsystems.magazin.domain.dto.request;

import com.tsystems.magazin.domain.dto.validation.ConfirmablePassword;
import com.tsystems.magazin.domain.dto.validation.DateInRange;
import com.tsystems.magazin.domain.dto.validation.PasswordsEquality;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
@PasswordsEquality
public class AccountRequestDTO implements ConfirmablePassword {
    @NotNull
    @Size(min = 1, max = 64, message = "{registration.name.size}")
    private String name;

    @NotNull
    @Size(min = 1, max = 64, message = "{registration.surname.size}")
    private String surname;

    @NotNull(message = "{registration.date.notNull}")
    @DateInRange(message = "{registration.date.dateInRange}")
    private LocalDate dateOfBirth;

    @NotNull
    @Email(message = "{registration.email.email}")
    private String username;

    @NotNull
    @Size(min = 6, max = 32, message = "{registration.password.size}")
    private String password;

    @NotNull
    @Size(min = 6, max = 32)
    private String passwordConfirmation;

    public UserRequestDTO getUserRequestDTO() {
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        userRequestDTO.setUsername(username);
        userRequestDTO.setPassword(password);

        return userRequestDTO;
    }
}
