package com.tsystems.magazin.domain.dto.request;

import com.tsystems.magazin.domain.dto.validation.NotNullElements;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class CategoryRequestDTO {
    private Integer parentId;

    @NotNull
    @Size(min = 4, max = 64)
    private String name;

    @NotNull
    @NotNullElements
    private List<Integer> filterTypesIds;
}
