package com.tsystems.magazin.domain.dto.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = DateInRangeValidator.class)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DateInRange {
    String message() default "Date should be in range";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String min() default "1900-01-01";
}
