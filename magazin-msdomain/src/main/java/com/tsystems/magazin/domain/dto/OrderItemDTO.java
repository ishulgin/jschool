package com.tsystems.magazin.domain.dto;

import lombok.Data;

@Data
public class OrderItemDTO {
    private int count;
    private int actualPrice;

    private ProductDTO product;
}
