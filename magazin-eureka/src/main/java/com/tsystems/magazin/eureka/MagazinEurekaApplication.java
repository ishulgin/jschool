package com.tsystems.magazin.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class MagazinEurekaApplication {
    public static void main(String[] args) {
        SpringApplication.run(MagazinEurekaApplication.class, args);
    }
}
