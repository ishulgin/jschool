CREATE TABLE Cart (
  id        INT AUTO_INCREMENT,
  accountId INT,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;

CREATE TABLE CartItem (
  id        INT AUTO_INCREMENT,
  count     INT NOT NULL,
  cartId    INT,
  productId INT,
  PRIMARY KEY (id),
  FOREIGN KEY (cartId) REFERENCES Cart (id)
)
  ENGINE = InnoDB;

CREATE TABLE `Order` (
  id            INT AUTO_INCREMENT,
  paymentMethod ENUM ('CARD', 'CASH')         NOT NULL,
  paymentStatus ENUM ('PENDING', 'COMPLETED') NOT NULL,
  orderStatus   ENUM ('PROCESSING', 'PENDING_PAYMENT',
                      'PENDING_SHIPPING', 'SHIPPED',
                      'DELIVERED')            NOT NULL,
  orderedAt     DATETIME                      NOT NULL,
  accountId     INT,
  addressId     INT,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;

CREATE TABLE OrderItem (
  id        INT AUTO_INCREMENT,
  count     INT NOT NULL,
  price     INT NOT NULL,
  orderId   INT,
  productId INT,
  PRIMARY KEY (id),
  FOREIGN KEY (orderId) REFERENCES `Order` (id)
)
  ENGINE = InnoDB;