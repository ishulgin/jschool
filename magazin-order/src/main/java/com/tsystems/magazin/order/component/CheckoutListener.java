package com.tsystems.magazin.order.component;

import com.tsystems.magazin.order.client.MagazinStatClient;
import com.tsystems.magazin.order.client.MagazinUpdateClient;
import com.tsystems.magazin.order.event.CheckoutEvent;
import com.tsystems.magazin.order.util.ModelMapperWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
@Slf4j
public class CheckoutListener {
    private AmqpTemplate rabbitTemplate;

    private MagazinUpdateClient magazinUpdateClient;

    private MagazinStatClient magazinStatClient;

    private ModelMapperWrapper modelMapperWrapper;

    @Autowired
    public void setMagazinUpdateClient(MagazinUpdateClient magazinUpdateClient) {
        this.magazinUpdateClient = magazinUpdateClient;
    }

    @Autowired
    public void setMagazinStatClient(MagazinStatClient magazinStatClient) {
        this.magazinStatClient = magazinStatClient;
    }

    @Autowired
    public void setRabbitTemplate(AmqpTemplate amqpTemplate) {
        this.rabbitTemplate = amqpTemplate;
    }

    @Autowired
    public void setModelMapperWrapper(ModelMapperWrapper modelMapperWrapper) {
        this.modelMapperWrapper = modelMapperWrapper;
    }

    @Async
    @TransactionalEventListener
    public void onCheckout(CheckoutEvent checkoutEvent) {
        rabbitTemplate.convertAndSend("delivery", "delivery-to", modelMapperWrapper.mapCheckoutEvent(checkoutEvent));

        try {
            magazinStatClient.updateStats(checkoutEvent.getAccountId(), checkoutEvent.getStats());
        } catch (Exception e) {
            log.error("Statistics update can't be initialized: {}.", e.getMessage());
        }

        try {
            magazinUpdateClient.initTopUpdate();
        } catch (Exception e) {
            log.error("Top update can't be initialized: {}.", e.getMessage());
        }
    }
}
