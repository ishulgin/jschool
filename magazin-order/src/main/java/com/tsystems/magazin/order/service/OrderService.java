package com.tsystems.magazin.order.service;

import com.tsystems.magazin.domain.dto.OrderDTO;
import com.tsystems.magazin.domain.dto.OrderItemDTO;
import com.tsystems.magazin.domain.dto.request.OrderRequestDTO;
import com.tsystems.magazin.domain.model.OrderStatus;
import com.tsystems.magazin.domain.model.PaymentStatus;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface OrderService {
    @NotNull List<OrderDTO> getOrders(int accountId);

    @NotNull List<OrderDTO> getAllOrders();

    List<OrderItemDTO> getOrderItems(int accountId, int orderId);

    List<OrderItemDTO> getOrderItems(int orderId);

    boolean checkout(OrderRequestDTO orderRequestDTO);

    void updatePaymentStatus(int orderId, PaymentStatus paymentStatus);

    void updateOrderStatus(int orderId, OrderStatus orderStatus);
}
