package com.tsystems.magazin.order.config;

import com.tsystems.magazin.order.component.DeliveryMQListener;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MQConfig {
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost("206.189.24.66");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("ilya");
        connectionFactory.setPassword("ilya");

        return connectionFactory;
    }

    @Bean
    public DirectExchange deliveryExchange() {
        return new DirectExchange("delivery");
    }

    @Bean
    public Queue deliveryFromQueue() {
        return new Queue("delivery-from");
    }

    @Bean
    public Queue deliveryToQueue() {
        return new Queue("delivery-to");
    }

    @Bean
    public Binding bindingDeliveryTo(DirectExchange deliveryExchange, Queue deliveryToQueue) {
        return BindingBuilder.bind(deliveryToQueue)
                .to(deliveryExchange)
                .with("delivery-to");
    }

    @Bean
    public Binding bindingDeliveryFrom(DirectExchange deliveryExchange, Queue deliveryFromQueue) {
        return BindingBuilder.bind(deliveryFromQueue)
                .to(deliveryExchange)
                .with("delivery-from");
    }

    @Bean
    public MessageListenerAdapter listenerAdapter(DeliveryMQListener deliveryMQListener) {
        return new MessageListenerAdapter(deliveryMQListener, "updateDeliveryInfo");
    }

    @Bean
    public SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                                    MessageListenerAdapter listenerAdapter,
                                                    Queue deliveryFromQueue) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(deliveryFromQueue.getName());
        container.setMessageListener(listenerAdapter);

        return container;
    }
}
