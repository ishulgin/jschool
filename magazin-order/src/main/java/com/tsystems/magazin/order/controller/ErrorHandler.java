package com.tsystems.magazin.order.controller;

import com.netflix.client.ClientException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

@ControllerAdvice
@Slf4j
public class ErrorHandler {
    @ExceptionHandler({SocketTimeoutException.class})
    public ResponseEntity processTimeOut(SocketTimeoutException e) {
        log.error("Timeout exception was thrown: {}.", e.getMessage());

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ClientException.class})
    public ResponseEntity processClientException(ClientException e) {
        log.error("Client exception was thrown: {}.", e.getMessage());

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ConnectException.class})
    public ResponseEntity connectionRefusedException(ConnectException e) {
        log.error("Connection refused: {}.", e.getMessage());

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
