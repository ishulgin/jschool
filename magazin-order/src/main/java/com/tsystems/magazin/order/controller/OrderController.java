package com.tsystems.magazin.order.controller;

import com.tsystems.magazin.domain.dto.OrderDTO;
import com.tsystems.magazin.domain.dto.OrderItemDTO;
import com.tsystems.magazin.domain.dto.request.OrderRequestDTO;
import com.tsystems.magazin.domain.model.OrderStatus;
import com.tsystems.magazin.domain.model.PaymentStatus;
import com.tsystems.magazin.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {
    private OrderService orderService;

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/account/{id}/orders")
    public ResponseEntity<List<OrderDTO>> getAccountOrders(@PathVariable("id") int accountId) {
        return ResponseEntity.ok(orderService.getOrders(accountId));
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/orders")
    public ResponseEntity<List<OrderDTO>> getAllOrders() {
        return ResponseEntity.ok(orderService.getAllOrders());
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/orders")
    public ResponseEntity<Boolean> checkout(@RequestBody @Valid OrderRequestDTO orderRequestDTO) {
        try {
            return ResponseEntity.ok(orderService.checkout(orderRequestDTO));
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/orders/{id}/paymentStatus")
    public ResponseEntity updatePaymentStatus(@PathVariable int id, @RequestBody PaymentStatus paymentStatus) {
        try {
            orderService.updatePaymentStatus(id, paymentStatus);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/orders/{id}/orderStatus")
    public ResponseEntity updateOrderStatus(@PathVariable int id, @RequestBody OrderStatus orderStatus) {
        try {
            orderService.updateOrderStatus(id, orderStatus);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/account/{accountId}/orders/{id}")
    public ResponseEntity<List<OrderItemDTO>> getOrderItems(@PathVariable("id") int orderId, @PathVariable("accountId") int accountId) {
        return ResponseEntity.ok(orderService.getOrderItems(accountId, orderId));
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/orders/{id}")
    public ResponseEntity<List<OrderItemDTO>> getOrderItems(@PathVariable("id") int orderId) {
        return ResponseEntity.ok(orderService.getOrderItems(orderId));
    }
}
