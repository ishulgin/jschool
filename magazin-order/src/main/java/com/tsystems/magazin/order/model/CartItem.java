package com.tsystems.magazin.order.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class CartItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private int count;

    @Column
    private int productId;

    public void increaseCount(int n) {
        count += n;
    }
}
