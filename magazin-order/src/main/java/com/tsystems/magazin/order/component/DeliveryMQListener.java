package com.tsystems.magazin.order.component;

import com.tsystems.magazin.domain.model.OrderStatus;
import com.tsystems.fury.model.GoodsInfo;
import com.tsystems.magazin.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DeliveryMQListener {
    private OrderService orderService;

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public void updateDeliveryInfo(GoodsInfo goodsInfo) {
        switch (goodsInfo.getStatus()) {
            case REJECTED:
                log.info("Order {}: Unable to deliver by Valentin.", goodsInfo.getOrderId());
                break;
            case OK:
                log.info("Order {}: Delivery was registered in system.", goodsInfo.getOrderId());
                orderService.updateOrderStatus(goodsInfo.getOrderId(), OrderStatus.PENDING_SHIPPING);
                break;
            case SHIPPED:
                orderService.updateOrderStatus(goodsInfo.getOrderId(), OrderStatus.SHIPPED);
                break;
            case DELIVERED:
                orderService.updateOrderStatus(goodsInfo.getOrderId(), OrderStatus.DELIVERED);
                break;
        }
    }
}
