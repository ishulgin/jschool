package com.tsystems.magazin.order.dao;

import com.tsystems.magazin.order.model.Cart;

public interface CartDAO {
    Cart getCartByAccountId(int accountId);
}
