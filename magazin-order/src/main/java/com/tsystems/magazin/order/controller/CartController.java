package com.tsystems.magazin.order.controller;

import com.tsystems.magazin.domain.dto.CartItemDTO;
import com.tsystems.magazin.domain.dto.request.CartItemRequestDTO;
import com.tsystems.magazin.order.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CartController {
    private CartService cartService;

    @Autowired
    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/carts/{id}/")
    public ResponseEntity<List<CartItemDTO>> getCartItems(@PathVariable("id") int accountId) {
        return ResponseEntity.ok(cartService.getCartItems(accountId));
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/carts/{id}/")
    public ResponseEntity addCartItem(@PathVariable("id") int accountId,
                                      @RequestBody @Valid CartItemRequestDTO cartItemRequestDTO) {
        try {
            cartService.addCartItem(accountId, cartItemRequestDTO);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @DeleteMapping("/carts/{id}/")
    public ResponseEntity removeCartItem(@PathVariable("id") int accountId,
                                         @RequestBody int productId) {
        cartService.removeCartItem(accountId, productId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/carts/{id}/merge")
    public ResponseEntity mergeCarts(@PathVariable("id") int accountId,
                                     @RequestBody List<@Valid CartItemRequestDTO> cartItemRequestDTO) {
        cartService.mergeCartItems(accountId, cartItemRequestDTO);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
