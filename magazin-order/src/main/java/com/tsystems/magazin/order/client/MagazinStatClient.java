package com.tsystems.magazin.order.client;

import com.tsystems.magazin.domain.dto.request.StatUnitRequestDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

@FeignClient("magazin-stat")
public interface MagazinStatClient {
    @PostMapping("/stats/{id}")
    void updateStats(@PathVariable("id") int accountId, @RequestBody List<@Valid StatUnitRequestDTO> statUnitRequestDTOs);
}
