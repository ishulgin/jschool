package com.tsystems.magazin.order.service;

import com.tsystems.magazin.domain.dto.AddressDTO;
import com.tsystems.magazin.domain.dto.OrderDTO;
import com.tsystems.magazin.domain.dto.OrderItemDTO;
import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.CountUpdateRequestDTO;
import com.tsystems.magazin.domain.dto.request.OrderRequestDTO;
import com.tsystems.magazin.domain.dto.request.StatUnitRequestDTO;
import com.tsystems.magazin.domain.model.OrderStatus;
import com.tsystems.magazin.domain.model.PaymentStatus;
import com.tsystems.magazin.order.client.MagazinAccountClient;
import com.tsystems.magazin.order.client.MagazinProductClient;
import com.tsystems.magazin.order.client.MagazinStatClient;
import com.tsystems.magazin.order.dao.CartDAO;
import com.tsystems.magazin.order.dao.OrderDAO;
import com.tsystems.magazin.order.event.CheckoutEvent;
import com.tsystems.magazin.order.model.Cart;
import com.tsystems.magazin.order.model.CartItem;
import com.tsystems.magazin.order.model.Order;
import com.tsystems.magazin.order.model.OrderItem;
import com.tsystems.magazin.order.util.ModelMapperWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {
    private OrderDAO orderDAO;

    private CartDAO cartDAO;

    private MagazinProductClient magazinProductClient;

    private MagazinAccountClient magazinAccountClient;

    private ModelMapperWrapper modelMapperWrapper;

    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Autowired
    public void setCartDAO(CartDAO cartDAO) {
        this.cartDAO = cartDAO;
    }

    @Autowired
    public void setModelMapperWrapper(ModelMapperWrapper modelMapperWrapper) {
        this.modelMapperWrapper = modelMapperWrapper;
    }

    @Autowired
    public void setMagazinProductClient(MagazinProductClient magazinProductClient) {
        this.magazinProductClient = magazinProductClient;
    }

    @Autowired
    public void setMagazinAccountClient(MagazinAccountClient magazinAccountClient) {
        this.magazinAccountClient = magazinAccountClient;
    }

    @Autowired
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @NotNull
    @Transactional
    @Override
    public List<OrderDTO> getOrders(int accountId) {
        return orderDAO.getOrdersByAccountId(accountId)
                .stream()
                .map(modelMapperWrapper::mapOrder)
                .collect(Collectors.toList());
    }

    @NotNull
    @Transactional
    @Override
    public List<OrderDTO> getAllOrders() {
        return orderDAO.getAllOrders()
                .stream()
                .map(modelMapperWrapper::mapOrder)
                .collect(Collectors.toList());
    }

    private static OrderItem mapCartItemToOrderItem(@NotNull CartItem cartItem) {
        OrderItem orderItem = new OrderItem();
        orderItem.setCount(cartItem.getCount());
        orderItem.setProductId(cartItem.getProductId());

        return orderItem;
    }

    private static CountUpdateRequestDTO mapCartItemToCountUpdateRequestDTO(@NotNull CartItem cartItem) {
        CountUpdateRequestDTO countUpdateRequestDTO = new CountUpdateRequestDTO();
        countUpdateRequestDTO.setProductId(cartItem.getProductId());
        countUpdateRequestDTO.setCount(cartItem.getCount());

        return countUpdateRequestDTO;
    }

    private static StatUnitRequestDTO getStatUnitRequestDTO(@NotNull ProductDTO product, int count, int accountId) {
        StatUnitRequestDTO statUnitRequestDTO = new StatUnitRequestDTO();
        statUnitRequestDTO.setProductId(product.getId());
        statUnitRequestDTO.setCategoryId(product.getCategoryId());
        statUnitRequestDTO.setDepartmentId(product.getDepartmentId());
        statUnitRequestDTO.setCount(count);
        statUnitRequestDTO.setProfit(count * product.getCurrentPrice());

        return statUnitRequestDTO;
    }

    @Transactional
    @Override
    public List<OrderItemDTO> getOrderItems(int accountId, int orderId) {
        Order order = orderDAO.getOrderById(orderId);

        if (order == null) {
            log.debug("Order {}: Not found.", orderId);

            return null;
        } else if (order.getAccountId() != accountId) {
            log.debug("Account id {}: Order {} doesn't belong to account.", accountId, orderId);

            return null;
        }

        return getOrderItems(order);
    }

    @Transactional
    @Override
    public List<OrderItemDTO> getOrderItems(int orderId) {
        Order order = orderDAO.getOrderById(orderId);

        if (order == null) {
            log.debug("Order {}: Not found.", orderId);

            return null;
        }

        return getOrderItems(order);
    }

    public List<OrderItemDTO> getOrderItems(Order order) {
        List<OrderItemDTO> result = new ArrayList<>();

        for (OrderItem orderItem : order.getOrderItems()) {
            OrderItemDTO orderItemDTO = modelMapperWrapper.mapOrderItem(orderItem);
            ProductDTO productDTO = magazinProductClient.getProductInfo(orderItem.getProductId());

            if (productDTO == null) {
                log.warn("Order item {}: Product {} doesn't exist.", orderItem.getId(), orderItem.getProductId());

                return null;
            }

            orderItemDTO.setProduct(productDTO);

            result.add(orderItemDTO);
        }

        return result;
    }

    @Transactional
    @Override
    public boolean checkout(OrderRequestDTO orderRequestDTO) {
        AddressDTO address = magazinAccountClient.getAddress(orderRequestDTO.getAddressId());

        if (address == null) {
            log.warn("Account id {}: Address {} not found.", orderRequestDTO.getAccountId(), orderRequestDTO.getAddressId());

            throw new IllegalArgumentException();
        }

        if (address.getAccountId() != orderRequestDTO.getAccountId()) {
            log.warn("Address owner {} and client {} are different.", address.getAccountId(), orderRequestDTO.getAccountId());

            throw new IllegalArgumentException();
        }

        Cart cart = cartDAO.getCartByAccountId(orderRequestDTO.getAccountId());

        if (cart.isEmpty()) {
            log.debug("Cart {}: Empty.", cart.getId());

            throw new IllegalArgumentException();
        }

        List<CountUpdateRequestDTO> countUpdateRequestDTOs = new ArrayList<>();
        List<StatUnitRequestDTO> statUnitRequestDTOs = new ArrayList<>();
        Set<OrderItem> orderItems = new HashSet<>();
        int totalWeight = 0;

        for (CartItem cartItem : cart.getCartItems()) {
            ProductDTO product = magazinProductClient.getProductInfo(cartItem.getProductId());

            if (product == null) {
                log.warn("Cart item {}: Product {} not found.", cartItem.getId(), cartItem.getProductId());

                throw new IllegalArgumentException();
            }

            if (product.getLeftCount() < cartItem.getCount()) {
                log.debug("Cart item {}: Can't order more items of {} than left in stock ({} > {}).",
                        cartItem.getId(), product.getId(), cartItem.getCount(), product.getLeftCount());

                return false;
            }

            totalWeight += product.getWeight() * cartItem.getCount();

            OrderItem orderItem = mapCartItemToOrderItem(cartItem);
            orderItem.setPrice(product.getCurrentPrice());

            orderItems.add(orderItem);

            countUpdateRequestDTOs.add(mapCartItemToCountUpdateRequestDTO(cartItem));
            statUnitRequestDTOs.add(getStatUnitRequestDTO(product, cartItem.getCount(), orderRequestDTO.getAccountId()));
        }

        try {
            magazinProductClient.updateProductsCount(countUpdateRequestDTOs);
        } catch (IllegalArgumentException e) {
            log.debug("Cart {}: Can't update products' count.", cart.getId());

            return false;
        }

        Order order = modelMapperWrapper.mapOrderRequestDTO(orderRequestDTO);
        order.setOrderStatus(OrderStatus.PROCESSING);
        order.setPaymentStatus(PaymentStatus.PENDING);
        order.setOrderItems(orderItems);

        int orderId = orderDAO.saveOrder(order);

        log.info("Account id {}: New order was created, paying by {}.", order.getAccountId(), order.getPaymentMethod());

        cart.clearCart();

        log.debug("Account id {}: Cart was cleared.", orderRequestDTO.getAccountId());

        CheckoutEvent checkoutEvent = new CheckoutEvent();
        checkoutEvent.setAccountId(orderRequestDTO.getAccountId());
        checkoutEvent.setOrderId(orderId);
        checkoutEvent.setWeight(totalWeight);
        checkoutEvent.setCityTo(address.getCity());
        checkoutEvent.setStats(statUnitRequestDTOs);
        applicationEventPublisher.publishEvent(checkoutEvent);

        return true;
    }


    @Transactional
    @Override
    public void updatePaymentStatus(int orderId, PaymentStatus paymentStatus) {
        Order order = orderDAO.getOrderById(orderId);

        if (order == null) {
            log.debug("Order {}: Order -> found.", orderId);

            throw new IllegalArgumentException();
        }

        log.info("Order {}: Payment status {} -> {}.", orderId, order.getPaymentStatus(), paymentStatus);

        order.setPaymentStatus(paymentStatus);
    }

    @Transactional
    @Override
    public void updateOrderStatus(int orderId, OrderStatus orderStatus) {
        Order order = orderDAO.getOrderById(orderId);

        if (order == null) {
            log.debug("Order {}: Not found.", orderId);

            throw new IllegalArgumentException();
        }

        log.info("Order {}: Order status {} -> {}.", orderId, order.getOrderStatus(), orderStatus);

        order.setOrderStatus(orderStatus);
    }
}
