package com.tsystems.magazin.order.dao;

import com.tsystems.magazin.order.model.Order;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public class OrderDAOImpl implements OrderDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Order getOrderById(int id) {
        return entityManager.find(Order.class, id);
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<Order> getAllOrders() {
        Query query = entityManager.createQuery("select o from Order o order by o.id desc");

        return (List<Order>) query.getResultList();
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<Order> getOrdersByAccountId(int accountId) {
        Query query = entityManager.createQuery("select o from Order o where o.accountId=:accountId order by o.id desc")
                .setParameter("accountId", accountId);

        return (List<Order>) query.getResultList();
    }

    @Override
    public int saveOrder(@NotNull Order order) {
        entityManager.persist(order);
        entityManager.flush();

        return order.getId();
    }
}
