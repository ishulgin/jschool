package com.tsystems.magazin.order.dao;

import com.tsystems.magazin.order.model.Cart;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
public class CartDAOImpl implements CartDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Cart getCartByAccountId(int accountId) {
        try {
            Query query = entityManager.createQuery("select c from Cart c where c.accountId=:accountId")
                    .setParameter("accountId", accountId);

            return (Cart) query.getSingleResult();
        } catch (NoResultException e) {
            Cart cart = new Cart();
            cart.setAccountId(accountId);

            entityManager.persist(cart);
            entityManager.flush();

            return cart;
        }
    }
}
