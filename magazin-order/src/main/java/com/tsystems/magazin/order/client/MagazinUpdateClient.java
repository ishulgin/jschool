package com.tsystems.magazin.order.client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient("magazin-update")
public interface MagazinUpdateClient {
    @PostMapping("/products/top")
    void initTopUpdate();
}
