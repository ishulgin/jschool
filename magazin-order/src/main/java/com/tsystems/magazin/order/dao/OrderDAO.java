package com.tsystems.magazin.order.dao;

import com.tsystems.magazin.order.model.Order;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface OrderDAO {
    Order getOrderById(int id);

    @NotNull List<Order> getAllOrders();

    @NotNull List<Order> getOrdersByAccountId(int accountId);

    int saveOrder(@NotNull Order order);
}
