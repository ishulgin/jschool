package com.tsystems.magazin.order.client;

import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.CountUpdateRequestDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("magazin-product")
public interface MagazinProductClient {
    @GetMapping("/products/{id}")
    ProductDTO getProductInfo(@PathVariable("id") int productId);

    @PostMapping("/products/count")
    void updateProductsCount(@RequestBody List<CountUpdateRequestDTO> countUpdateRequestDTO);
}
