package com.tsystems.magazin.order.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private int accountId;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cartId", referencedColumnName = "id")
    private Set<CartItem> cartItems;

    public void addCartItem(CartItem cartItem) {
        getCartItems().add(cartItem);
    }

    public boolean isEmpty() {
        return getCartItems().isEmpty();
    }

    public void clearCart() {
        getCartItems().clear();
    }

    public CartItem getCartItemByProductId(int productId) {
        return getCartItems().stream()
                .filter(it -> productId == it.getProductId())
                .findFirst()
                .orElse(null);
    }
}
