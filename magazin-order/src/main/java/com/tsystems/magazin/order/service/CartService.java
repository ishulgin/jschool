package com.tsystems.magazin.order.service;

import com.tsystems.magazin.domain.dto.CartItemDTO;
import com.tsystems.magazin.domain.dto.request.CartItemRequestDTO;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface CartService {
    void addCartItem(int userId, @NotNull CartItemRequestDTO cartItemRequestDTO);

    void mergeCartItems(int userId, @NotNull List<CartItemRequestDTO> sessionCart);

    int getProductInCartCount(int userId, int productId);

    List<CartItemDTO> getCartItems(int userId);

    void removeCartItem(int userId, int productId);
}
