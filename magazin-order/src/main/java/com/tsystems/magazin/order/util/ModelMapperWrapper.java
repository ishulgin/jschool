package com.tsystems.magazin.order.util;

import com.tsystems.magazin.domain.dto.CartItemDTO;
import com.tsystems.magazin.domain.dto.OrderDTO;
import com.tsystems.magazin.domain.dto.OrderItemDTO;
import com.tsystems.magazin.domain.dto.request.OrderRequestDTO;
import com.tsystems.magazin.order.event.CheckoutEvent;
import com.tsystems.magazin.order.model.CartItem;
import com.tsystems.magazin.order.model.Order;
import com.tsystems.magazin.order.model.OrderItem;
import com.tsystems.fury.model.Goods;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class ModelMapperWrapper {
    private ModelMapper modelMapper = new ModelMapper();

    public ModelMapperWrapper() {
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.LOOSE);

        modelMapper.getConfiguration().setAmbiguityIgnored(true);

        modelMapper.createTypeMap(CartItem.class, CartItemDTO.class)
                .addMappings(mapping -> mapping.skip(CartItemDTO::setProduct));


        modelMapper.createTypeMap(OrderItem.class, OrderItemDTO.class)
                .addMappings(mapping -> mapping.skip(OrderItemDTO::setProduct));
    }

    @NotNull
    public CartItemDTO mapCartItem(@NotNull CartItem cartItem) {
        return modelMapper.map(cartItem, CartItemDTO.class);
    }

    @NotNull
    public OrderItemDTO mapOrderItem(@NotNull OrderItem orderItem) {
        return modelMapper.map(orderItem, OrderItemDTO.class);
    }

    @NotNull
    public OrderDTO mapOrder(@NotNull Order order) {
        return modelMapper.map(order, OrderDTO.class);
    }

    @NotNull
    public Order mapOrderRequestDTO(@NotNull OrderRequestDTO orderRequestDTO) {
        return modelMapper.map(orderRequestDTO, Order.class);
    }

    @NotNull
    public Goods mapCheckoutEvent(@NotNull CheckoutEvent checkoutEvent) {
        return modelMapper.map(checkoutEvent, Goods.class);
    }
}
