package com.tsystems.magazin.order.event;

import com.tsystems.magazin.domain.dto.request.StatUnitRequestDTO;
import lombok.Data;

import java.util.List;

@Data
public class CheckoutEvent {
    private int accountId;
    private int orderId;
    private String cityTo;
    private int weight;

    List<StatUnitRequestDTO> stats;
}
