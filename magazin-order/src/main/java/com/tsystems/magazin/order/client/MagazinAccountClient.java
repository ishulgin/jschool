package com.tsystems.magazin.order.client;

import com.tsystems.magazin.domain.dto.AddressDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("magazin-account")
public interface MagazinAccountClient {
    @GetMapping("/addresses/{id}")
    AddressDTO getAddress(@PathVariable("id") int addressId);
}
