package com.tsystems.magazin.order.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private int count;

    @Column(nullable = false)
    private int price;

    @Column
    private int productId;
}
