package com.tsystems.magazin.order.model;


import com.tsystems.magazin.domain.model.OrderStatus;
import com.tsystems.magazin.domain.model.PaymentMethod;
import com.tsystems.magazin.domain.model.PaymentStatus;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "`Order`")
@Data
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PaymentMethod paymentMethod;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PaymentStatus paymentStatus;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @Column(nullable = false)
    @CreationTimestamp
    private LocalDateTime orderedAt;

    @Column
    private int addressId;

    @Column
    private int accountId;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "orderId", referencedColumnName = "id")
    private Set<OrderItem> orderItems;
}
