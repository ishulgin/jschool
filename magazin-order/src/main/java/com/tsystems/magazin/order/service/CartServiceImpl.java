package com.tsystems.magazin.order.service;

import com.tsystems.magazin.domain.dto.CartItemDTO;
import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.CartItemRequestDTO;
import com.tsystems.magazin.order.client.MagazinProductClient;
import com.tsystems.magazin.order.dao.CartDAO;
import com.tsystems.magazin.order.model.Cart;
import com.tsystems.magazin.order.model.CartItem;
import com.tsystems.magazin.order.util.ModelMapperWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
@Slf4j
public class CartServiceImpl implements CartService {
    private CartDAO cartDAO;

    private MagazinProductClient magazinProductClient;

    private ModelMapperWrapper modelMapperWrapper;

    @Autowired
    public void setCartDAO(CartDAO cartDAO) {
        this.cartDAO = cartDAO;
    }

    @Autowired
    public void setMagazinProductClient(MagazinProductClient magazinProductClient) {
        this.magazinProductClient = magazinProductClient;
    }

    @Autowired
    public void setModelMapperWrapper(ModelMapperWrapper modelMapperWrapper) {
        this.modelMapperWrapper = modelMapperWrapper;
    }

    @Transactional
    @Override
    public void addCartItem(int accountId, @NotNull CartItemRequestDTO cartItemRequestDTO) {
        getProductById(cartItemRequestDTO.getProductId());

        updateCartItem(cartDAO.getCartByAccountId(accountId), cartItemRequestDTO);
    }

    @Transactional
    @Override
    public void mergeCartItems(int accountId, @NotNull List<CartItemRequestDTO> cartItemRequestDTOs) {
        Cart cart = cartDAO.getCartByAccountId(accountId);

        for (CartItemRequestDTO cartItemRequestDTO : cartItemRequestDTOs) {
            getProductById(cartItemRequestDTO.getProductId());

            updateCartItem(cart, cartItemRequestDTO);
        }

        log.info("Cart {}: Merged with the session cart.", cart.getId());
    }

    private void getProductById(int productId) {
        if (magazinProductClient.getProductInfo(productId) == null) {
            log.debug("Product id {}: Not found.", productId);

            throw new IllegalArgumentException();
        }
    }

    private void updateCartItem(@NotNull Cart cart, @NotNull CartItemRequestDTO cartItemRequestDTO) {
        CartItem cartItem = cart.getCartItemByProductId(cartItemRequestDTO.getProductId());

        if (cartItem == null) {
            cartItem = new CartItem();
            cartItem.setCount(cartItemRequestDTO.getCount());
            cartItem.setProductId(cartItemRequestDTO.getProductId());

            cart.addCartItem(cartItem);

            log.info("Cart {}: +{} of {}.", cart.getId(), cartItemRequestDTO.getCount(), cartItemRequestDTO.getProductId());
        } else {
            cartItem.increaseCount(cartItemRequestDTO.getCount());

            log.info("Cart {}: +{} ({}) of {}.", cart.getId(), cartItemRequestDTO.getCount(),
                    cartItem.getCount(), cartItemRequestDTO.getProductId());
        }
    }

    @Transactional
    @Override
    public int getProductInCartCount(int accountId, int productId) {
        CartItem cartItem = cartDAO.getCartByAccountId(accountId).getCartItemByProductId(productId);

        return cartItem == null ? 0 : cartItem.getCount();
    }

    @Transactional
    @Override
    public List<CartItemDTO> getCartItems(int accountId) {
        List<CartItemDTO> cartItems = new ArrayList<>();

        for (CartItem cartItem : cartDAO.getCartByAccountId(accountId).getCartItems()) {
            CartItemDTO cartItemDTO = modelMapperWrapper.mapCartItem(cartItem);

            ProductDTO productDTO = magazinProductClient.getProductInfo(cartItemDTO.getProductId());

            if (productDTO == null) {
                log.warn("Product id {}: Not found.", cartItemDTO.getProductId());

                return null;
            }

            cartItemDTO.setProduct(productDTO);
            cartItems.add(cartItemDTO);
        }

        return cartItems;
    }

    @Transactional
    @Override
    public void removeCartItem(int accountId, int productId) {
        boolean wasRemoved = false;
        Cart cart = cartDAO.getCartByAccountId(accountId);

        Iterator<CartItem> iterator = cart.getCartItems().iterator();

        while (iterator.hasNext()) {
            CartItem cartItem = iterator.next();

            if (cartItem.getProductId() == productId) {
                wasRemoved = true;
                iterator.remove();
            }
        }

        if (!wasRemoved) {
            log.debug("Cart {}: Product {} is not present in the cart.", cart.getId(), productId);
        } else {
            log.info("Cart {}: Product {} was removed from the cart.", cart.getId(), productId);
        }
    }
}
