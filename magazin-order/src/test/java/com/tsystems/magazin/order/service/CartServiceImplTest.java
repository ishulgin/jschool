package com.tsystems.magazin.order.service;

import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.CartItemRequestDTO;
import com.tsystems.magazin.order.client.MagazinProductClient;
import com.tsystems.magazin.order.dao.CartDAO;
import com.tsystems.magazin.order.model.Cart;
import com.tsystems.magazin.order.model.CartItem;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CartServiceImplTest {
    @Mock
    private CartDAO cartDAO;

    @Mock
    private MagazinProductClient magazinProductClient;

    @InjectMocks
    private CartServiceImpl cartService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addCartItem_productNotFound_IllegalArgumentException() {
        when(magazinProductClient.getProductInfo(1)).thenReturn(null);

        cartService.addCartItem(1, new CartItemRequestDTO());
    }

    @Test
    public void addCartItem_productNotPresent_add() {
        Cart cart = Mockito.mock(Cart.class);
        when(cartDAO.getCartByAccountId(1)).thenReturn(cart);
        when(cart.getCartItemByProductId(1)).thenReturn(null);

        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(1);
        when(magazinProductClient.getProductInfo(1)).thenReturn(productDTO);

        cartService.addCartItem(1, TestData.getCartItemRequestDTO1());

        verify(cart).addCartItem(TestData.getMappedCartItem1());
    }

    @Test
    public void addCartItem_productIsPresent_add() {
        Cart cart = Mockito.mock(Cart.class);
        when(cartDAO.getCartByAccountId(1)).thenReturn(cart);

        CartItem cartItem = Mockito.mock(CartItem.class);
        when(cart.getCartItemByProductId(1)).thenReturn(cartItem);

        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(1);
        when(magazinProductClient.getProductInfo(1)).thenReturn(productDTO);

        cartService.addCartItem(1, TestData.getCartItemRequestDTO1());

        verify(cartItem).increaseCount(2);
    }

    @Test
    public void getProductInCartCount_present_100() {
        when(cartDAO.getCartByAccountId(1)).thenReturn(TestData.getCart1());

        assertEquals(100, cartService.getProductInCartCount(1, 1));
    }

    @Test
    public void getProductInCartCount_notPresent_0() {
        when(cartDAO.getCartByAccountId(1)).thenReturn(TestData.getCart1());

        assertEquals(0, cartService.getProductInCartCount(1, 3));
    }

    @Test
    public void removeCartItem_itemNotPresent() {
        Cart cart = TestData.getCart1();
        when(cartDAO.getCartByAccountId(1)).thenReturn(cart);

        cartService.removeCartItem(1, 3);

        assertEquals(TestData.getCart1(), cart);
    }

    @Test
    public void removeCartItem_itemt() {
        Cart cart = TestData.getCart1();
        when(cartDAO.getCartByAccountId(1)).thenReturn(cart);

        cartService.removeCartItem(1, 2);

        assertEquals(TestData.getCartAfterProductWasRemoved1(), cart);
    }
}