package com.tsystems.magazin.order.service;

import com.tsystems.magazin.domain.dto.AddressDTO;
import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.model.OrderStatus;
import com.tsystems.magazin.domain.model.PaymentStatus;
import com.tsystems.magazin.order.client.MagazinAccountClient;
import com.tsystems.magazin.order.client.MagazinProductClient;
import com.tsystems.magazin.order.client.MagazinStatClient;
import com.tsystems.magazin.order.dao.CartDAO;
import com.tsystems.magazin.order.dao.OrderDAO;
import com.tsystems.magazin.order.model.Cart;
import com.tsystems.magazin.order.model.Order;
import com.tsystems.magazin.order.util.ModelMapperWrapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

public class OrderServiceImplTest {
    @Mock
    private OrderDAO orderDAO;

    @Mock
    private CartDAO cartDAO;

    @Mock
    private MagazinProductClient magazinProductClient;

    @Mock
    private MagazinAccountClient magazinAccountClient;

    @Mock
    private MagazinStatClient magazinStatClient;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @Mock
    private ModelMapperWrapper modelMapperWrapper;

    @InjectMocks
    private OrderServiceImpl orderService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getOrderItems_orderNotFound_null() {
        when(orderDAO.getOrderById(1)).thenReturn(null);

        assertNull(orderService.getOrderItems(11, 1));
    }

    @Test
    public void getOrderItems_differentAccounts_null() {
        Order order = new Order();
        order.setAccountId(10);
        when(orderDAO.getOrderById(1)).thenReturn(order);

        assertNull(orderService.getOrderItems(11, 1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkout_addressNotFound_IllegalArgumentException() {
        when(magazinAccountClient.getAddress(1)).thenReturn(null);

        orderService.checkout(TestData.getOrderRequestDTO1());
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkout_differentAccounts_IllegalArgumentException() {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAccountId(11);
        when(magazinAccountClient.getAddress(1)).thenReturn(addressDTO);

        orderService.checkout(TestData.getOrderRequestDTO1());
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkout_emptyCart_IllegalArgumentException() {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAccountId(1);
        when(magazinAccountClient.getAddress(1)).thenReturn(addressDTO);

        Cart cart = TestData.getCart1();
        cart.setCartItems(Collections.emptySet());
        when(cartDAO.getCartByAccountId(1)).thenReturn(cart);

        orderService.checkout(TestData.getOrderRequestDTO1());
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkout_productNotFound_IllegalArgumentException() {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAccountId(1);
        when(magazinAccountClient.getAddress(1)).thenReturn(addressDTO);

        when(cartDAO.getCartByAccountId(1)).thenReturn(TestData.getCart1());
        when(magazinProductClient.getProductInfo(anyInt())).thenReturn(null);

        orderService.checkout(TestData.getOrderRequestDTO1());
    }

    @Test
    public void checkout_countLessThanLeft_false() {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAccountId(1);
        when(magazinAccountClient.getAddress(1)).thenReturn(addressDTO);

        when(cartDAO.getCartByAccountId(1)).thenReturn(TestData.getCart1());

        ProductDTO product1 = new ProductDTO();
        product1.setLeftCount(10);
        when(magazinProductClient.getProductInfo(1)).thenReturn(product1);

        ProductDTO product2 = new ProductDTO();
        product2.setLeftCount(1000);
        when(magazinProductClient.getProductInfo(2)).thenReturn(product2);

        assertFalse(orderService.checkout(TestData.getOrderRequestDTO1()));
    }

    @Test
    public void checkout_updateFailed_false() {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAccountId(1);
        when(magazinAccountClient.getAddress(1)).thenReturn(addressDTO);

        when(cartDAO.getCartByAccountId(1)).thenReturn(TestData.getCart1());

        ProductDTO product1 = new ProductDTO();
        product1.setLeftCount(1000);
        when(magazinProductClient.getProductInfo(1)).thenReturn(product1);

        ProductDTO product2 = new ProductDTO();
        product2.setLeftCount(1000);
        when(magazinProductClient.getProductInfo(2)).thenReturn(product2);

        doThrow(new IllegalArgumentException())
                .when(magazinProductClient)
                .updateProductsCount(Arrays.asList(TestData.getCountUpdateRequestDTO1(), TestData.getCountUpdateRequestDTO2()));

        assertFalse(orderService.checkout(TestData.getOrderRequestDTO1()));
    }

    @Test
    public void checkout_true() {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAccountId(1);
        when(magazinAccountClient.getAddress(1)).thenReturn(addressDTO);

        Cart cart = Mockito.mock(Cart.class);
        when(cart.getCartItems()).thenReturn(new HashSet<>(Arrays.asList(TestData.getCartItem1(), TestData.getCartItem2())));
        when(cartDAO.getCartByAccountId(1)).thenReturn(cart);

        ProductDTO product1 = new ProductDTO();
        product1.setLeftCount(1000);
        product1.setPrice(11111);
        product1.setCurrentPrice(100);
        when(magazinProductClient.getProductInfo(1)).thenReturn(product1);

        ProductDTO product2 = new ProductDTO();
        product2.setLeftCount(1000);
        product2.setPrice(10000);
        product2.setCurrentPrice(200);
        when(magazinProductClient.getProductInfo(2)).thenReturn(product2);
        when(modelMapperWrapper.mapOrderRequestDTO(TestData.getOrderRequestDTO1())).thenReturn(TestData.getMappedOrder1());

        assertTrue(orderService.checkout(TestData.getOrderRequestDTO1()));

        verify(orderDAO).saveOrder(TestData.getOrderBeforeSave1());
        verify(cart).clearCart();
    }

    @Test(expected = IllegalArgumentException.class)
    public void updatePaymentStatus_orderNotFound_IllegalArgumentException() {
        when(orderDAO.getOrderById(1)).thenReturn(null);

        orderService.updatePaymentStatus(1, PaymentStatus.PENDING);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateOrderStatus_orderNotFound_IllegalArgumentException() {
        when(orderDAO.getOrderById(1)).thenReturn(null);

        orderService.updateOrderStatus(1, OrderStatus.PENDING_PAYMENT);
    }
}