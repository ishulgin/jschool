package com.tsystems.magazin.order.service;

import com.tsystems.magazin.domain.dto.request.CartItemRequestDTO;
import com.tsystems.magazin.domain.dto.request.CountUpdateRequestDTO;
import com.tsystems.magazin.domain.dto.request.OrderRequestDTO;
import com.tsystems.magazin.domain.model.OrderStatus;
import com.tsystems.magazin.domain.model.PaymentMethod;
import com.tsystems.magazin.domain.model.PaymentStatus;
import com.tsystems.magazin.order.model.Cart;
import com.tsystems.magazin.order.model.CartItem;
import com.tsystems.magazin.order.model.Order;
import com.tsystems.magazin.order.model.OrderItem;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

public class TestData {
    public static Cart getCart1() {
        Cart cart = new Cart();
        cart.setCartItems(new HashSet<>(Arrays.asList(getCartItem1(), getCartItem2())));

        return cart;
    }

    public static Cart getCartAfterProductWasRemoved1() {
        Cart cart = new Cart();
        cart.setCartItems(new HashSet<>(Collections.singletonList(getCartItem1())));

        return cart;
    }

    public static CartItem getCartItem1() {
        CartItem cartItem = new CartItem();
        cartItem.setId(100);
        cartItem.setProductId(1);
        cartItem.setCount(100);

        return cartItem;
    }

    public static OrderItem getOrderItem1() {
        OrderItem orderItem = new OrderItem();
        orderItem.setProductId(1);
        orderItem.setPrice(100);
        orderItem.setCount(100);

        return orderItem;
    }

    public static CountUpdateRequestDTO getCountUpdateRequestDTO1() {
        CountUpdateRequestDTO countUpdateRequestDTO = new CountUpdateRequestDTO();
        countUpdateRequestDTO.setCount(100);
        countUpdateRequestDTO.setProductId(1);

        return countUpdateRequestDTO;
    }

    public static CartItem getCartItem2() {
        CartItem cartItem = new CartItem();
        cartItem.setId(120);
        cartItem.setProductId(2);
        cartItem.setCount(200);

        return cartItem;
    }

    public static OrderItem getOrderItem2() {
        OrderItem orderItem = new OrderItem();
        orderItem.setProductId(2);
        orderItem.setPrice(200);
        orderItem.setCount(200);

        return orderItem;
    }

    public static CountUpdateRequestDTO getCountUpdateRequestDTO2() {
        CountUpdateRequestDTO countUpdateRequestDTO = new CountUpdateRequestDTO();
        countUpdateRequestDTO.setCount(200);
        countUpdateRequestDTO.setProductId(2);

        return countUpdateRequestDTO;
    }

    public static CartItemRequestDTO getCartItemRequestDTO1() {
        CartItemRequestDTO cartItemRequestDTO = new CartItemRequestDTO();
        cartItemRequestDTO.setProductId(1);
        cartItemRequestDTO.setCount(2);

        return cartItemRequestDTO;
    }

    public static CartItem getMappedCartItem1() {
        CartItem cartItem = new CartItem();
        cartItem.setProductId(1);
        cartItem.setCount(2);

        return cartItem;
    }

    public static OrderRequestDTO getOrderRequestDTO1() {
        OrderRequestDTO orderRequestDTO = new OrderRequestDTO();
        orderRequestDTO.setAddressId(1);
        orderRequestDTO.setAccountId(1);
        orderRequestDTO.setPaymentMethod(PaymentMethod.CARD);

        return orderRequestDTO;
    }

    public static Order getMappedOrder1() {
        Order order = new Order();
        order.setAccountId(1);
        order.setAddressId(1);

        return order;
    }

    public static Order getOrderBeforeSave1() {
        OrderItem orderItem1 = new OrderItem();
        orderItem1.setCount(100);
        orderItem1.setPrice(100);
        orderItem1.setProductId(1);

        OrderItem orderItem2 = new OrderItem();
        orderItem2.setCount(200);
        orderItem2.setPrice(200);
        orderItem2.setProductId(2);

        Order order = getMappedOrder1();
        order.setOrderItems(new HashSet<>(Arrays.asList(orderItem1, orderItem2)));

        order.setOrderStatus(OrderStatus.PROCESSING);
        order.setPaymentStatus(PaymentStatus.PENDING);

        return order;
    }
}
