CREATE TABLE DepartmentStat (
  id           INT AUTO_INCREMENT,
  departmentId INT  NOT NULL,
  date         DATE NOT NULL,
  count        INT  NOT NULL,
  profit       INT  NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;

CREATE TABLE ProductStat (
  id         INT AUTO_INCREMENT,
  productId  INT  NOT NULL,
  categoryId INT  NOT NULL,
  date       DATE NOT NULL,
  count      INT  NOT NULL,
  profit     INT  NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;

CREATE TABLE CategoryStat (
  id           INT AUTO_INCREMENT,
  categoryId   INT  NOT NULL,
  departmentId INT  NOT NULL,
  date         DATE NOT NULL,
  count        INT  NOT NULL,
  profit       INT  NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;

CREATE TABLE AccountStat (
  id        INT AUTO_INCREMENT,
  accountId INT         NOT NULL,
  name      VARCHAR(64) NOT NULL,
  surname   VARCHAR(64) NOT NULL,
  profit    INT         NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;

CREATE TABLE MonthStat (
  id INT AUTO_INCREMENT,
  month DATE NOT NULL,
  profit INT NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;