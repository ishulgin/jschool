package com.tsystems.magazin.stat.service;

import com.tsystems.magazin.domain.dto.*;
import com.tsystems.magazin.domain.dto.request.StatUnitRequestDTO;
import com.tsystems.magazin.stat.client.MagazinAccountClient;
import com.tsystems.magazin.stat.client.MagazinProductClient;
import com.tsystems.magazin.stat.dao.StatDAO;
import com.tsystems.magazin.stat.model.*;
import com.tsystems.magazin.stat.util.ModelMapperWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class StatServiceImpl implements StatService {
    private StatDAO statDAO;

    private MagazinProductClient magazinProductClient;

    private MagazinAccountClient magazinAccountClient;

    private ModelMapperWrapper modelMapperWrapper;

    @Autowired
    public void setStatDAO(StatDAO statDAO) {
        this.statDAO = statDAO;
    }

    @Autowired
    public void setMagazinProductClient(MagazinProductClient magazinProductClient) {
        this.magazinProductClient = magazinProductClient;
    }

    @Autowired
    public void setMagazinAccountClient(MagazinAccountClient magazinAccountClient) {
        this.magazinAccountClient = magazinAccountClient;
    }

    @Autowired
    public void setModelMapperWrapper(ModelMapperWrapper modelMapperWrapper) {
        this.modelMapperWrapper = modelMapperWrapper;
    }

    @Override
    @Transactional
    public void updateStats(int accountId, @NotNull List<StatUnitRequestDTO> statUnitRequestDTOs) {
        int totalProfit = 0;

        for (StatUnitRequestDTO statUnitRequestDTO : statUnitRequestDTOs) {
            updateDepartmentStatRecord(statUnitRequestDTO);
            updateCategoryStatRecord(statUnitRequestDTO);
            updateProductStatRecord(statUnitRequestDTO);


            totalProfit += statUnitRequestDTO.getProfit();
        }

        updateMonthStat(totalProfit);
        updateAccountStatRecord(accountId, totalProfit);
    }

    private void updateDepartmentStatRecord(@NotNull StatUnitRequestDTO statUnitRequestDTO) {
        DepartmentStat departmentStat = statDAO.getTodayDepartmentStatByDepartmentId(statUnitRequestDTO.getDepartmentId());

        if (departmentStat == null) {
            createDepartmentStatRecord(statUnitRequestDTO);
        } else {
            log.info("Department stat {}: count {} -> {}; profit {} -> {}.",
                    departmentStat.getId(), departmentStat.getCount(), departmentStat.getCount() + statUnitRequestDTO.getCount(),
                    departmentStat.getProfit(), departmentStat.getProfit() + statUnitRequestDTO.getProfit());

            departmentStat.increaseCount(statUnitRequestDTO.getCount());
            departmentStat.increaseProfit(statUnitRequestDTO.getProfit());
        }
    }

    private void updateCategoryStatRecord(@NotNull StatUnitRequestDTO statUnitRequestDTO) {
        CategoryStat categoryStat = statDAO.getTodayCategoryStatByCategoryId(statUnitRequestDTO.getCategoryId());

        if (categoryStat == null) {
            createCategoryStatRecord(statUnitRequestDTO);
        } else {
            log.info("Category stat {}: count {} -> {}; profit {} -> {}.",
                    categoryStat.getId(), categoryStat.getCount(), categoryStat.getCount() + statUnitRequestDTO.getCount(),
                    categoryStat.getProfit(), categoryStat.getProfit() + statUnitRequestDTO.getProfit());

            categoryStat.increaseCount(statUnitRequestDTO.getCount());
            categoryStat.increaseProfit(statUnitRequestDTO.getProfit());
        }
    }

    private void updateProductStatRecord(@NotNull StatUnitRequestDTO statUnitRequestDTO) {
        ProductStat productStat = statDAO.getTodayProductStatByProductId(statUnitRequestDTO.getProductId());

        if (productStat == null) {
            createProductStatRecord(statUnitRequestDTO);
        } else {
            log.info("Product stat {}: count {} -> {}; profit {} -> {}.",
                    productStat.getId(), productStat.getCount(), productStat.getCount() + statUnitRequestDTO.getCount(),
                    productStat.getProfit(), productStat.getProfit() + statUnitRequestDTO.getProfit());

            productStat.increaseCount(statUnitRequestDTO.getCount());
            productStat.increaseProfit(statUnitRequestDTO.getProfit());
        }
    }

    private void updateAccountStatRecord(int accountId, int profit) {
        AccountStat accountStat = statDAO.getAccountStatByAccountId(accountId);

        AccountDTO accountDTO = magazinAccountClient.getAccountDetails(accountId);

        if (accountDTO == null) {
            log.warn("Account {}: Not found.", accountId);

            throw new IllegalArgumentException();
        }

        if (accountStat == null) {
            createAccountStatRecord(accountDTO, profit);
        } else {
            log.info("Account stat {}: profit {} -> {}.", accountStat.getId(), accountStat.getProfit(),
                    accountStat.getProfit() + profit);
            if (!accountDTO.getName().equals(accountStat.getName())) {
                accountStat.setName(accountDTO.getName());
            }

            if (!accountDTO.getSurname().equals(accountStat.getSurname())) {
                accountStat.setSurname(accountDTO.getSurname());
            }

            accountStat.increaseProfit(profit);
        }
    }

    private void updateMonthStat(int profit) {
        MonthStat monthStat = statDAO.getCurrentMonthStat();

        if (monthStat == null) {
            createMonthStatRecord(profit);
        } else {
            log.info("Month stat {}: profit {} -> {}.",
                    monthStat.getMonth().toLocalDate().getMonthValue(),
                    monthStat.getProfit(), monthStat.getProfit() + profit);

            monthStat.increaseProfit(profit);
        }
    }

    private void createDepartmentStatRecord(@NotNull StatUnitRequestDTO statUnitRequestDTO) {
        DepartmentStat departmentStat = new DepartmentStat();
        departmentStat.setDepartmentId(statUnitRequestDTO.getDepartmentId());
        departmentStat.setCount(statUnitRequestDTO.getCount());
        departmentStat.setProfit(statUnitRequestDTO.getProfit());

        statDAO.saveDepartmentStat(departmentStat);

        log.info("Department {}: Department stat record for {} was created with initial count {} and profit {}.",
                departmentStat.getDepartmentId(), departmentStat.getDate(),
                statUnitRequestDTO.getCount(), statUnitRequestDTO.getProfit());
    }

    private void createCategoryStatRecord(@NotNull StatUnitRequestDTO statUnitRequestDTO) {
        CategoryStat categoryStat = new CategoryStat();
        categoryStat.setDepartmentId(statUnitRequestDTO.getDepartmentId());
        categoryStat.setCategoryId(statUnitRequestDTO.getCategoryId());
        categoryStat.setCount(statUnitRequestDTO.getCount());
        categoryStat.setProfit(statUnitRequestDTO.getProfit());

        statDAO.saveCategoryStat(categoryStat);

        log.info("Category {}: Category stat record for {} was created with initial count {} and profit {}.",
                categoryStat.getCategoryId(), categoryStat.getDate(),
                statUnitRequestDTO.getCount(), statUnitRequestDTO.getProfit());
    }

    private void createProductStatRecord(@NotNull StatUnitRequestDTO statUnitRequestDTO) {
        ProductStat productStat = new ProductStat();
        productStat.setProductId(statUnitRequestDTO.getProductId());
        productStat.setCategoryId(statUnitRequestDTO.getCategoryId());
        productStat.setCount(statUnitRequestDTO.getCount());
        productStat.setProfit(statUnitRequestDTO.getProfit());

        statDAO.saveProductStat(productStat);
        log.info("Product {}: Product stat record for {} was created with initial count {} and profit {}.",
                productStat.getProductId(), productStat.getDate(),
                statUnitRequestDTO.getCount(), statUnitRequestDTO.getProfit());
    }

    private void createAccountStatRecord(AccountDTO accountDTO, int profit) {
        AccountStat accountStat = new AccountStat();
        accountStat.setAccountId(accountDTO.getId());
        accountStat.setName(accountDTO.getName());
        accountStat.setSurname(accountDTO.getSurname());
        accountStat.setProfit(profit);

        statDAO.saveAccountStat(accountStat);
        log.info("Account {}: Account stat with initial profit {} was created.",
                accountStat.getAccountId(), profit);
    }

    private void createMonthStatRecord(int profit) {
        MonthStat monthStat = new MonthStat();
        monthStat.setProfit(profit);
        monthStat.setMonth(Date.valueOf(LocalDate.now()));

        statDAO.saveMonthStat(monthStat);
        log.info("Month {}: Month stat with initial profit {} was created.", LocalDate.now().getMonthValue(), profit);
    }

    @Transactional
    @Override
    public List<DepartmentStatDTO> getDepartmentStatForLastWeek() {
        Map<Integer, DepartmentStatDTO> departmentStats = new HashMap<>();

        for (DepartmentStat stat : statDAO.getDepartmentsStatSince(getWeekAgoDate())) {
            int id = stat.getDepartmentId();

            StatUnit statUnit = new StatUnit();
            statUnit.setId(stat.getId());
            statUnit.setCount(stat.getCount());
            statUnit.setProfit(stat.getProfit());

            if (departmentStats.containsKey(id)) {
                departmentStats.get(id).addStat(stat.getDate().toLocalDate(), statUnit);
            } else {
                CategoryDTO categoryDTO = magazinProductClient.getAbstractCategoryInfo(stat.getDepartmentId());

                if (categoryDTO == null) {
                    log.warn("Department id {}: Can't get category details.", stat.getDepartmentId());

                    return null;
                }

                DepartmentStatDTO departmentStatDTO = new DepartmentStatDTO();
                departmentStatDTO.setDepartmentName(categoryDTO.getName());
                departmentStatDTO.addStat(stat.getDate().toLocalDate(), statUnit);

                departmentStats.put(id, departmentStatDTO);
            }
        }

        return new ArrayList<>(departmentStats.values());
    }

    private static Date getWeekAgoDate() {
        return Date.valueOf(LocalDate.now().minusDays(6));
    }

    @Transactional
    @Override
    public List<CategoryStatDTO> getCategoryStat(int departmentStatId) {
        DepartmentStat departmentStat = statDAO.getDepartmentStatById(departmentStatId);

        if (departmentStat == null) {
            log.debug("Department stat {}: Not found.", departmentStatId);

            return null;
        }

        List<CategoryStatDTO> result = new ArrayList<>();

        for (CategoryStat categoryStat : statDAO.getCategoryStat(departmentStat.getDepartmentId(), departmentStat.getDate())) {
            CategoryDTO categoryDTO = magazinProductClient.getRealCategoryInfo(categoryStat.getCategoryId());

            if (categoryDTO == null) {
                log.warn("Category id {}: Not found.", categoryStat.getCategoryId());

                return null;
            }

            CategoryStatDTO categoryStatDTO = new CategoryStatDTO();
            categoryStatDTO.setCategoryName(categoryDTO.getName());
            categoryStatDTO.setId(categoryStat.getId());
            categoryStatDTO.setCount(categoryStat.getCount());
            categoryStatDTO.setProfit(categoryStat.getProfit());

            result.add(categoryStatDTO);
        }

        return result;
    }

    @Transactional
    @Override
    public List<ProductStatDTO> getProductStat(int categoryStatId) {
        CategoryStat categoryStat = statDAO.getCategoryStatById(categoryStatId);

        if (categoryStat == null) {
            log.debug("Category stat {}: Not found.", categoryStatId);

            return null;
        }

        List<ProductStatDTO> result = new ArrayList<>();

        for (ProductStat productStat : statDAO.getProductStat(categoryStat.getCategoryId(), categoryStat.getDate())) {
            ProductDTO productDTO = magazinProductClient.getProductInfo(productStat.getProductId());

            if (productDTO == null) {
                log.warn("Product {}: Not found.", productStat.getProductId());

                return null;
            }

            ProductStatDTO productStatDTO = new ProductStatDTO();
            productStatDTO.setProductName(productDTO.getName());
            productStatDTO.setProductId(productStat.getProductId());
            productStatDTO.setCount(productStat.getCount());
            productStatDTO.setProfit(productStat.getProfit());

            result.add(productStatDTO);
        }

        return result;
    }

    @Transactional
    @Override
    public List<Integer> getMostPopularProductsIds(int n) {
        try {
            return statDAO.getMostPopularProductsIds(n);
        } catch (IllegalArgumentException e) {
            log.warn("Unable to get {} most popular products.", n);

            return null;
        }
    }

    @Transactional
    @Override
    public List<AccountStatDTO> getTopAccountStat(int n) {
        try {
            return statDAO.getTopAccountStat(n)
                    .stream()
                    .map(modelMapperWrapper::mapAccountStat)
                    .collect(Collectors.toList());
        } catch (IllegalArgumentException e) {
            log.warn("Unable to get top {} account stat.", n);

            return null;
        }
    }

    @Override
    public List<MonthStatDTO> getMonthStatForCurrentYear() {
        return statDAO.getMonthStatByYear(LocalDate.now().getYear())
                .stream()
                .map(modelMapperWrapper::mapMonthStat)
                .collect(Collectors.toList());
    }
}
