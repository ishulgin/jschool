package com.tsystems.magazin.stat.dao;

import com.tsystems.magazin.stat.model.*;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.List;

public interface StatDAO {
    DepartmentStat getDepartmentStatById(int id);

    CategoryStat getCategoryStatById(int id);

    @NotNull List<DepartmentStat> getDepartmentsStatSince(@NotNull Date dateSince);

    @NotNull List<CategoryStat> getCategoryStat(int departmentId, @NotNull Date date);

    @NotNull List<ProductStat> getProductStat(int categoryId, @NotNull Date date);

    @NotNull List<AccountStat> getTopAccountStat(int n);

    @NotNull List<MonthStat> getMonthStatByYear(int year);

    DepartmentStat getTodayDepartmentStatByDepartmentId(int departmentId);

    CategoryStat getTodayCategoryStatByCategoryId(int categoryId);

    ProductStat getTodayProductStatByProductId(int productId);

    AccountStat getAccountStatByAccountId(int accountId);

    MonthStat getCurrentMonthStat();

    void saveDepartmentStat(@NotNull DepartmentStat departmentStat);

    void saveCategoryStat(@NotNull CategoryStat categoryStat);

    void saveProductStat(@NotNull ProductStat productStat);

    void saveAccountStat(@NotNull AccountStat accountStat);

    void saveMonthStat(@NotNull MonthStat monthStat);

    @NotNull List<Integer> getMostPopularProductsIds(int n);
}
