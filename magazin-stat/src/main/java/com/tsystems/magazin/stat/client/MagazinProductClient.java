package com.tsystems.magazin.stat.client;

import com.tsystems.magazin.domain.dto.CategoryDTO;
import com.tsystems.magazin.domain.dto.ProductDTO;
import com.tsystems.magazin.domain.dto.request.CountUpdateRequestDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("magazin-product")
public interface MagazinProductClient {
    @PostMapping(value = "/categories/abstract/{id}")
    CategoryDTO getRealCategoryInfo(@PathVariable("id") int categoryId);

    @PostMapping(value = "/categories/real/{id}")
    CategoryDTO getAbstractCategoryInfo(@PathVariable("id") int categoryId);

    @GetMapping("/products/{id}")
    ProductDTO getProductInfo(@PathVariable("id") int productId);

    @PostMapping("/products/count")
    void updateProductsCount(@RequestBody List<CountUpdateRequestDTO> countUpdateRequestDTO);
}
