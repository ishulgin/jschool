package com.tsystems.magazin.stat.controller;

import com.tsystems.magazin.domain.dto.*;
import com.tsystems.magazin.domain.dto.request.StatUnitRequestDTO;
import com.tsystems.magazin.stat.service.StatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class StatController {
    private StatService statService;

    @Autowired
    public void setStatService(StatService statService) {
        this.statService = statService;
    }

    @GetMapping("/stats/products/top")
    public ResponseEntity<List<Integer>> getMostPopularProductsIds(@RequestParam("n") int n) {
        return ResponseEntity.ok(statService.getMostPopularProductsIds(n));
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/stats/{id}")
    public ResponseEntity updateStats(@PathVariable("id") int accountId, @RequestBody List<@Valid StatUnitRequestDTO> statUnitRequestDTOs) {
        try {
            statService.updateStats(accountId, statUnitRequestDTOs);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/stats/week/departments")
    public ResponseEntity<List<DepartmentStatDTO>> getDepartmentStatForLastWeek() {
        return ResponseEntity.ok(statService.getDepartmentStatForLastWeek());
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/stats/week/departments/{id}")
    public ResponseEntity<List<CategoryStatDTO>> getCategoryStat(@PathVariable int id) {
        return ResponseEntity.ok(statService.getCategoryStat(id));
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/stats/week/categories/{id}")
    public ResponseEntity<List<ProductStatDTO>> getProductStat(@PathVariable int id) {
        return ResponseEntity.ok(statService.getProductStat(id));
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/stats/year")
    public ResponseEntity<List<MonthStatDTO>> getYearStat() {
        return ResponseEntity.ok(statService.getMonthStatForCurrentYear());
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/stats/accounts")
    public ResponseEntity<List<AccountStatDTO>> getAccountStats(@RequestParam int n) {
        return ResponseEntity.ok(statService.getTopAccountStat(n));
    }
}
