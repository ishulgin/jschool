package com.tsystems.magazin.stat.service;

import com.tsystems.magazin.domain.dto.*;
import com.tsystems.magazin.domain.dto.request.StatUnitRequestDTO;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface StatService {
    void updateStats(int accountId, @NotNull List<StatUnitRequestDTO> statUnitRequestDTOs);

    List<DepartmentStatDTO> getDepartmentStatForLastWeek();

    List<CategoryStatDTO> getCategoryStat(int departmentStatId);

    List<ProductStatDTO> getProductStat(int categoryStatId);

    List<Integer> getMostPopularProductsIds(int n);

    List<AccountStatDTO> getTopAccountStat(int n);

    List<MonthStatDTO> getMonthStatForCurrentYear();
}
