package com.tsystems.magazin.stat.client;

import com.tsystems.magazin.domain.dto.AccountDTO;
import com.tsystems.magazin.domain.dto.AddressDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("magazin-account")
public interface MagazinAccountClient {
    @GetMapping("/accountDetails")
    AccountDTO getAccountDetails(@RequestParam("id") int accountId);

    @GetMapping("/addresses/{id}")
    AddressDTO getAddress(@PathVariable("id") int addressId);
}
