package com.tsystems.magazin.stat.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class AccountStat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private int accountId;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;

    @Column(nullable = false)
    private int profit;

    public void increaseProfit(int n) {
        profit += n;
    }
}
