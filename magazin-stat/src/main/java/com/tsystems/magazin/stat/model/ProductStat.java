package com.tsystems.magazin.stat.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class ProductStat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private int productId;
    @Column(nullable = false)
    private int categoryId;

    @Column(nullable = false)
    private int count;

    @Column(nullable = false)
    private int profit;

    @Column(nullable = false)
    @CreationTimestamp
    private Date date;

    public void increaseCount(int n) {
        count += n;
    }

    public void increaseProfit(int n) {
        profit += n;
    }
}
