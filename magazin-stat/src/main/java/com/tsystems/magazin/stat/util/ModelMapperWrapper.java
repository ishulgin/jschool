package com.tsystems.magazin.stat.util;

import com.tsystems.magazin.domain.dto.AccountStatDTO;
import com.tsystems.magazin.domain.dto.MonthStatDTO;
import com.tsystems.magazin.stat.model.AccountStat;
import com.tsystems.magazin.stat.model.MonthStat;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.sql.Date;

@Component
public class ModelMapperWrapper {
    private ModelMapper modelMapper = new ModelMapper();

    public ModelMapperWrapper() {
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.LOOSE);

        modelMapper.getConfiguration().setAmbiguityIgnored(true);

        modelMapper.createTypeMap(MonthStat.class, MonthStatDTO.class)
                .addMappings(mapping -> {
                    mapping.using(ctx -> ((Date) ctx.getSource()).toLocalDate().getMonthValue())
                            .map(MonthStat::getMonth, MonthStatDTO::setMonth);
                    mapping.using(ctx -> ((Date) ctx.getSource()).toLocalDate().getYear())
                            .map(MonthStat::getMonth, MonthStatDTO::setYear);
                });
    }

    @NotNull
    public AccountStatDTO mapAccountStat(@NotNull AccountStat accountStat) {
        return modelMapper.map(accountStat, AccountStatDTO.class);
    }

    @NotNull
    public MonthStatDTO mapMonthStat(@NotNull MonthStat monthStat) {
        return modelMapper.map(monthStat, MonthStatDTO.class);
    }
}
