package com.tsystems.magazin.stat.dao;

import com.tsystems.magazin.stat.model.*;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.List;

@Repository
public class StatDAOImpl implements StatDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public DepartmentStat getDepartmentStatById(int id) {
        return entityManager.find(DepartmentStat.class, id);
    }

    @Override
    public CategoryStat getCategoryStatById(int id) {
        return entityManager.find(CategoryStat.class, id);
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<DepartmentStat> getDepartmentsStatSince(@NotNull Date dateSince) {
        Query query = entityManager.createQuery("select s from DepartmentStat s where s.date > :dateSince")
                .setParameter("dateSince", dateSince);

        return (List<DepartmentStat>) query.getResultList();
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<CategoryStat> getCategoryStat(int departmentId, @NotNull Date date) {
        Query query = entityManager.createQuery("select s from CategoryStat s where s.departmentId = :departmentId and s.date = :date")
                .setParameter("departmentId", departmentId)
                .setParameter("date", date);

        return (List<CategoryStat>) query.getResultList();
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<ProductStat> getProductStat(int categoryId, @NotNull Date date) {
        Query query = entityManager.createQuery("select s from ProductStat s where s.categoryId = :categoryId and s.date = :date")
                .setParameter("categoryId", categoryId)
                .setParameter("date", date);

        return (List<ProductStat>) query.getResultList();
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<AccountStat> getTopAccountStat(int n) {
        Query query = entityManager.createQuery("select s from AccountStat s order by s.profit desc")
                .setMaxResults(n);

        return (List<AccountStat>) query.getResultList();
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<MonthStat> getMonthStatByYear(int year) {
        Query query = entityManager.createQuery("select s from MonthStat s where YEAR(s.month) = :year ")
                .setParameter("year", year);

        return (List<MonthStat>) query.getResultList();
    }

    @Override
    public DepartmentStat getTodayDepartmentStatByDepartmentId(int departmentId) {
        try {
            Query query = entityManager.createQuery("select s from DepartmentStat s where s.departmentId = :departmentId and s.date = CURRENT_DATE()")
                    .setParameter("departmentId", departmentId);

            return (DepartmentStat) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public CategoryStat getTodayCategoryStatByCategoryId(int categoryId) {
        try {
            Query query = entityManager.createQuery("select s from CategoryStat s where s.categoryId = :categoryId and s.date = CURRENT_DATE()")
                    .setParameter("categoryId", categoryId);

            return (CategoryStat) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public ProductStat getTodayProductStatByProductId(int productId) {
        try {
            Query query = entityManager.createQuery("select s from ProductStat s where s.productId = :productId and s.date = CURRENT_DATE()")
                    .setParameter("productId", productId);

            return (ProductStat) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public AccountStat getAccountStatByAccountId(int accountId) {
        try {
            Query query = entityManager.createQuery("select s from AccountStat s where s.accountId = :accountId")
                    .setParameter("accountId", accountId);

            return (AccountStat) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public MonthStat getCurrentMonthStat() {
        try {
            Query query = entityManager.createQuery("select s from MonthStat s where MONTH(s.month) = MONTH(CURRENT_DATE())");

            return (MonthStat) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public void saveDepartmentStat(@NotNull DepartmentStat departmentStat) {
        entityManager.persist(departmentStat);
    }

    @Override
    public void saveCategoryStat(@NotNull CategoryStat categoryStat) {
        entityManager.persist(categoryStat);
    }

    @Override
    public void saveProductStat(@NotNull ProductStat productStat) {
        entityManager.persist(productStat);
    }

    @Override
    public void saveAccountStat(@NotNull AccountStat accountStat) {
        entityManager.persist(accountStat);
    }

    @Override
    public void saveMonthStat(@NotNull MonthStat monthStat) {
        entityManager.persist(monthStat);
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<Integer> getMostPopularProductsIds(int n) {
        Query query = entityManager.createQuery("select s.productId from ProductStat s group by s.productId order by sum(s.count) desc")
                .setMaxResults(n);

        return (List<Integer>) query.getResultList();
    }
}
