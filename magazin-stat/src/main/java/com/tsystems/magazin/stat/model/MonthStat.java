package com.tsystems.magazin.stat.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
public class MonthStat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @CreationTimestamp
    private Date month;

    @Column(nullable = false)
    private int profit;

    public void increaseProfit(int n) {
        profit += n;
    }
}
