package com.tsystems.magazin.stat.client;

import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.stereotype.Component;

@Component
public class MagazinProductErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String methodKey, Response response) {
        if (response.status() == 400) {
            return new IllegalArgumentException();
        }

        return new Default().decode(methodKey, response);
    }
}
