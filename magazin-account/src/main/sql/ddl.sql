CREATE TABLE Account (
  id          INT AUTO_INCREMENT,
  name        VARCHAR(64) NOT NULL,
  surname     VARCHAR(64) NOT NULL,
  dateOfBirth DATE        NOT NULL,
  userId      INT         NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;

CREATE INDEX accountUserIdIndex
  ON Account (userId) USING HASH;

CREATE TABLE Address (
  id         INT AUTO_INCREMENT,
  country    VARCHAR(64) NOT NULL,
  city       VARCHAR(64) NOT NULL,
  postalCode VARCHAR(12) NOT NULL,
  street     VARCHAR(64) NOT NULL,
  house      SMALLINT    NOT NULL,
  flat       SMALLINT,
  accountId  INT,
  PRIMARY KEY (id),
  FOREIGN KEY (accountId) REFERENCES Account (id)
)
  ENGINE = InnoDB;