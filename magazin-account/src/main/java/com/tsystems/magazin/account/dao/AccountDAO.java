package com.tsystems.magazin.account.dao;

import com.tsystems.magazin.account.model.Account;
import com.tsystems.magazin.account.model.Address;

import javax.validation.constraints.NotNull;

public interface AccountDAO {
    void saveAccount(@NotNull Account account);

    Account getAccountById(int id);

    Account getAccountByUserId(int userId);

    Address getAddressById(int id);

    void saveAddress(@NotNull Address address);
}
