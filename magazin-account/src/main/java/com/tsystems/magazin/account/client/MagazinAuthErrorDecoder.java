package com.tsystems.magazin.account.client;

import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.stereotype.Component;

@Component
public class MagazinAuthErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String methodKey, Response response) {
        if (response.status() == 400) {
            return new IllegalArgumentException();
        }

        return new Default().decode(methodKey, response);
    }
}
