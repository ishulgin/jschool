package com.tsystems.magazin.account.service;

import com.tsystems.magazin.domain.dto.AccountDTO;
import com.tsystems.magazin.domain.dto.AddressDTO;
import com.tsystems.magazin.domain.dto.UserDTO;
import com.tsystems.magazin.domain.dto.request.AccountDetailsRequestDTO;
import com.tsystems.magazin.domain.dto.request.AccountRequestDTO;
import com.tsystems.magazin.domain.dto.request.AddressRequestDTO;
import com.tsystems.magazin.domain.dto.request.PasswordRequestDTO;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface AccountService {
    void createAccount(@NotNull AccountRequestDTO accountRequestDTO);

    UserDTO getUserByUsername(@NotNull String username);

    AccountDTO getAccountById(int accountId);

    void saveAddressByAccountId(int accountId, @NotNull AddressRequestDTO addressRequestDTO);

    AddressDTO getAddress(int addressId);

    List<AddressDTO> getAddressesByAccountId(int accountId);

    void updateAccountDetails(int accountId, @NotNull AccountDetailsRequestDTO accountDetailsRequestDTO);

    void changePasswordByAccountId(int accountId, @NotNull PasswordRequestDTO passwordRequestDTO);
}
