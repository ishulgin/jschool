package com.tsystems.magazin.account.dao;

import com.tsystems.magazin.account.model.Account;
import com.tsystems.magazin.account.model.Address;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;

@Service
public class AccountDAOImpl implements AccountDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void saveAccount(@NotNull Account account) {
        entityManager.persist(account);
    }

    @Override
    public Account getAccountById(int id) {
        return entityManager.find(Account.class, id);
    }

    @Override
    public Account getAccountByUserId(int userId) {
        return (Account) entityManager.createQuery("select a from Account a where a.userId=:userId")
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public Address getAddressById(int id) {
        return entityManager.find(Address.class, id);
    }

    @Override
    public void saveAddress(@NotNull Address address) {
        entityManager.persist(address);
    }
}
