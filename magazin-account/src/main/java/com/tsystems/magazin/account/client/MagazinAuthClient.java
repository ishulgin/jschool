package com.tsystems.magazin.account.client;

import com.tsystems.magazin.domain.dto.UserDTO;
import com.tsystems.magazin.domain.dto.request.PasswordRequestDTO;
import com.tsystems.magazin.domain.dto.request.UserRequestDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.constraints.NotNull;

@FeignClient(name = "magazin-auth")
public interface MagazinAuthClient {
    @PostMapping(value = "/users", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    int createUser(UserRequestDTO userRequestDTO);

    @GetMapping(value = "/users/{username}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    UserDTO loadUserDetails(@PathVariable("username") @NotNull String username);

    @PostMapping("/users/{id}/password")
    void changePassword(@PathVariable("id") int userId, @RequestBody @NotNull PasswordRequestDTO passwordRequestDTO);
}
