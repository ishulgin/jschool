package com.tsystems.magazin.account.service;

import com.tsystems.magazin.account.client.MagazinAuthClient;
import com.tsystems.magazin.account.dao.AccountDAO;
import com.tsystems.magazin.account.model.Account;
import com.tsystems.magazin.account.model.Address;
import com.tsystems.magazin.account.util.ModelMapperWrapper;
import com.tsystems.magazin.domain.dto.AccountDTO;
import com.tsystems.magazin.domain.dto.AddressDTO;
import com.tsystems.magazin.domain.dto.UserDTO;
import com.tsystems.magazin.domain.dto.request.AccountDetailsRequestDTO;
import com.tsystems.magazin.domain.dto.request.AccountRequestDTO;
import com.tsystems.magazin.domain.dto.request.AddressRequestDTO;
import com.tsystems.magazin.domain.dto.request.PasswordRequestDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {
    private MagazinAuthClient magazinAuthClient;

    private AccountDAO accountDAO;

    private ModelMapperWrapper modelMapperWrapper;

    @Autowired
    public void setMagazinAuthClient(MagazinAuthClient magazinAuthClient) {
        this.magazinAuthClient = magazinAuthClient;
    }

    @Autowired
    public void setModelMapperWrapper(ModelMapperWrapper modelMapperWrapper) {
        this.modelMapperWrapper = modelMapperWrapper;
    }

    @Autowired
    public void setAccountDAO(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @Override
    @Transactional
    public void createAccount(@NotNull AccountRequestDTO accountRequestDTO) {
        Account account = modelMapperWrapper.mapAccountRequestDTO(accountRequestDTO);

        account.setUserId(magazinAuthClient.createUser(accountRequestDTO.getUserRequestDTO()));
        accountDAO.saveAccount(account);

        log.info("{}: User was registered.", accountRequestDTO.getUsername());
    }

    @Override
    @Transactional
    public UserDTO getUserByUsername(@NotNull String username) {
        log.info("{}: Getting account details.", username);

        UserDTO userDTO = magazinAuthClient.loadUserDetails(username);

        try {
            if (userDTO != null) {
                userDTO.setAccountId(accountDAO.getAccountByUserId(userDTO.getId()).getId());
            }
        } catch (NoResultException e) {
            log.info("{}: no account associated with.", username);
        }

        return userDTO;
    }

    @Override
    @Transactional
    public AccountDTO getAccountById(int accountId) {
        Account account = getAccountIfExists(accountId);

        return modelMapperWrapper.mapAccount(account);
    }

    @Override
    @Transactional
    public void saveAddressByAccountId(int accountId, @NotNull AddressRequestDTO addressRequestDTO) {
        Account account = getAccountIfExists(accountId);

        Address address = modelMapperWrapper.mapAddressRequestDTO(addressRequestDTO);
        address.setAccount(account);

        accountDAO.saveAddress(address);

        log.info("Account {}: New address was associated.", accountId);
    }

    @Override
    @Transactional
    public AddressDTO getAddress(int addressId) {
        Address address = accountDAO.getAddressById(addressId);

        if (address == null) {
            log.debug("Address {}: Not found.", addressId);

            return null;
        }

        return modelMapperWrapper.mapAddress(address);
    }

    @Override
    @Transactional
    public List<AddressDTO> getAddressesByAccountId(int accountId) {
        Account account = getAccountIfExists(accountId);

        return account.getAddresses()
                .stream()
                .map(modelMapperWrapper::mapAddress)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void updateAccountDetails(int accountId, @NotNull AccountDetailsRequestDTO accountDetailsRequestDTO) {
        Account account = getAccountIfExists(accountId);

        account.setName(accountDetailsRequestDTO.getName());
        account.setSurname(accountDetailsRequestDTO.getSurname());
        account.setDateOfBirth(accountDetailsRequestDTO.getDateOfBirth());

        log.info("Account {}: Details were changed.", accountId);
    }

    @Override
    @Transactional
    public void changePasswordByAccountId(int accountId, @NotNull PasswordRequestDTO passwordRequestDTO) {
        log.debug("Account {}: Changing password.", accountId);

        Account account = getAccountIfExists(accountId);

        try {
            magazinAuthClient.changePassword(account.getUserId(), passwordRequestDTO);
        } catch (IllegalArgumentException e) {
            log.debug("Account {}: Unable to change password.", accountId);

            throw new IllegalArgumentException();
        }
    }

    private Account getAccountIfExists(int accountId) {
        Account account = accountDAO.getAccountById(accountId);

        if (account == null) {
            log.debug("Account {}: Not found.", accountId);

            throw new IllegalArgumentException();
        }

        return account;
    }
}
