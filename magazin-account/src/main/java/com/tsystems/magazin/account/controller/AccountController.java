package com.tsystems.magazin.account.controller;

import com.tsystems.magazin.account.service.AccountService;
import com.tsystems.magazin.domain.dto.AccountDTO;
import com.tsystems.magazin.domain.dto.AddressDTO;
import com.tsystems.magazin.domain.dto.UserDTO;
import com.tsystems.magazin.domain.dto.request.AccountDetailsRequestDTO;
import com.tsystems.magazin.domain.dto.request.AccountRequestDTO;
import com.tsystems.magazin.domain.dto.request.AddressRequestDTO;
import com.tsystems.magazin.domain.dto.request.PasswordRequestDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/account")
    public ResponseEntity createAccount(@RequestBody @Valid AccountRequestDTO accountRequestDTO) {
        try {
            accountService.createAccount(accountRequestDTO);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            logger.debug("Can't create account with username {}. Account with this username already exists.", accountRequestDTO.getUsername());

            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/userDetails")
    public ResponseEntity<UserDTO> loadUserDetails(@RequestParam("username") String username) {
        return ResponseEntity.ok(accountService.getUserByUsername(username));
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/accountDetails")
    public ResponseEntity<AccountDTO> getAccountDetails(@RequestParam("id") int accountId) {
        return ResponseEntity.ok(accountService.getAccountById(accountId));
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/account/{id}/address")
    public ResponseEntity addAddress(@PathVariable("id") int accountId, @RequestBody @Valid AddressRequestDTO addressRequestDTO) {
        try {
            accountService.saveAddressByAccountId(accountId, addressRequestDTO);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/account/{id}/addresses")
    public ResponseEntity<List<AddressDTO>> getAddresses(@PathVariable("id") int accountId) {
        return ResponseEntity.ok(accountService.getAddressesByAccountId(accountId));
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping("/addresses/{id}")
    public ResponseEntity<AddressDTO> getAddress(@PathVariable("id") int addressId) {
        return ResponseEntity.ok(accountService.getAddress(addressId));
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/account/{id}/details")
    public ResponseEntity changeDetails(@PathVariable("id") int accountId, @RequestBody @Valid AccountDetailsRequestDTO accountDetailsRequestDTO) {
        try {
            accountService.updateAccountDetails(accountId, accountDetailsRequestDTO);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("#oauth2.hasScope('server')")
    @PostMapping("/account/{id}/password")
    public ResponseEntity changePassword(@PathVariable("id") int accountId, @RequestBody @Valid PasswordRequestDTO passwordRequestDTO) {
        try {
            accountService.changePasswordByAccountId(accountId, passwordRequestDTO);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
