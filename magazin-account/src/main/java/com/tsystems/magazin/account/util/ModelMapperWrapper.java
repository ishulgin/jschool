package com.tsystems.magazin.account.util;

import com.tsystems.magazin.account.model.Account;
import com.tsystems.magazin.account.model.Address;
import com.tsystems.magazin.domain.dto.AccountDTO;
import com.tsystems.magazin.domain.dto.AddressDTO;
import com.tsystems.magazin.domain.dto.request.AccountRequestDTO;
import com.tsystems.magazin.domain.dto.request.AddressRequestDTO;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class ModelMapperWrapper {
    private ModelMapper modelMapper = new ModelMapper();

    public ModelMapperWrapper() {
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.LOOSE);

        modelMapper.getConfiguration().setAmbiguityIgnored(true);

        modelMapper.createTypeMap(AccountRequestDTO.class, Account.class)
                .addMappings(mapping -> {
                    mapping.skip(Account::setAddresses);
                    mapping.skip(Account::setId);
                    mapping.skip(Account::setUserId);
                });
    }

    @NotNull
    public Account mapAccountRequestDTO(@NotNull AccountRequestDTO accountRequestDTO) {
        return modelMapper.map(accountRequestDTO, Account.class);
    }

    @NotNull
    public AccountDTO mapAccount(@NotNull Account account) {
        return modelMapper.map(account, AccountDTO.class);
    }

    @NotNull
    public Address mapAddressRequestDTO(@NotNull AddressRequestDTO addressRequestDTO) {
        return modelMapper.map(addressRequestDTO, Address.class);
    }

    @NotNull
    public AddressDTO mapAddress(@NotNull Address address) {
        return modelMapper.map(address, AddressDTO.class);
    }
}
